Summary: WebSphere MQ Telemetry Service
Name: MQSeriesXRService
Version: 7.5.0
Release: 2
License: Commercial
ExclusiveArch: x86_64
Group: Applications/Networking
AutoReqProv: no
Vendor: IBM
Prefix: /opt/mqm
Requires: MQSeriesJava = 7.5.0-2
Requires: MQSeriesJRE = 7.5.0-2
Requires: MQSeriesServer = 7.5.0-2
Requires: MQSeriesRuntime = 7.5.0-2
%global __strip /bin/true
%global _rpmdir /build/slot1/p750_P/inst.images/amd64_linux_2/images/
%global _tmppath /build/slot1/p750_P/obj/amd64_linux_2/install/unix/linux_2
BuildRoot: /build/slot1/p750_P/obj/amd64_linux_2/ship

%description
IBM WebSphere MQ for Developers for Linux for x86_64
5724-H72
%clean
rm -rf $RPM_BUILD_ROOT

%install
install -d $RPM_BUILD_ROOT/opt/mqm
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr
install -d $RPM_BUILD_ROOT/opt/mqm/properties
install -d $RPM_BUILD_ROOT/opt/mqm/properties/version
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/bin
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/config
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/lib
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/samples
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/samples/TDD
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/samples/samples
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/samples/scraper
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/properties/version/IBM_WebSphere_MQ_Telemetry_Service.7.5.0.swtag $RPM_BUILD_ROOT/opt/mqm/properties/version/IBM_WebSphere_MQ_Telemetry_Service.7.5.0.swtag
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/bin/controlMQXR.bat $RPM_BUILD_ROOT/opt/mqm/mqxr/bin/controlMQXR.bat
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/bin/controlMQXRChannel.bat $RPM_BUILD_ROOT/opt/mqm/mqxr/bin/controlMQXRChannel.bat
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/bin/controlMQXRChannel.sh $RPM_BUILD_ROOT/opt/mqm/mqxr/bin/controlMQXRChannel.sh
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/bin/controlMQXR_mqm.sh $RPM_BUILD_ROOT/opt/mqm/mqxr/bin/controlMQXR_mqm.sh
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/bin/endMQXRService.bat $RPM_BUILD_ROOT/opt/mqm/mqxr/bin/endMQXRService.bat
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/bin/endMQXRService.sh $RPM_BUILD_ROOT/opt/mqm/mqxr/bin/endMQXRService.sh
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/bin/runMQXRService.bat $RPM_BUILD_ROOT/opt/mqm/mqxr/bin/runMQXRService.bat
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/bin/runMQXRService.sh $RPM_BUILD_ROOT/opt/mqm/mqxr/bin/runMQXRService.sh
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/config/jaas.config $RPM_BUILD_ROOT/opt/mqm/mqxr/config/jaas.config
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/config/java.properties $RPM_BUILD_ROOT/opt/mqm/mqxr/config/java.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/config/mqxr_unix.properties $RPM_BUILD_ROOT/opt/mqm/mqxr/config/mqxr_unix.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/config/mqxr_win.properties $RPM_BUILD_ROOT/opt/mqm/mqxr/config/mqxr_win.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/config/mqxrtrace.properties $RPM_BUILD_ROOT/opt/mqm/mqxr/config/mqxrtrace.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/config/trace.config $RPM_BUILD_ROOT/opt/mqm/mqxr/config/trace.config
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/lib/MQXRListener.jar $RPM_BUILD_ROOT/opt/mqm/mqxr/lib/MQXRListener.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/lib/com.ibm.micro.client.mqttv3.jar $RPM_BUILD_ROOT/opt/mqm/mqxr/lib/com.ibm.micro.client.mqttv3.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/lib/com.ibm.micro.client.mqttv3_7.5.0.2.jar $RPM_BUILD_ROOT/opt/mqm/mqxr/lib/com.ibm.micro.client.mqttv3_7.5.0.2.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/lib/com.ibm.micro.xr.jar $RPM_BUILD_ROOT/opt/mqm/mqxr/lib/com.ibm.micro.xr.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/lib/nonBlockingTrace.jar $RPM_BUILD_ROOT/opt/mqm/mqxr/lib/nonBlockingTrace.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/lib/objectManager.utils.jar $RPM_BUILD_ROOT/opt/mqm/mqxr/lib/objectManager.utils.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/samples/CleanupMQM.bat $RPM_BUILD_ROOT/opt/mqm/mqxr/samples/CleanupMQM.bat
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/samples/CleanupMQM.sh $RPM_BUILD_ROOT/opt/mqm/mqxr/samples/CleanupMQM.sh
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/samples/JAASLoginModule.java $RPM_BUILD_ROOT/opt/mqm/mqxr/samples/JAASLoginModule.java
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/samples/JAASPrincipal.java $RPM_BUILD_ROOT/opt/mqm/mqxr/samples/JAASPrincipal.java
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/samples/MQTTV3Sample.java $RPM_BUILD_ROOT/opt/mqm/mqxr/samples/MQTTV3Sample.java
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/samples/RunMQTTV3Sample.bat $RPM_BUILD_ROOT/opt/mqm/mqxr/samples/RunMQTTV3Sample.bat
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/samples/RunMQTTV3Sample.sh $RPM_BUILD_ROOT/opt/mqm/mqxr/samples/RunMQTTV3Sample.sh
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/samples/SampleMQM.bat $RPM_BUILD_ROOT/opt/mqm/mqxr/samples/SampleMQM.bat
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/samples/SampleMQM.sh $RPM_BUILD_ROOT/opt/mqm/mqxr/samples/SampleMQM.sh
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/samples/TDD/amqtdd.cfg $RPM_BUILD_ROOT/opt/mqm/mqxr/samples/TDD/amqtdd.cfg
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/samples/TDD/installMQTDDService_unix.mqsc $RPM_BUILD_ROOT/opt/mqm/mqxr/samples/TDD/installMQTDDService_unix.mqsc
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/samples/TDD/installMQTDDService_win.mqsc $RPM_BUILD_ROOT/opt/mqm/mqxr/samples/TDD/installMQTDDService_win.mqsc
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/samples/installMQXRService_unix.mqsc $RPM_BUILD_ROOT/opt/mqm/mqxr/samples/installMQXRService_unix.mqsc
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/samples/installMQXRService_win.mqsc $RPM_BUILD_ROOT/opt/mqm/mqxr/samples/installMQXRService_win.mqsc
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/samples/samples/JAASLoginModule.class $RPM_BUILD_ROOT/opt/mqm/mqxr/samples/samples/JAASLoginModule.class
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/samples/samples/JAASPrincipal.class $RPM_BUILD_ROOT/opt/mqm/mqxr/samples/samples/JAASPrincipal.class
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/samples/samples/MQTTV3Sample.class $RPM_BUILD_ROOT/opt/mqm/mqxr/samples/samples/MQTTV3Sample.class
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/samples/scraper/ScraperService\$Feed.class $RPM_BUILD_ROOT/opt/mqm/mqxr/samples/scraper/ScraperService\$Feed.class
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/samples/scraper/ScraperService.class $RPM_BUILD_ROOT/opt/mqm/mqxr/samples/scraper/ScraperService.class
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/samples/scraper/ScraperService.java $RPM_BUILD_ROOT/opt/mqm/mqxr/samples/scraper/ScraperService.java
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/samples/scraper/installScraperService_unix.mqsc $RPM_BUILD_ROOT/opt/mqm/mqxr/samples/scraper/installScraperService_unix.mqsc
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/samples/scraper/installScraperService_win.mqsc $RPM_BUILD_ROOT/opt/mqm/mqxr/samples/scraper/installScraperService_win.mqsc
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/samples/scraper/runScraperService.bat $RPM_BUILD_ROOT/opt/mqm/mqxr/samples/scraper/runScraperService.bat
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/samples/scraper/runScraperService.sh $RPM_BUILD_ROOT/opt/mqm/mqxr/samples/scraper/runScraperService.sh

%files
%dir %attr(555,mqm,mqm) "/opt/mqm"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqxr"
%dir %attr(555,mqm,mqm) "/opt/mqm/properties"
%dir %attr(555,mqm,mqm) "/opt/mqm/properties/version"
%dir %attr(550,mqm,mqm) "/opt/mqm/mqxr/bin"
%dir %attr(550,mqm,mqm) "/opt/mqm/mqxr/config"
%dir %attr(550,mqm,mqm) "/opt/mqm/mqxr/lib"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqxr/samples"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqxr/samples/TDD"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqxr/samples/samples"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqxr/samples/scraper"
%attr(444,mqm,mqm) "/opt/mqm/properties/version/IBM_WebSphere_MQ_Telemetry_Service.7.5.0.swtag"
%attr(444,mqm,mqm) "/opt/mqm/mqxr/bin/controlMQXR.bat"
%attr(444,mqm,mqm) "/opt/mqm/mqxr/bin/controlMQXRChannel.bat"
%attr(550,mqm,mqm) "/opt/mqm/mqxr/bin/controlMQXRChannel.sh"
%attr(550,mqm,mqm) "/opt/mqm/mqxr/bin/controlMQXR_mqm.sh"
%attr(444,mqm,mqm) "/opt/mqm/mqxr/bin/endMQXRService.bat"
%attr(550,mqm,mqm) "/opt/mqm/mqxr/bin/endMQXRService.sh"
%attr(444,mqm,mqm) "/opt/mqm/mqxr/bin/runMQXRService.bat"
%attr(550,mqm,mqm) "/opt/mqm/mqxr/bin/runMQXRService.sh"
%attr(440,mqm,mqm) "/opt/mqm/mqxr/config/jaas.config"
%attr(440,mqm,mqm) "/opt/mqm/mqxr/config/java.properties"
%attr(440,mqm,mqm) "/opt/mqm/mqxr/config/mqxr_unix.properties"
%attr(440,mqm,mqm) "/opt/mqm/mqxr/config/mqxr_win.properties"
%attr(440,mqm,mqm) "/opt/mqm/mqxr/config/mqxrtrace.properties"
%attr(440,mqm,mqm) "/opt/mqm/mqxr/config/trace.config"
%attr(440,mqm,mqm) "/opt/mqm/mqxr/lib/MQXRListener.jar"
%attr(440,mqm,mqm) "/opt/mqm/mqxr/lib/com.ibm.micro.client.mqttv3.jar"
%attr(440,mqm,mqm) "/opt/mqm/mqxr/lib/com.ibm.micro.client.mqttv3_7.5.0.2.jar"
%attr(440,mqm,mqm) "/opt/mqm/mqxr/lib/com.ibm.micro.xr.jar"
%attr(440,mqm,mqm) "/opt/mqm/mqxr/lib/nonBlockingTrace.jar"
%attr(440,mqm,mqm) "/opt/mqm/mqxr/lib/objectManager.utils.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqxr/samples/CleanupMQM.bat"
%attr(555,mqm,mqm) "/opt/mqm/mqxr/samples/CleanupMQM.sh"
%attr(444,mqm,mqm) "/opt/mqm/mqxr/samples/JAASLoginModule.java"
%attr(444,mqm,mqm) "/opt/mqm/mqxr/samples/JAASPrincipal.java"
%attr(444,mqm,mqm) "/opt/mqm/mqxr/samples/MQTTV3Sample.java"
%attr(444,mqm,mqm) "/opt/mqm/mqxr/samples/RunMQTTV3Sample.bat"
%attr(555,mqm,mqm) "/opt/mqm/mqxr/samples/RunMQTTV3Sample.sh"
%attr(444,mqm,mqm) "/opt/mqm/mqxr/samples/SampleMQM.bat"
%attr(555,mqm,mqm) "/opt/mqm/mqxr/samples/SampleMQM.sh"
%attr(444,mqm,mqm) "/opt/mqm/mqxr/samples/TDD/amqtdd.cfg"
%attr(444,mqm,mqm) "/opt/mqm/mqxr/samples/TDD/installMQTDDService_unix.mqsc"
%attr(444,mqm,mqm) "/opt/mqm/mqxr/samples/TDD/installMQTDDService_win.mqsc"
%attr(444,mqm,mqm) "/opt/mqm/mqxr/samples/installMQXRService_unix.mqsc"
%attr(444,mqm,mqm) "/opt/mqm/mqxr/samples/installMQXRService_win.mqsc"
%attr(444,mqm,mqm) "/opt/mqm/mqxr/samples/samples/JAASLoginModule.class"
%attr(444,mqm,mqm) "/opt/mqm/mqxr/samples/samples/JAASPrincipal.class"
%attr(444,mqm,mqm) "/opt/mqm/mqxr/samples/samples/MQTTV3Sample.class"
%attr(444,mqm,mqm) "/opt/mqm/mqxr/samples/scraper/ScraperService$Feed.class"
%attr(444,mqm,mqm) "/opt/mqm/mqxr/samples/scraper/ScraperService.class"
%attr(444,mqm,mqm) "/opt/mqm/mqxr/samples/scraper/ScraperService.java"
%attr(444,mqm,mqm) "/opt/mqm/mqxr/samples/scraper/installScraperService_unix.mqsc"
%attr(444,mqm,mqm) "/opt/mqm/mqxr/samples/scraper/installScraperService_win.mqsc"
%attr(444,mqm,mqm) "/opt/mqm/mqxr/samples/scraper/runScraperService.bat"
%attr(555,mqm,mqm) "/opt/mqm/mqxr/samples/scraper/runScraperService.sh"

%pre
RPM_PACKAGE_SUMMARY="WebSphere MQ Telemetry Service"
RPM_PACKAGE_NAME="MQSeriesXRService"
RPM_PACKAGE_VERSION="7.5.0"
RPM_PACKAGE_RELEASE="2"
PACKAGEPLATFORMS="x86_64"
MQ_DEFAULT_INSTALLATION_PATH=/opt/mqm
MQ_INSTALLATION_PATH=${RPM_INSTALL_PREFIX}
MQ_RUNSCRIPT=
echo ${SHELLOPTS} | grep xtrace > /dev/null
if [ $? -eq 0 ] ; then 
  env | sort
  uname -a
  id
  MQ_RUNSCRIPT="sh -x"
fi

# @(#) MQMBID sn=p750-002-130704 su=_UmspIOSZEeK1oKoKL_dPJA pn=install/unix/linux_2/preinstall.sh
#   <copyright 
#   notice="lm-source-program" 
#   pids="5724-H72," 
#   years="2005,2012" 
#   crc="2186337890" > 
#  Licensed Materials - Property of IBM  
#   
#  5724-H72, 
#   
#  (C) Copyright IBM Corp. 2005, 2012 All Rights Reserved.  
#   
#  US Government Users Restricted Rights - Use, duplication or  
#  disclosure restricted by GSA ADP Schedule Contract with  
#  IBM Corp.  
#   </copyright> 
# Preinstallation script
# Checks to see if the mqm userid and groups exists, and if now will create
# 
# Check that this package is not being installed to a location where 
# a different VR exists
# 

# Set up environment variables
TMPDIR="/tmp"
RC=0
DUMMY=""

create_mqm_group()
{
  echo "Creating group mqm"
  ErrorText=`groupadd mqm 2>&1`
  RC=$?
  if [ $RC -ne 0 ]; then
    echo "ERROR:  Failed to add 'mqm' group:" >&2
    echo $ErrorText >&2
    exit $RC
  fi
}
# Determine if the group exists on the system.  The solution used here is to
# create a file in the temporary location, attempt to change its group id to
# mqm.  This method should work independent of the multitude of ways groups can
# be defined across distributions and network environments
TMPFILE="${TMPDIR}/amq_grouptest"
touch $TMPFILE 2>/dev/null
if [ -f "$TMPFILE" ]; then
  chown :mqm $TMPFILE 2>/dev/null
  if [ $? -ne 0 ]; then
    create_mqm_group
  fi
  rm -f $TMPFILE
else  # Unable to create temporary file.  This is also needed by other install
      # processes - so it is fair to abort the install at this point
  echo "ERROR: Unable to write to ${TMPDIR} - aborting install"
  exit 1
fi

# Determine if the user id "mqm" exists, and is in the group mqm
ErrorText=`id -u mqm 2>&1`
RC=$?
if [ $RC -ne 0 ]; then
  echo "Creating user mqm"
  ErrorText=`useradd -r -d /var/mqm -g mqm mqm 2>&1`
  RC=$?
  if [ $RC -ne 0 ]; then
    echo "ERROR:  Failed to add 'mqm' user:" >&2
    echo $ErrorText >&2
    exit $RC
  fi
else    # Check the user mqm is in the group mqm
  ErrorText=`id -Gn mqm | grep -w mqm 2>&1`
  if [ $? -ne 0 ]; then
    ErrorText=`usermod -G mqm mqm 2>&1`
    if [ $RC -ne 0 ]; then
      echo "ERROR:  Failed to add user mqm to group mqm:" >&2
      echo $ErrorText >&1
      echo $RC
    fi
  fi
fi

#######################################################################################################
# Check the install path does not exceed the Websphere MQ maximum length of 256
#######################################################################################################
if [ ${#MQ_INSTALLATION_PATH} -gt 256 ]; then
  echo ""
  echo "ERROR:   Specified path exceeds WebSphere MQ maximum length of 256"
  echo ""
  exit 1
fi

#######################################################################################################
# Check the install path does not contain unsupported charaters
#######################################################################################################
echo ${MQ_INSTALLATION_PATH} | grep "[:%#]" > /dev/null
if [ $? -eq 0 ] ; then
  echo ""
  echo "ERROR:   Specified path contains an unsupported character"
  echo ""
  exit 1
fi

#######################################################################################################
# Runtime checks
#######################################################################################################
echo ${RPM_PACKAGE_NAME} | grep  "MQSeriesRuntime" > /dev/null
if [ $? -eq 0 ] ; then
  #####################################################################################################
  # Check that the install path is empty
  #####################################################################################################
  if [ -d ${MQ_INSTALLATION_PATH} ] ; then
    LS_ALL=`ls -A ${MQ_INSTALLATION_PATH}`
    if [ "${LS_ALL}" ] && [ "${LS_ALL}" != "lost+found" ] ; then
      echo ""
      echo "ERROR:   Specified path is not empty"
      echo ""
      exit 1
    fi
  fi
#######################################################################################################
# Non Runtime checks
#######################################################################################################
else
  #####################################################################################################
  # Check the version/release of the product already at MQ_INSTALLATION_PATH is the same as this one
  #####################################################################################################
  if [ -x ${MQ_INSTALLATION_PATH}/bin/dspmqver ] ; then
    INSTALLED_VR=$(${MQ_INSTALLATION_PATH}/bin/dspmqver -f2 -b | awk -F. '{print $1 "." $2}')
    PACKAGE_VR=`echo ${RPM_PACKAGE_VERSION} | awk -F. '{print $1 "." $2}'`
    if [ ${INSTALLED_VR} != ${PACKAGE_VR} ] ; then
      echo ""
      echo "ERROR:   This package is not applicable to the WebSphere MQ installation at ${MQ_INSTALLATION_PATH}"
      echo ""
      exit 1
    fi
  else
    echo ""
    echo "ERROR:   There is no MQSeriesRuntime installed at ${MQ_INSTALLATION_PATH}"
    echo ""
    exit 1
  fi
fi

#   <copyright 
#   notice="lm-source-program" 
#   pids="5724-H72," 
#   years="2005,2012" 
#   crc="1114153681" > 
#  Licensed Materials - Property of IBM  
#   
#  5724-H72, 
#   
#  (C) Copyright IBM Corp. 2005, 2012 All Rights Reserved.  
#   
#  US Government Users Restricted Rights - Use, duplication or  
#  disclosure restricted by GSA ADP Schedule Contract with  
#  IBM Corp.  
#   </copyright> 
# Preinstallation script
# Check's to see if the license agreement has been accepted

if [ ! -r /tmp/mq_license_${RPM_PACKAGE_VERSION}/license/status.dat ] && [ ! -r ${MQ_INSTALLATION_PATH}/licenses/status.dat ] ; then

    cat << +++EOM+++
ERROR:  Product cannot be installed until the license
        agreement has been accepted.
        Run the 'mqlicense' script, which is in the root
        directory of the install media, or see the
        installation instructions in the product 
        Infocenter for more information
+++EOM+++

   exit 1
fi

# @(#) MQMBID sn=p750-002-130704 su=_UmspIOSZEeK1oKoKL_dPJA pn=install/unix/linux_2/xrservice_preinstall.sh
# Pre-install script for the mqxr package
# This script checks whether xr701 is installed using the existence
# of a directory specific to that release to do so. The user must uninstall
# 7.0.1 before installing 7.1 in the same location if this is the default
# location.
#   <copyright 
#   notice="lm-source-program" 
#   pids="5724-H72," 
#   years="2010,2012" 
#   crc="608018138" > 
#  Licensed Materials - Property of IBM  
#   
#  5724-H72, 
#   
#  (C) Copyright IBM Corp. 2010, 2012 All Rights Reserved.  
#   
#  US Government Users Restricted Rights - Use, duplication or  
#  disclosure restricted by GSA ADP Schedule Contract with  
#  IBM Corp.  
#   </copyright> 
MQXR_ROOT=${MQ_INSTALLATION_PATH}/mqxr
if [ \( ${MQ_INSTALLATION_PATH} = ${MQ_DEFAULT_INSTALLATION_PATH} \) -a  \( -x ${MQXR_ROOT}/license \) ]
then
 echo "You appear to have WebSphere MQ Telemetry v.7.0.1 installed \
 at the following location: ${MQXR_ROOT}."
 echo "Please uninstall it before continuing with this installation."
 exit 1
fi
echo > /dev/null 2>/dev/null

%post
RPM_PACKAGE_SUMMARY="WebSphere MQ Telemetry Service"
RPM_PACKAGE_NAME="MQSeriesXRService"
RPM_PACKAGE_VERSION="7.5.0"
RPM_PACKAGE_RELEASE="2"
PACKAGEPLATFORMS="x86_64"
MQ_DEFAULT_INSTALLATION_PATH=/opt/mqm
MQ_INSTALLATION_PATH=${RPM_INSTALL_PREFIX}
MQ_RUNSCRIPT=
echo ${SHELLOPTS} | grep xtrace > /dev/null
if [ $? -eq 0 ] ; then 
  env | sort
  uname -a
  id
  MQ_RUNSCRIPT="sh -x"
fi
if [ ${MQ_INSTALLATION_PATH} !=  ${MQ_DEFAULT_INSTALLATION_PATH} ] ; then 
  if [ -x ${MQ_INSTALLATION_PATH}/bin/amqicrel ] ; then 
     ${MQ_RUNSCRIPT} ${MQ_INSTALLATION_PATH}/bin/amqicrel ${MQ_INSTALLATION_PATH} ${RPM_PACKAGE_NAME}-${RPM_PACKAGE_VERSION}-${RPM_PACKAGE_RELEASE}
  fi
fi

%preun
RPM_PACKAGE_SUMMARY="WebSphere MQ Telemetry Service"
RPM_PACKAGE_NAME="MQSeriesXRService"
RPM_PACKAGE_VERSION="7.5.0"
RPM_PACKAGE_RELEASE="2"
PACKAGEPLATFORMS="x86_64"
MQ_DEFAULT_INSTALLATION_PATH=/opt/mqm
MQ_INSTALLATION_PATH=${RPM_INSTALL_PREFIX}
MQ_RUNSCRIPT=
echo ${SHELLOPTS} | grep xtrace > /dev/null
if [ $? -eq 0 ] ; then 
  env | sort
  uname -a
  id
  MQ_RUNSCRIPT="sh -x"
fi

#   <copyright 
#   notice="lm-source-program" 
#   pids="5724-H72," 
#   years="2005,2012" 
#   crc="122768040" > 
#  Licensed Materials - Property of IBM  
#   
#  5724-H72, 
#   
#  (C) Copyright IBM Corp. 2005, 2012 All Rights Reserved.  
#   
#  US Government Users Restricted Rights - Use, duplication or  
#  disclosure restricted by GSA ADP Schedule Contract with  
#  IBM Corp.  
#   </copyright> 
#
# Pre-uninstallation script
# Checks for already running Q Managers, and if it finds one, stops the
# uninstall.

# If amqiclen exists (should do during uninstall) then run it to clean up
# IPCC resources. If amqiclen returns an error then a queue manager is still
# running so stop the uninstall.
if [ -x ${MQ_INSTALLATION_PATH}/bin/amqiclen ] && [ -f /var/mqm/mqs.ini ]
then
    ${MQ_INSTALLATION_PATH}/bin/amqiclen -x -v > /tmp/amqiclen.$$.out 2>&1
    amqiclen_rc=$?
    if [ $amqiclen_rc -ne 0 ]
    then
      echo "ERROR:    WebSphere MQ shared resources for this installation are still in use." >&2
      echo "          It is likely that one or more queue managers are still running." >&2
      echo "          Return code from from amqiclen was \"$amqiclen_rc\", output was:" >&2
      cat /tmp/amqiclen.$$.out >&2
      rm /tmp/amqiclen.$$.out
      exit 1
    fi
    rm /tmp/amqiclen.$$.out
fi

#   <copyright 
#   notice="lm-source-program" 
#   pids="5724-H72," 
#   years="2005,2012" 
#   crc="1595222582" > 
#  Licensed Materials - Property of IBM  
#   
#  5724-H72, 
#   
#  (C) Copyright IBM Corp. 2005, 2012 All Rights Reserved.  
#   
#  US Government Users Restricted Rights - Use, duplication or  
#  disclosure restricted by GSA ADP Schedule Contract with  
#  IBM Corp.  
#   </copyright> 
#
# Pre-uninstallation check script for all components
# A check is performed to see if there are any fixpack filesets applied to
# the base component which is currently being uninstalled.  If the fixpack
# has been applied, the uninstallation of this component is aborted to prevent
# the situation where the base fileset has been uninstalled leaving an
# uninstallable fixpack.

FIXPACK_BACKUPDIR="${MQ_INSTALLATION_PATH}/maintenance"

find $FIXPACK_BACKUPDIR -type d -maxdepth 2 -print 2>/dev/null | \
      while read directory ; do
  component=`basename $directory`
  if [ "$RPM_PACKAGE_NAME" = "$component" ]; then
    # safety check - are there actually files under this directory?
    num_files=`find "$directory" -type f -print 2>/dev/null | wc -l`
    if [ $num_files -gt 0 ]; then
      cat << EOF

  ERROR:  There appears to be a fixpack installed on this machine for this
          component.

  Please ensure you have removed all fixpacks for the \"$RPM_PACKAGE_NAME\"
  component before trying to remove this package.

EOF
      exit 1
    fi
  fi
done

# Removing product links

%postun
RPM_PACKAGE_SUMMARY="WebSphere MQ Telemetry Service"
RPM_PACKAGE_NAME="MQSeriesXRService"
RPM_PACKAGE_VERSION="7.5.0"
RPM_PACKAGE_RELEASE="2"
PACKAGEPLATFORMS="x86_64"
MQ_DEFAULT_INSTALLATION_PATH=/opt/mqm
MQ_INSTALLATION_PATH=${RPM_INSTALL_PREFIX}
MQ_RUNSCRIPT=
echo ${SHELLOPTS} | grep xtrace > /dev/null
if [ $? -eq 0 ] ; then 
  env | sort
  uname -a
  id
  MQ_RUNSCRIPT="sh -x"
fi

