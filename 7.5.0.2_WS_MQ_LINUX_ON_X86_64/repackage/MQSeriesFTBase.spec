Summary: WebSphere MQ Managed File Transfer Base Component
Name: MQSeriesFTBase
Version: 7.5.0
Release: 2
License: Commercial
ExclusiveArch: x86_64
Group: Applications/Networking
AutoReqProv: no
Vendor: IBM
Prefix: /opt/mqm
Requires: MQSeriesRuntime = 7.5.0-2
Requires: MQSeriesJava = 7.5.0-2
Requires: MQSeriesJRE = 7.5.0-2
%global __strip /bin/true
%global _rpmdir /build/slot1/p750_P/inst.images/amd64_linux_2/images/
%global _tmppath /build/slot1/p750_P/obj/amd64_linux_2/install/unix/linux_2
BuildRoot: /build/slot1/p750_P/obj/amd64_linux_2/ship

%description
IBM WebSphere MQ for Developers for Linux for x86_64
5724-H72
%clean
rm -rf $RPM_BUILD_ROOT

%install
install -d $RPM_BUILD_ROOT/opt/mqm
install -d $RPM_BUILD_ROOT/opt/mqm/properties
install -d $RPM_BUILD_ROOT/opt/mqm/properties/version
install -d $RPM_BUILD_ROOT/opt/mqm/bin
install -d $RPM_BUILD_ROOT/opt/mqm/mqft
install -d $RPM_BUILD_ROOT/opt/mqm/mqft/ant
install -d $RPM_BUILD_ROOT/opt/mqm/mqft/ant/bin
install -d $RPM_BUILD_ROOT/opt/mqm/mqft/ant/etc
install -d $RPM_BUILD_ROOT/opt/mqm/mqft/ant/etc/checkstyle
install -d $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib
install -d $RPM_BUILD_ROOT/opt/mqm/mqft/lib
install -d $RPM_BUILD_ROOT/opt/mqm/mqft/lib/messages
install -d $RPM_BUILD_ROOT/opt/mqm/mqft/lib64
install -d $RPM_BUILD_ROOT/opt/mqm/mqft/samples
install -d $RPM_BUILD_ROOT/opt/mqm/mqft/samples/4690
install -d $RPM_BUILD_ROOT/opt/mqm/mqft/samples/credentials
install -d $RPM_BUILD_ROOT/opt/mqm/mqft/samples/fteant
install -d $RPM_BUILD_ROOT/opt/mqm/mqft/samples/fteant/email
install -d $RPM_BUILD_ROOT/opt/mqm/mqft/samples/fteant/hub
install -d $RPM_BUILD_ROOT/opt/mqm/mqft/samples/fteant/timeout
install -d $RPM_BUILD_ROOT/opt/mqm/mqft/samples/fteant/zip
install -d $RPM_BUILD_ROOT/opt/mqm/mqft/samples/schema
install -d $RPM_BUILD_ROOT/opt/mqm/mqft/samples/schema/7.0.4
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/properties/version/IBM_WebSphere_MQ_Managed_File_Transfer_Base.7.5.0.cmptag $RPM_BUILD_ROOT/opt/mqm/properties/version/IBM_WebSphere_MQ_Managed_File_Transfer_Base.7.5.0.cmptag
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/INSTALL $RPM_BUILD_ROOT/opt/mqm/mqft/ant/INSTALL
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/KEYS $RPM_BUILD_ROOT/opt/mqm/mqft/ant/KEYS
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/LICENSE $RPM_BUILD_ROOT/opt/mqm/mqft/ant/LICENSE
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/LICENSE.dom $RPM_BUILD_ROOT/opt/mqm/mqft/ant/LICENSE.dom
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/LICENSE.sax $RPM_BUILD_ROOT/opt/mqm/mqft/ant/LICENSE.sax
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/LICENSE.xerces $RPM_BUILD_ROOT/opt/mqm/mqft/ant/LICENSE.xerces
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/NOTICE $RPM_BUILD_ROOT/opt/mqm/mqft/ant/NOTICE
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/README $RPM_BUILD_ROOT/opt/mqm/mqft/ant/README
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/WHATSNEW $RPM_BUILD_ROOT/opt/mqm/mqft/ant/WHATSNEW
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/bin/ant $RPM_BUILD_ROOT/opt/mqm/mqft/ant/bin/ant
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/bin/ant.bat $RPM_BUILD_ROOT/opt/mqm/mqft/ant/bin/ant.bat
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/bin/ant.cmd $RPM_BUILD_ROOT/opt/mqm/mqft/ant/bin/ant.cmd
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/bin/antRun $RPM_BUILD_ROOT/opt/mqm/mqft/ant/bin/antRun
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/bin/antRun.bat $RPM_BUILD_ROOT/opt/mqm/mqft/ant/bin/antRun.bat
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/bin/antRun.pl $RPM_BUILD_ROOT/opt/mqm/mqft/ant/bin/antRun.pl
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/bin/antenv.cmd $RPM_BUILD_ROOT/opt/mqm/mqft/ant/bin/antenv.cmd
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/bin/complete-ant-cmd.pl $RPM_BUILD_ROOT/opt/mqm/mqft/ant/bin/complete-ant-cmd.pl
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/bin/envset.cmd $RPM_BUILD_ROOT/opt/mqm/mqft/ant/bin/envset.cmd
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/bin/lcp.bat $RPM_BUILD_ROOT/opt/mqm/mqft/ant/bin/lcp.bat
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/bin/runant.pl $RPM_BUILD_ROOT/opt/mqm/mqft/ant/bin/runant.pl
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/bin/runant.py $RPM_BUILD_ROOT/opt/mqm/mqft/ant/bin/runant.py
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/bin/runrc.cmd $RPM_BUILD_ROOT/opt/mqm/mqft/ant/bin/runrc.cmd
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/etc/ant-bootstrap.jar $RPM_BUILD_ROOT/opt/mqm/mqft/ant/etc/ant-bootstrap.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/etc/changelog.xsl $RPM_BUILD_ROOT/opt/mqm/mqft/ant/etc/changelog.xsl
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/etc/checkstyle/checkstyle-frames.xsl $RPM_BUILD_ROOT/opt/mqm/mqft/ant/etc/checkstyle/checkstyle-frames.xsl
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/etc/checkstyle/checkstyle-text.xsl $RPM_BUILD_ROOT/opt/mqm/mqft/ant/etc/checkstyle/checkstyle-text.xsl
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/etc/checkstyle/checkstyle-xdoc.xsl $RPM_BUILD_ROOT/opt/mqm/mqft/ant/etc/checkstyle/checkstyle-xdoc.xsl
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/etc/coverage-frames.xsl $RPM_BUILD_ROOT/opt/mqm/mqft/ant/etc/coverage-frames.xsl
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/etc/jdepend-frames.xsl $RPM_BUILD_ROOT/opt/mqm/mqft/ant/etc/jdepend-frames.xsl
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/etc/jdepend.xsl $RPM_BUILD_ROOT/opt/mqm/mqft/ant/etc/jdepend.xsl
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/etc/junit-frames-xalan1.xsl $RPM_BUILD_ROOT/opt/mqm/mqft/ant/etc/junit-frames-xalan1.xsl
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/etc/junit-frames.xsl $RPM_BUILD_ROOT/opt/mqm/mqft/ant/etc/junit-frames.xsl
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/etc/junit-noframes.xsl $RPM_BUILD_ROOT/opt/mqm/mqft/ant/etc/junit-noframes.xsl
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/etc/log.xsl $RPM_BUILD_ROOT/opt/mqm/mqft/ant/etc/log.xsl
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/etc/maudit-frames.xsl $RPM_BUILD_ROOT/opt/mqm/mqft/ant/etc/maudit-frames.xsl
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/etc/mmetrics-frames.xsl $RPM_BUILD_ROOT/opt/mqm/mqft/ant/etc/mmetrics-frames.xsl
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/etc/tagdiff.xsl $RPM_BUILD_ROOT/opt/mqm/mqft/ant/etc/tagdiff.xsl
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/fetch.xml $RPM_BUILD_ROOT/opt/mqm/mqft/ant/fetch.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/get-m2.xml $RPM_BUILD_ROOT/opt/mqm/mqft/ant/get-m2.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/README $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/README
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-1.7.0.pom $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-1.7.0.pom
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-1.7.0.pom.md5 $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-1.7.0.pom.md5
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-1.7.0.pom.sha1 $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-1.7.0.pom.sha1
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-antlr-1.7.0.pom $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-antlr-1.7.0.pom
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-antlr-1.7.0.pom.md5 $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-antlr-1.7.0.pom.md5
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-antlr-1.7.0.pom.sha1 $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-antlr-1.7.0.pom.sha1
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-antlr.jar $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-antlr.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-apache-bcel-1.7.0.pom $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-apache-bcel-1.7.0.pom
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-apache-bcel-1.7.0.pom.md5 $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-apache-bcel-1.7.0.pom.md5
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-apache-bcel-1.7.0.pom.sha1 $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-apache-bcel-1.7.0.pom.sha1
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-apache-bcel.jar $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-apache-bcel.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-apache-bsf-1.7.0.pom $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-apache-bsf-1.7.0.pom
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-apache-bsf-1.7.0.pom.md5 $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-apache-bsf-1.7.0.pom.md5
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-apache-bsf-1.7.0.pom.sha1 $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-apache-bsf-1.7.0.pom.sha1
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-apache-bsf.jar $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-apache-bsf.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-apache-log4j-1.7.0.pom $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-apache-log4j-1.7.0.pom
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-apache-log4j-1.7.0.pom.md5 $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-apache-log4j-1.7.0.pom.md5
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-apache-log4j-1.7.0.pom.sha1 $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-apache-log4j-1.7.0.pom.sha1
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-apache-log4j.jar $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-apache-log4j.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-apache-oro-1.7.0.pom $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-apache-oro-1.7.0.pom
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-apache-oro-1.7.0.pom.md5 $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-apache-oro-1.7.0.pom.md5
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-apache-oro-1.7.0.pom.sha1 $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-apache-oro-1.7.0.pom.sha1
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-apache-oro.jar $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-apache-oro.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-apache-regexp-1.7.0.pom $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-apache-regexp-1.7.0.pom
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-apache-regexp-1.7.0.pom.md5 $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-apache-regexp-1.7.0.pom.md5
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-apache-regexp-1.7.0.pom.sha1 $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-apache-regexp-1.7.0.pom.sha1
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-apache-regexp.jar $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-apache-regexp.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-apache-resolver-1.7.0.pom $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-apache-resolver-1.7.0.pom
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-apache-resolver-1.7.0.pom.md5 $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-apache-resolver-1.7.0.pom.md5
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-apache-resolver-1.7.0.pom.sha1 $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-apache-resolver-1.7.0.pom.sha1
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-apache-resolver.jar $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-apache-resolver.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-commons-logging-1.7.0.pom $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-commons-logging-1.7.0.pom
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-commons-logging-1.7.0.pom.md5 $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-commons-logging-1.7.0.pom.md5
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-commons-logging-1.7.0.pom.sha1 $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-commons-logging-1.7.0.pom.sha1
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-commons-logging.jar $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-commons-logging.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-commons-net-1.7.0.pom $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-commons-net-1.7.0.pom
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-commons-net-1.7.0.pom.md5 $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-commons-net-1.7.0.pom.md5
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-commons-net-1.7.0.pom.sha1 $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-commons-net-1.7.0.pom.sha1
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-commons-net.jar $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-commons-net.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-jai-1.7.0.pom $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-jai-1.7.0.pom
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-jai-1.7.0.pom.md5 $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-jai-1.7.0.pom.md5
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-jai-1.7.0.pom.sha1 $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-jai-1.7.0.pom.sha1
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-jai.jar $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-jai.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-javamail-1.7.0.pom $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-javamail-1.7.0.pom
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-javamail-1.7.0.pom.md5 $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-javamail-1.7.0.pom.md5
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-javamail-1.7.0.pom.sha1 $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-javamail-1.7.0.pom.sha1
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-javamail.jar $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-javamail.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-jdepend-1.7.0.pom $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-jdepend-1.7.0.pom
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-jdepend-1.7.0.pom.md5 $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-jdepend-1.7.0.pom.md5
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-jdepend-1.7.0.pom.sha1 $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-jdepend-1.7.0.pom.sha1
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-jdepend.jar $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-jdepend.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-jmf-1.7.0.pom $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-jmf-1.7.0.pom
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-jmf-1.7.0.pom.md5 $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-jmf-1.7.0.pom.md5
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-jmf-1.7.0.pom.sha1 $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-jmf-1.7.0.pom.sha1
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-jmf.jar $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-jmf.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-jsch-1.7.0.pom $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-jsch-1.7.0.pom
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-jsch-1.7.0.pom.md5 $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-jsch-1.7.0.pom.md5
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-jsch-1.7.0.pom.sha1 $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-jsch-1.7.0.pom.sha1
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-jsch.jar $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-jsch.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-junit-1.7.0.pom $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-junit-1.7.0.pom
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-junit-1.7.0.pom.md5 $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-junit-1.7.0.pom.md5
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-junit-1.7.0.pom.sha1 $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-junit-1.7.0.pom.sha1
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-junit.jar $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-junit.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-launcher-1.7.0.pom $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-launcher-1.7.0.pom
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-launcher-1.7.0.pom.md5 $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-launcher-1.7.0.pom.md5
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-launcher-1.7.0.pom.sha1 $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-launcher-1.7.0.pom.sha1
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-launcher.jar $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-launcher.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-netrexx-1.7.0.pom $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-netrexx-1.7.0.pom
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-netrexx-1.7.0.pom.md5 $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-netrexx-1.7.0.pom.md5
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-netrexx-1.7.0.pom.sha1 $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-netrexx-1.7.0.pom.sha1
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-netrexx.jar $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-netrexx.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-nodeps-1.7.0.pom $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-nodeps-1.7.0.pom
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-nodeps-1.7.0.pom.md5 $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-nodeps-1.7.0.pom.md5
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-nodeps-1.7.0.pom.sha1 $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-nodeps-1.7.0.pom.sha1
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-nodeps.jar $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-nodeps.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-parent-1.7.0.pom $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-parent-1.7.0.pom
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-parent-1.7.0.pom.md5 $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-parent-1.7.0.pom.md5
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-parent-1.7.0.pom.sha1 $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-parent-1.7.0.pom.sha1
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant.jar $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-starteam-1.7.0.pom $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-starteam-1.7.0.pom
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-starteam-1.7.0.pom.md5 $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-starteam-1.7.0.pom.md5
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-starteam-1.7.0.pom.sha1 $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-starteam-1.7.0.pom.sha1
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-starteam.jar $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-starteam.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-stylebook-1.7.0.pom $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-stylebook-1.7.0.pom
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-stylebook-1.7.0.pom.md5 $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-stylebook-1.7.0.pom.md5
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-stylebook-1.7.0.pom.sha1 $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-stylebook-1.7.0.pom.sha1
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-stylebook.jar $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-stylebook.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-swing-1.7.0.pom $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-swing-1.7.0.pom
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-swing-1.7.0.pom.md5 $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-swing-1.7.0.pom.md5
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-swing-1.7.0.pom.sha1 $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-swing-1.7.0.pom.sha1
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-swing.jar $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-swing.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-testutil-1.7.0.pom $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-testutil-1.7.0.pom
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-testutil-1.7.0.pom.md5 $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-testutil-1.7.0.pom.md5
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-testutil-1.7.0.pom.sha1 $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-testutil-1.7.0.pom.sha1
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-testutil.jar $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-testutil.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-trax-1.7.0.pom $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-trax-1.7.0.pom
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-trax-1.7.0.pom.md5 $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-trax-1.7.0.pom.md5
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-trax-1.7.0.pom.sha1 $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-trax-1.7.0.pom.sha1
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-trax.jar $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-trax.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-weblogic-1.7.0.pom $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-weblogic-1.7.0.pom
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-weblogic-1.7.0.pom.md5 $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-weblogic-1.7.0.pom.md5
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-weblogic-1.7.0.pom.sha1 $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-weblogic-1.7.0.pom.sha1
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/ant-weblogic.jar $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/ant-weblogic.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/libraries.properties $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/libraries.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/xercesImpl.jar $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/xercesImpl.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/ant/lib/xml-apis.jar $RPM_BUILD_ROOT/opt/mqm/mqft/ant/lib/xml-apis.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/lib/com.ibm.wmqfte.ant.jar $RPM_BUILD_ROOT/opt/mqm/mqft/lib/com.ibm.wmqfte.ant.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/lib/com.ibm.wmqfte.bootstrap.jar $RPM_BUILD_ROOT/opt/mqm/mqft/lib/com.ibm.wmqfte.bootstrap.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/lib/com.ibm.wmqfte.cmdline.jar $RPM_BUILD_ROOT/opt/mqm/mqft/lib/com.ibm.wmqfte.cmdline.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/lib/com.ibm.wmqfte.common.jar $RPM_BUILD_ROOT/opt/mqm/mqft/lib/com.ibm.wmqfte.common.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/lib/com.ibm.wmqfte.daemon.jar $RPM_BUILD_ROOT/opt/mqm/mqft/lib/com.ibm.wmqfte.daemon.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/lib/com.ibm.wmqfte.databaselogger.jar $RPM_BUILD_ROOT/opt/mqm/mqft/lib/com.ibm.wmqfte.databaselogger.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/lib/com.ibm.wmqfte.native.jni.jar $RPM_BUILD_ROOT/opt/mqm/mqft/lib/com.ibm.wmqfte.native.jni.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/lib/logging.properties $RPM_BUILD_ROOT/opt/mqm/mqft/lib/logging.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/lib/messages/BFGNVElements.properties $RPM_BUILD_ROOT/opt/mqm/mqft/lib/messages/BFGNVElements.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/lib/messages/BFGNVMessages.properties $RPM_BUILD_ROOT/opt/mqm/mqft/lib/messages/BFGNVMessages.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/lib/messages/BFGPCElements.properties $RPM_BUILD_ROOT/opt/mqm/mqft/lib/messages/BFGPCElements.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/lib/messages/BFGPCMessages.properties $RPM_BUILD_ROOT/opt/mqm/mqft/lib/messages/BFGPCMessages.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/lib64/libicudata.so.48 $RPM_BUILD_ROOT/opt/mqm/mqft/lib64/libicudata.so.48
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/lib64/libicui18n.so.48 $RPM_BUILD_ROOT/opt/mqm/mqft/lib64/libicui18n.so.48
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/lib64/libicuio.so.48 $RPM_BUILD_ROOT/opt/mqm/mqft/lib64/libicuio.so.48
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/lib64/libicule.so.48 $RPM_BUILD_ROOT/opt/mqm/mqft/lib64/libicule.so.48
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/lib64/libiculx.so.48 $RPM_BUILD_ROOT/opt/mqm/mqft/lib64/libiculx.so.48
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/lib64/libicutu.so.48 $RPM_BUILD_ROOT/opt/mqm/mqft/lib64/libicutu.so.48
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/lib64/libicuuc.so.48 $RPM_BUILD_ROOT/opt/mqm/mqft/lib64/libicuuc.so.48
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/lib64/libmqmft.so $RPM_BUILD_ROOT/opt/mqm/mqft/lib64/libmqmft.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/lib/commons-beanutils.jar $RPM_BUILD_ROOT/opt/mqm/mqft/lib/commons-beanutils.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/lib/commons-digester-1.8.jar $RPM_BUILD_ROOT/opt/mqm/mqft/lib/commons-digester-1.8.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/lib/commons-io-1.4.jar $RPM_BUILD_ROOT/opt/mqm/mqft/lib/commons-io-1.4.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/lib/commons-lang-2.4.jar $RPM_BUILD_ROOT/opt/mqm/mqft/lib/commons-lang-2.4.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/lib/commons-logging-1.1.1.jar $RPM_BUILD_ROOT/opt/mqm/mqft/lib/commons-logging-1.1.1.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/lib/commons-net-2.0.jar $RPM_BUILD_ROOT/opt/mqm/mqft/lib/commons-net-2.0.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/lib/icu4j-49_1.jar $RPM_BUILD_ROOT/opt/mqm/mqft/lib/icu4j-49_1.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/lib/icu4j-charset-49_1.jar $RPM_BUILD_ROOT/opt/mqm/mqft/lib/icu4j-charset-49_1.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/lib/icu4j-localespi-49_1.jar $RPM_BUILD_ROOT/opt/mqm/mqft/lib/icu4j-localespi-49_1.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/lib64/libmqmftpc.so $RPM_BUILD_ROOT/opt/mqm/mqft/lib64/libmqmftpc.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/lib64/mqmftpc $RPM_BUILD_ROOT/opt/mqm/mqft/lib64/mqmftpc
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/samples/4690/basic.zip $RPM_BUILD_ROOT/opt/mqm/mqft/samples/4690/basic.zip
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/samples/4690/custom1.zip $RPM_BUILD_ROOT/opt/mqm/mqft/samples/4690/custom1.zip
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/samples/4690/custom2.zip $RPM_BUILD_ROOT/opt/mqm/mqft/samples/4690/custom2.zip
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/samples/4690/SSL.zip $RPM_BUILD_ROOT/opt/mqm/mqft/samples/4690/SSL.zip
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/samples/credentials/MQMFTCredentials.xml $RPM_BUILD_ROOT/opt/mqm/mqft/samples/credentials/MQMFTCredentials.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/samples/fteant/email/email.xml $RPM_BUILD_ROOT/opt/mqm/mqft/samples/fteant/email/email.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/samples/fteant/hub/hubcopy.xml $RPM_BUILD_ROOT/opt/mqm/mqft/samples/fteant/hub/hubcopy.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/samples/fteant/hub/hubprocess.xml $RPM_BUILD_ROOT/opt/mqm/mqft/samples/fteant/hub/hubprocess.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/samples/fteant/timeout/timeout.xml $RPM_BUILD_ROOT/opt/mqm/mqft/samples/fteant/timeout/timeout.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/samples/fteant/zip/zip.xml $RPM_BUILD_ROOT/opt/mqm/mqft/samples/fteant/zip/zip.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/samples/fteant/zip/zipfiles.xml $RPM_BUILD_ROOT/opt/mqm/mqft/samples/fteant/zip/zipfiles.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/samples/schema/Trace.xsd $RPM_BUILD_ROOT/opt/mqm/mqft/samples/schema/Trace.xsd
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/samples/schema/AgentTraceStatus.xsd $RPM_BUILD_ROOT/opt/mqm/mqft/samples/schema/AgentTraceStatus.xsd
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/samples/schema/ConnectDirectCredentials.xsd $RPM_BUILD_ROOT/opt/mqm/mqft/samples/schema/ConnectDirectCredentials.xsd
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/samples/schema/ConnectDirectNodeProperties.xsd $RPM_BUILD_ROOT/opt/mqm/mqft/samples/schema/ConnectDirectNodeProperties.xsd
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/samples/schema/ConnectDirectProcessDefinitions.xsd $RPM_BUILD_ROOT/opt/mqm/mqft/samples/schema/ConnectDirectProcessDefinitions.xsd
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/samples/schema/FileLoggerFormat.xsd $RPM_BUILD_ROOT/opt/mqm/mqft/samples/schema/FileLoggerFormat.xsd
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/samples/schema/FileSpaceInfo.xsd $RPM_BUILD_ROOT/opt/mqm/mqft/samples/schema/FileSpaceInfo.xsd
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/samples/schema/FileTransfer.xsd $RPM_BUILD_ROOT/opt/mqm/mqft/samples/schema/FileTransfer.xsd
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/samples/schema/Filespace.xsd $RPM_BUILD_ROOT/opt/mqm/mqft/samples/schema/Filespace.xsd
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/samples/schema/Internal.xsd $RPM_BUILD_ROOT/opt/mqm/mqft/samples/schema/Internal.xsd
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/samples/schema/Monitor.xsd $RPM_BUILD_ROOT/opt/mqm/mqft/samples/schema/Monitor.xsd
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/samples/schema/MonitorList.xsd $RPM_BUILD_ROOT/opt/mqm/mqft/samples/schema/MonitorList.xsd
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/samples/schema/MonitorLog.xsd $RPM_BUILD_ROOT/opt/mqm/mqft/samples/schema/MonitorLog.xsd
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/samples/schema/MQMFTCredentials.xsd $RPM_BUILD_ROOT/opt/mqm/mqft/samples/schema/MQMFTCredentials.xsd
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/samples/schema/Notification.xsd $RPM_BUILD_ROOT/opt/mqm/mqft/samples/schema/Notification.xsd
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/samples/schema/PingAgent.xsd $RPM_BUILD_ROOT/opt/mqm/mqft/samples/schema/PingAgent.xsd
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/samples/schema/ProtocolBridgeCredentials.xsd $RPM_BUILD_ROOT/opt/mqm/mqft/samples/schema/ProtocolBridgeCredentials.xsd
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/samples/schema/ProtocolBridgeProperties.xsd $RPM_BUILD_ROOT/opt/mqm/mqft/samples/schema/ProtocolBridgeProperties.xsd
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/samples/schema/Reply.xsd $RPM_BUILD_ROOT/opt/mqm/mqft/samples/schema/Reply.xsd
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/samples/schema/ScheduleList.xsd $RPM_BUILD_ROOT/opt/mqm/mqft/samples/schema/ScheduleList.xsd
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/samples/schema/ScheduleLog.xsd $RPM_BUILD_ROOT/opt/mqm/mqft/samples/schema/ScheduleLog.xsd
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/samples/schema/TransferLog.xsd $RPM_BUILD_ROOT/opt/mqm/mqft/samples/schema/TransferLog.xsd
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/samples/schema/TransferStatus.xsd $RPM_BUILD_ROOT/opt/mqm/mqft/samples/schema/TransferStatus.xsd
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/samples/schema/UserInfo.xsd $RPM_BUILD_ROOT/opt/mqm/mqft/samples/schema/UserInfo.xsd
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/samples/schema/UserSandboxes.xsd $RPM_BUILD_ROOT/opt/mqm/mqft/samples/schema/UserSandboxes.xsd
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/samples/schema/WebFileSpaceList.xsd $RPM_BUILD_ROOT/opt/mqm/mqft/samples/schema/WebFileSpaceList.xsd
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/samples/schema/WebTransferStatus.xsd $RPM_BUILD_ROOT/opt/mqm/mqft/samples/schema/WebTransferStatus.xsd
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/samples/schema/fteutils.xjb $RPM_BUILD_ROOT/opt/mqm/mqft/samples/schema/fteutils.xjb
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/samples/schema/fteutils.xsd $RPM_BUILD_ROOT/opt/mqm/mqft/samples/schema/fteutils.xsd
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/samples/schema/7.0.4/ConnectDirectCredentials.xsd $RPM_BUILD_ROOT/opt/mqm/mqft/samples/schema/7.0.4/ConnectDirectCredentials.xsd
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/samples/schema/7.0.4/ConnectDirectNodeProperties.xsd $RPM_BUILD_ROOT/opt/mqm/mqft/samples/schema/7.0.4/ConnectDirectNodeProperties.xsd
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/samples/schema/7.0.4/ProtocolBridgeCredentials.xsd $RPM_BUILD_ROOT/opt/mqm/mqft/samples/schema/7.0.4/ProtocolBridgeCredentials.xsd
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqft/samples/schema/7.0.4/ProtocolBridgeProperties.xsd $RPM_BUILD_ROOT/opt/mqm/mqft/samples/schema/7.0.4/ProtocolBridgeProperties.xsd
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/fteChangeDefaultConfigurationOptions $RPM_BUILD_ROOT/opt/mqm/bin/fteChangeDefaultConfigurationOptions
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/fteCommon $RPM_BUILD_ROOT/opt/mqm/bin/fteCommon
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/fteDisplayVersion $RPM_BUILD_ROOT/opt/mqm/bin/fteDisplayVersion
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/fteMigrateConfigurationOptions $RPM_BUILD_ROOT/opt/mqm/bin/fteMigrateConfigurationOptions
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/fteObfuscate $RPM_BUILD_ROOT/opt/mqm/bin/fteObfuscate
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/ftePlatform $RPM_BUILD_ROOT/opt/mqm/bin/ftePlatform
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/fteSetupCommands $RPM_BUILD_ROOT/opt/mqm/bin/fteSetupCommands
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/fteSetupCoordination $RPM_BUILD_ROOT/opt/mqm/bin/fteSetupCoordination
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/fteListAgents $RPM_BUILD_ROOT/opt/mqm/bin/fteListAgents
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/fteShowAgentDetails $RPM_BUILD_ROOT/opt/mqm/bin/fteShowAgentDetails

%files
%dir %attr(555,mqm,mqm) "/opt/mqm"
%dir %attr(555,mqm,mqm) "/opt/mqm/properties"
%dir %attr(555,mqm,mqm) "/opt/mqm/properties/version"
%dir %attr(555,mqm,mqm) "/opt/mqm/bin"
%dir %attr(555,bin,bin) "/opt/mqm/mqft"
%dir %attr(555,bin,bin) "/opt/mqm/mqft/ant"
%dir %attr(555,bin,bin) "/opt/mqm/mqft/ant/bin"
%dir %attr(555,bin,bin) "/opt/mqm/mqft/ant/etc"
%dir %attr(555,bin,bin) "/opt/mqm/mqft/ant/etc/checkstyle"
%dir %attr(555,bin,bin) "/opt/mqm/mqft/ant/lib"
%dir %attr(555,bin,bin) "/opt/mqm/mqft/lib"
%dir %attr(555,bin,bin) "/opt/mqm/mqft/lib/messages"
%dir %attr(555,bin,bin) "/opt/mqm/mqft/lib64"
%dir %attr(555,bin,bin) "/opt/mqm/mqft/samples"
%dir %attr(555,bin,bin) "/opt/mqm/mqft/samples/4690"
%dir %attr(555,bin,bin) "/opt/mqm/mqft/samples/credentials"
%dir %attr(555,bin,bin) "/opt/mqm/mqft/samples/fteant"
%dir %attr(555,bin,bin) "/opt/mqm/mqft/samples/fteant/email"
%dir %attr(555,bin,bin) "/opt/mqm/mqft/samples/fteant/hub"
%dir %attr(555,bin,bin) "/opt/mqm/mqft/samples/fteant/timeout"
%dir %attr(555,bin,bin) "/opt/mqm/mqft/samples/fteant/zip"
%dir %attr(555,bin,bin) "/opt/mqm/mqft/samples/schema"
%dir %attr(555,bin,bin) "/opt/mqm/mqft/samples/schema/7.0.4"
%attr(444,mqm,mqm) "/opt/mqm/properties/version/IBM_WebSphere_MQ_Managed_File_Transfer_Base.7.5.0.cmptag"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/INSTALL"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/KEYS"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/LICENSE"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/LICENSE.dom"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/LICENSE.sax"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/LICENSE.xerces"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/NOTICE"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/README"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/WHATSNEW"
%attr(555,bin,bin) "/opt/mqm/mqft/ant/bin/ant"
%attr(555,bin,bin) "/opt/mqm/mqft/ant/bin/ant.bat"
%attr(555,bin,bin) "/opt/mqm/mqft/ant/bin/ant.cmd"
%attr(555,bin,bin) "/opt/mqm/mqft/ant/bin/antRun"
%attr(555,bin,bin) "/opt/mqm/mqft/ant/bin/antRun.bat"
%attr(555,bin,bin) "/opt/mqm/mqft/ant/bin/antRun.pl"
%attr(555,bin,bin) "/opt/mqm/mqft/ant/bin/antenv.cmd"
%attr(555,bin,bin) "/opt/mqm/mqft/ant/bin/complete-ant-cmd.pl"
%attr(555,bin,bin) "/opt/mqm/mqft/ant/bin/envset.cmd"
%attr(555,bin,bin) "/opt/mqm/mqft/ant/bin/lcp.bat"
%attr(555,bin,bin) "/opt/mqm/mqft/ant/bin/runant.pl"
%attr(555,bin,bin) "/opt/mqm/mqft/ant/bin/runant.py"
%attr(555,bin,bin) "/opt/mqm/mqft/ant/bin/runrc.cmd"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/etc/ant-bootstrap.jar"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/etc/changelog.xsl"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/etc/checkstyle/checkstyle-frames.xsl"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/etc/checkstyle/checkstyle-text.xsl"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/etc/checkstyle/checkstyle-xdoc.xsl"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/etc/coverage-frames.xsl"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/etc/jdepend-frames.xsl"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/etc/jdepend.xsl"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/etc/junit-frames-xalan1.xsl"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/etc/junit-frames.xsl"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/etc/junit-noframes.xsl"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/etc/log.xsl"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/etc/maudit-frames.xsl"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/etc/mmetrics-frames.xsl"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/etc/tagdiff.xsl"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/fetch.xml"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/get-m2.xml"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/README"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-1.7.0.pom"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-1.7.0.pom.md5"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-1.7.0.pom.sha1"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-antlr-1.7.0.pom"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-antlr-1.7.0.pom.md5"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-antlr-1.7.0.pom.sha1"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-antlr.jar"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-apache-bcel-1.7.0.pom"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-apache-bcel-1.7.0.pom.md5"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-apache-bcel-1.7.0.pom.sha1"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-apache-bcel.jar"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-apache-bsf-1.7.0.pom"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-apache-bsf-1.7.0.pom.md5"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-apache-bsf-1.7.0.pom.sha1"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-apache-bsf.jar"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-apache-log4j-1.7.0.pom"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-apache-log4j-1.7.0.pom.md5"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-apache-log4j-1.7.0.pom.sha1"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-apache-log4j.jar"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-apache-oro-1.7.0.pom"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-apache-oro-1.7.0.pom.md5"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-apache-oro-1.7.0.pom.sha1"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-apache-oro.jar"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-apache-regexp-1.7.0.pom"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-apache-regexp-1.7.0.pom.md5"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-apache-regexp-1.7.0.pom.sha1"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-apache-regexp.jar"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-apache-resolver-1.7.0.pom"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-apache-resolver-1.7.0.pom.md5"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-apache-resolver-1.7.0.pom.sha1"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-apache-resolver.jar"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-commons-logging-1.7.0.pom"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-commons-logging-1.7.0.pom.md5"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-commons-logging-1.7.0.pom.sha1"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-commons-logging.jar"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-commons-net-1.7.0.pom"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-commons-net-1.7.0.pom.md5"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-commons-net-1.7.0.pom.sha1"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-commons-net.jar"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-jai-1.7.0.pom"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-jai-1.7.0.pom.md5"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-jai-1.7.0.pom.sha1"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-jai.jar"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-javamail-1.7.0.pom"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-javamail-1.7.0.pom.md5"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-javamail-1.7.0.pom.sha1"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-javamail.jar"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-jdepend-1.7.0.pom"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-jdepend-1.7.0.pom.md5"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-jdepend-1.7.0.pom.sha1"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-jdepend.jar"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-jmf-1.7.0.pom"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-jmf-1.7.0.pom.md5"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-jmf-1.7.0.pom.sha1"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-jmf.jar"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-jsch-1.7.0.pom"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-jsch-1.7.0.pom.md5"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-jsch-1.7.0.pom.sha1"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-jsch.jar"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-junit-1.7.0.pom"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-junit-1.7.0.pom.md5"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-junit-1.7.0.pom.sha1"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-junit.jar"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-launcher-1.7.0.pom"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-launcher-1.7.0.pom.md5"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-launcher-1.7.0.pom.sha1"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-launcher.jar"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-netrexx-1.7.0.pom"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-netrexx-1.7.0.pom.md5"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-netrexx-1.7.0.pom.sha1"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-netrexx.jar"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-nodeps-1.7.0.pom"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-nodeps-1.7.0.pom.md5"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-nodeps-1.7.0.pom.sha1"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-nodeps.jar"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-parent-1.7.0.pom"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-parent-1.7.0.pom.md5"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-parent-1.7.0.pom.sha1"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant.jar"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-starteam-1.7.0.pom"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-starteam-1.7.0.pom.md5"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-starteam-1.7.0.pom.sha1"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-starteam.jar"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-stylebook-1.7.0.pom"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-stylebook-1.7.0.pom.md5"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-stylebook-1.7.0.pom.sha1"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-stylebook.jar"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-swing-1.7.0.pom"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-swing-1.7.0.pom.md5"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-swing-1.7.0.pom.sha1"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-swing.jar"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-testutil-1.7.0.pom"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-testutil-1.7.0.pom.md5"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-testutil-1.7.0.pom.sha1"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-testutil.jar"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-trax-1.7.0.pom"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-trax-1.7.0.pom.md5"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-trax-1.7.0.pom.sha1"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-trax.jar"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-weblogic-1.7.0.pom"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-weblogic-1.7.0.pom.md5"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-weblogic-1.7.0.pom.sha1"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/ant-weblogic.jar"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/libraries.properties"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/xercesImpl.jar"
%attr(444,bin,bin) "/opt/mqm/mqft/ant/lib/xml-apis.jar"
%attr(444,bin,bin) "/opt/mqm/mqft/lib/com.ibm.wmqfte.ant.jar"
%attr(444,bin,bin) "/opt/mqm/mqft/lib/com.ibm.wmqfte.bootstrap.jar"
%attr(444,bin,bin) "/opt/mqm/mqft/lib/com.ibm.wmqfte.cmdline.jar"
%attr(444,bin,bin) "/opt/mqm/mqft/lib/com.ibm.wmqfte.common.jar"
%attr(444,bin,bin) "/opt/mqm/mqft/lib/com.ibm.wmqfte.daemon.jar"
%attr(444,bin,bin) "/opt/mqm/mqft/lib/com.ibm.wmqfte.databaselogger.jar"
%attr(444,bin,bin) "/opt/mqm/mqft/lib/com.ibm.wmqfte.native.jni.jar"
%attr(444,bin,bin) "/opt/mqm/mqft/lib/logging.properties"
%attr(444,bin,bin) "/opt/mqm/mqft/lib/messages/BFGNVElements.properties"
%attr(444,bin,bin) "/opt/mqm/mqft/lib/messages/BFGNVMessages.properties"
%attr(444,bin,bin) "/opt/mqm/mqft/lib/messages/BFGPCElements.properties"
%attr(444,bin,bin) "/opt/mqm/mqft/lib/messages/BFGPCMessages.properties"
%attr(555,bin,bin) %verify(not md5 mtime) "/opt/mqm/mqft/lib64/libicudata.so.48"
%attr(555,bin,bin) %verify(not md5 mtime) "/opt/mqm/mqft/lib64/libicui18n.so.48"
%attr(555,bin,bin) %verify(not md5 mtime) "/opt/mqm/mqft/lib64/libicuio.so.48"
%attr(555,bin,bin) %verify(not md5 mtime) "/opt/mqm/mqft/lib64/libicule.so.48"
%attr(555,bin,bin) %verify(not md5 mtime) "/opt/mqm/mqft/lib64/libiculx.so.48"
%attr(555,bin,bin) %verify(not md5 mtime) "/opt/mqm/mqft/lib64/libicutu.so.48"
%attr(555,bin,bin) %verify(not md5 mtime) "/opt/mqm/mqft/lib64/libicuuc.so.48"
%attr(555,bin,bin) %verify(not md5 mtime) "/opt/mqm/mqft/lib64/libmqmft.so"
%attr(444,bin,bin) "/opt/mqm/mqft/lib/commons-beanutils.jar"
%attr(444,bin,bin) "/opt/mqm/mqft/lib/commons-digester-1.8.jar"
%attr(444,bin,bin) "/opt/mqm/mqft/lib/commons-io-1.4.jar"
%attr(444,bin,bin) "/opt/mqm/mqft/lib/commons-lang-2.4.jar"
%attr(444,bin,bin) "/opt/mqm/mqft/lib/commons-logging-1.1.1.jar"
%attr(444,bin,bin) "/opt/mqm/mqft/lib/commons-net-2.0.jar"
%attr(444,bin,bin) "/opt/mqm/mqft/lib/icu4j-49_1.jar"
%attr(444,bin,bin) "/opt/mqm/mqft/lib/icu4j-charset-49_1.jar"
%attr(444,bin,bin) "/opt/mqm/mqft/lib/icu4j-localespi-49_1.jar"
%attr(555,bin,bin) %verify(not md5 mtime) "/opt/mqm/mqft/lib64/libmqmftpc.so"
%attr(555,bin,bin) %verify(not md5 mtime) "/opt/mqm/mqft/lib64/mqmftpc"
%attr(444,bin,bin) "/opt/mqm/mqft/samples/4690/basic.zip"
%attr(444,bin,bin) "/opt/mqm/mqft/samples/4690/custom1.zip"
%attr(444,bin,bin) "/opt/mqm/mqft/samples/4690/custom2.zip"
%attr(444,bin,bin) "/opt/mqm/mqft/samples/4690/SSL.zip"
%attr(444,bin,bin) "/opt/mqm/mqft/samples/credentials/MQMFTCredentials.xml"
%attr(444,bin,bin) "/opt/mqm/mqft/samples/fteant/email/email.xml"
%attr(444,bin,bin) "/opt/mqm/mqft/samples/fteant/hub/hubcopy.xml"
%attr(444,bin,bin) "/opt/mqm/mqft/samples/fteant/hub/hubprocess.xml"
%attr(444,bin,bin) "/opt/mqm/mqft/samples/fteant/timeout/timeout.xml"
%attr(444,bin,bin) "/opt/mqm/mqft/samples/fteant/zip/zip.xml"
%attr(444,bin,bin) "/opt/mqm/mqft/samples/fteant/zip/zipfiles.xml"
%attr(444,bin,bin) "/opt/mqm/mqft/samples/schema/Trace.xsd"
%attr(444,bin,bin) "/opt/mqm/mqft/samples/schema/AgentTraceStatus.xsd"
%attr(444,bin,bin) "/opt/mqm/mqft/samples/schema/ConnectDirectCredentials.xsd"
%attr(444,bin,bin) "/opt/mqm/mqft/samples/schema/ConnectDirectNodeProperties.xsd"
%attr(444,bin,bin) "/opt/mqm/mqft/samples/schema/ConnectDirectProcessDefinitions.xsd"
%attr(444,bin,bin) "/opt/mqm/mqft/samples/schema/FileLoggerFormat.xsd"
%attr(444,bin,bin) "/opt/mqm/mqft/samples/schema/FileSpaceInfo.xsd"
%attr(444,bin,bin) "/opt/mqm/mqft/samples/schema/FileTransfer.xsd"
%attr(444,bin,bin) "/opt/mqm/mqft/samples/schema/Filespace.xsd"
%attr(444,bin,bin) "/opt/mqm/mqft/samples/schema/Internal.xsd"
%attr(444,bin,bin) "/opt/mqm/mqft/samples/schema/Monitor.xsd"
%attr(444,bin,bin) "/opt/mqm/mqft/samples/schema/MonitorList.xsd"
%attr(444,bin,bin) "/opt/mqm/mqft/samples/schema/MonitorLog.xsd"
%attr(444,bin,bin) "/opt/mqm/mqft/samples/schema/MQMFTCredentials.xsd"
%attr(444,bin,bin) "/opt/mqm/mqft/samples/schema/Notification.xsd"
%attr(444,bin,bin) "/opt/mqm/mqft/samples/schema/PingAgent.xsd"
%attr(444,bin,bin) "/opt/mqm/mqft/samples/schema/ProtocolBridgeCredentials.xsd"
%attr(444,bin,bin) "/opt/mqm/mqft/samples/schema/ProtocolBridgeProperties.xsd"
%attr(444,bin,bin) "/opt/mqm/mqft/samples/schema/Reply.xsd"
%attr(444,bin,bin) "/opt/mqm/mqft/samples/schema/ScheduleList.xsd"
%attr(444,bin,bin) "/opt/mqm/mqft/samples/schema/ScheduleLog.xsd"
%attr(444,bin,bin) "/opt/mqm/mqft/samples/schema/TransferLog.xsd"
%attr(444,bin,bin) "/opt/mqm/mqft/samples/schema/TransferStatus.xsd"
%attr(444,bin,bin) "/opt/mqm/mqft/samples/schema/UserInfo.xsd"
%attr(444,bin,bin) "/opt/mqm/mqft/samples/schema/UserSandboxes.xsd"
%attr(444,bin,bin) "/opt/mqm/mqft/samples/schema/WebFileSpaceList.xsd"
%attr(444,bin,bin) "/opt/mqm/mqft/samples/schema/WebTransferStatus.xsd"
%attr(444,bin,bin) "/opt/mqm/mqft/samples/schema/fteutils.xjb"
%attr(444,bin,bin) "/opt/mqm/mqft/samples/schema/fteutils.xsd"
%attr(444,bin,bin) "/opt/mqm/mqft/samples/schema/7.0.4/ConnectDirectCredentials.xsd"
%attr(444,bin,bin) "/opt/mqm/mqft/samples/schema/7.0.4/ConnectDirectNodeProperties.xsd"
%attr(444,bin,bin) "/opt/mqm/mqft/samples/schema/7.0.4/ProtocolBridgeCredentials.xsd"
%attr(444,bin,bin) "/opt/mqm/mqft/samples/schema/7.0.4/ProtocolBridgeProperties.xsd"
%attr(555,mqm,mqm) "/opt/mqm/bin/fteChangeDefaultConfigurationOptions"
%attr(555,mqm,mqm) "/opt/mqm/bin/fteCommon"
%attr(555,mqm,mqm) "/opt/mqm/bin/fteDisplayVersion"
%attr(555,mqm,mqm) "/opt/mqm/bin/fteMigrateConfigurationOptions"
%attr(555,mqm,mqm) "/opt/mqm/bin/fteObfuscate"
%attr(555,mqm,mqm) "/opt/mqm/bin/ftePlatform"
%attr(555,mqm,mqm) "/opt/mqm/bin/fteSetupCommands"
%attr(555,mqm,mqm) "/opt/mqm/bin/fteSetupCoordination"
%attr(555,mqm,mqm) "/opt/mqm/bin/fteListAgents"
%attr(555,mqm,mqm) "/opt/mqm/bin/fteShowAgentDetails"

%pre
RPM_PACKAGE_SUMMARY="WebSphere MQ Managed File Transfer Base Component"
RPM_PACKAGE_NAME="MQSeriesFTBase"
RPM_PACKAGE_VERSION="7.5.0"
RPM_PACKAGE_RELEASE="2"
PACKAGEPLATFORMS="x86_64"
MQ_DEFAULT_INSTALLATION_PATH=/opt/mqm
MQ_INSTALLATION_PATH=${RPM_INSTALL_PREFIX}
MQ_RUNSCRIPT=
echo ${SHELLOPTS} | grep xtrace > /dev/null
if [ $? -eq 0 ] ; then 
  env | sort
  uname -a
  id
  MQ_RUNSCRIPT="sh -x"
fi

# @(#) MQMBID sn=p750-002-130704 su=_UmspIOSZEeK1oKoKL_dPJA pn=install/unix/linux_2/preinstall.sh
#   <copyright 
#   notice="lm-source-program" 
#   pids="5724-H72," 
#   years="2005,2012" 
#   crc="2186337890" > 
#  Licensed Materials - Property of IBM  
#   
#  5724-H72, 
#   
#  (C) Copyright IBM Corp. 2005, 2012 All Rights Reserved.  
#   
#  US Government Users Restricted Rights - Use, duplication or  
#  disclosure restricted by GSA ADP Schedule Contract with  
#  IBM Corp.  
#   </copyright> 
# Preinstallation script
# Checks to see if the mqm userid and groups exists, and if now will create
# 
# Check that this package is not being installed to a location where 
# a different VR exists
# 

# Set up environment variables
TMPDIR="/tmp"
RC=0
DUMMY=""

create_mqm_group()
{
  echo "Creating group mqm"
  ErrorText=`groupadd mqm 2>&1`
  RC=$?
  if [ $RC -ne 0 ]; then
    echo "ERROR:  Failed to add 'mqm' group:" >&2
    echo $ErrorText >&2
    exit $RC
  fi
}
# Determine if the group exists on the system.  The solution used here is to
# create a file in the temporary location, attempt to change its group id to
# mqm.  This method should work independent of the multitude of ways groups can
# be defined across distributions and network environments
TMPFILE="${TMPDIR}/amq_grouptest"
touch $TMPFILE 2>/dev/null
if [ -f "$TMPFILE" ]; then
  chown :mqm $TMPFILE 2>/dev/null
  if [ $? -ne 0 ]; then
    create_mqm_group
  fi
  rm -f $TMPFILE
else  # Unable to create temporary file.  This is also needed by other install
      # processes - so it is fair to abort the install at this point
  echo "ERROR: Unable to write to ${TMPDIR} - aborting install"
  exit 1
fi

# Determine if the user id "mqm" exists, and is in the group mqm
ErrorText=`id -u mqm 2>&1`
RC=$?
if [ $RC -ne 0 ]; then
  echo "Creating user mqm"
  ErrorText=`useradd -r -d /var/mqm -g mqm mqm 2>&1`
  RC=$?
  if [ $RC -ne 0 ]; then
    echo "ERROR:  Failed to add 'mqm' user:" >&2
    echo $ErrorText >&2
    exit $RC
  fi
else    # Check the user mqm is in the group mqm
  ErrorText=`id -Gn mqm | grep -w mqm 2>&1`
  if [ $? -ne 0 ]; then
    ErrorText=`usermod -G mqm mqm 2>&1`
    if [ $RC -ne 0 ]; then
      echo "ERROR:  Failed to add user mqm to group mqm:" >&2
      echo $ErrorText >&1
      echo $RC
    fi
  fi
fi

#######################################################################################################
# Check the install path does not exceed the Websphere MQ maximum length of 256
#######################################################################################################
if [ ${#MQ_INSTALLATION_PATH} -gt 256 ]; then
  echo ""
  echo "ERROR:   Specified path exceeds WebSphere MQ maximum length of 256"
  echo ""
  exit 1
fi

#######################################################################################################
# Check the install path does not contain unsupported charaters
#######################################################################################################
echo ${MQ_INSTALLATION_PATH} | grep "[:%#]" > /dev/null
if [ $? -eq 0 ] ; then
  echo ""
  echo "ERROR:   Specified path contains an unsupported character"
  echo ""
  exit 1
fi

#######################################################################################################
# Runtime checks
#######################################################################################################
echo ${RPM_PACKAGE_NAME} | grep  "MQSeriesRuntime" > /dev/null
if [ $? -eq 0 ] ; then
  #####################################################################################################
  # Check that the install path is empty
  #####################################################################################################
  if [ -d ${MQ_INSTALLATION_PATH} ] ; then
    LS_ALL=`ls -A ${MQ_INSTALLATION_PATH}`
    if [ "${LS_ALL}" ] && [ "${LS_ALL}" != "lost+found" ] ; then
      echo ""
      echo "ERROR:   Specified path is not empty"
      echo ""
      exit 1
    fi
  fi
#######################################################################################################
# Non Runtime checks
#######################################################################################################
else
  #####################################################################################################
  # Check the version/release of the product already at MQ_INSTALLATION_PATH is the same as this one
  #####################################################################################################
  if [ -x ${MQ_INSTALLATION_PATH}/bin/dspmqver ] ; then
    INSTALLED_VR=$(${MQ_INSTALLATION_PATH}/bin/dspmqver -f2 -b | awk -F. '{print $1 "." $2}')
    PACKAGE_VR=`echo ${RPM_PACKAGE_VERSION} | awk -F. '{print $1 "." $2}'`
    if [ ${INSTALLED_VR} != ${PACKAGE_VR} ] ; then
      echo ""
      echo "ERROR:   This package is not applicable to the WebSphere MQ installation at ${MQ_INSTALLATION_PATH}"
      echo ""
      exit 1
    fi
  else
    echo ""
    echo "ERROR:   There is no MQSeriesRuntime installed at ${MQ_INSTALLATION_PATH}"
    echo ""
    exit 1
  fi
fi

#   <copyright 
#   notice="lm-source-program" 
#   pids="5724-H72," 
#   years="2005,2012" 
#   crc="1114153681" > 
#  Licensed Materials - Property of IBM  
#   
#  5724-H72, 
#   
#  (C) Copyright IBM Corp. 2005, 2012 All Rights Reserved.  
#   
#  US Government Users Restricted Rights - Use, duplication or  
#  disclosure restricted by GSA ADP Schedule Contract with  
#  IBM Corp.  
#   </copyright> 
# Preinstallation script
# Check's to see if the license agreement has been accepted

if [ ! -r /tmp/mq_license_${RPM_PACKAGE_VERSION}/license/status.dat ] && [ ! -r ${MQ_INSTALLATION_PATH}/licenses/status.dat ] ; then

    cat << +++EOM+++
ERROR:  Product cannot be installed until the license
        agreement has been accepted.
        Run the 'mqlicense' script, which is in the root
        directory of the install media, or see the
        installation instructions in the product 
        Infocenter for more information
+++EOM+++

   exit 1
fi

echo > /dev/null 2>/dev/null

%post
RPM_PACKAGE_SUMMARY="WebSphere MQ Managed File Transfer Base Component"
RPM_PACKAGE_NAME="MQSeriesFTBase"
RPM_PACKAGE_VERSION="7.5.0"
RPM_PACKAGE_RELEASE="2"
PACKAGEPLATFORMS="x86_64"
MQ_DEFAULT_INSTALLATION_PATH=/opt/mqm
MQ_INSTALLATION_PATH=${RPM_INSTALL_PREFIX}
MQ_RUNSCRIPT=
echo ${SHELLOPTS} | grep xtrace > /dev/null
if [ $? -eq 0 ] ; then 
  env | sort
  uname -a
  id
  MQ_RUNSCRIPT="sh -x"
fi
if [ ${MQ_INSTALLATION_PATH} !=  ${MQ_DEFAULT_INSTALLATION_PATH} ] ; then 
  if [ -x ${MQ_INSTALLATION_PATH}/bin/amqicrel ] ; then 
     ${MQ_RUNSCRIPT} ${MQ_INSTALLATION_PATH}/bin/amqicrel ${MQ_INSTALLATION_PATH} ${RPM_PACKAGE_NAME}-${RPM_PACKAGE_VERSION}-${RPM_PACKAGE_RELEASE}
  fi
fi

%preun
RPM_PACKAGE_SUMMARY="WebSphere MQ Managed File Transfer Base Component"
RPM_PACKAGE_NAME="MQSeriesFTBase"
RPM_PACKAGE_VERSION="7.5.0"
RPM_PACKAGE_RELEASE="2"
PACKAGEPLATFORMS="x86_64"
MQ_DEFAULT_INSTALLATION_PATH=/opt/mqm
MQ_INSTALLATION_PATH=${RPM_INSTALL_PREFIX}
MQ_RUNSCRIPT=
echo ${SHELLOPTS} | grep xtrace > /dev/null
if [ $? -eq 0 ] ; then 
  env | sort
  uname -a
  id
  MQ_RUNSCRIPT="sh -x"
fi

#   <copyright 
#   notice="lm-source-program" 
#   pids="5724-H72," 
#   years="2005,2012" 
#   crc="122768040" > 
#  Licensed Materials - Property of IBM  
#   
#  5724-H72, 
#   
#  (C) Copyright IBM Corp. 2005, 2012 All Rights Reserved.  
#   
#  US Government Users Restricted Rights - Use, duplication or  
#  disclosure restricted by GSA ADP Schedule Contract with  
#  IBM Corp.  
#   </copyright> 
#
# Pre-uninstallation script
# Checks for already running Q Managers, and if it finds one, stops the
# uninstall.

# If amqiclen exists (should do during uninstall) then run it to clean up
# IPCC resources. If amqiclen returns an error then a queue manager is still
# running so stop the uninstall.
if [ -x ${MQ_INSTALLATION_PATH}/bin/amqiclen ] && [ -f /var/mqm/mqs.ini ]
then
    ${MQ_INSTALLATION_PATH}/bin/amqiclen -x -v > /tmp/amqiclen.$$.out 2>&1
    amqiclen_rc=$?
    if [ $amqiclen_rc -ne 0 ]
    then
      echo "ERROR:    WebSphere MQ shared resources for this installation are still in use." >&2
      echo "          It is likely that one or more queue managers are still running." >&2
      echo "          Return code from from amqiclen was \"$amqiclen_rc\", output was:" >&2
      cat /tmp/amqiclen.$$.out >&2
      rm /tmp/amqiclen.$$.out
      exit 1
    fi
    rm /tmp/amqiclen.$$.out
fi

#   <copyright 
#   notice="lm-source-program" 
#   pids="5724-H72," 
#   years="2005,2012" 
#   crc="1595222582" > 
#  Licensed Materials - Property of IBM  
#   
#  5724-H72, 
#   
#  (C) Copyright IBM Corp. 2005, 2012 All Rights Reserved.  
#   
#  US Government Users Restricted Rights - Use, duplication or  
#  disclosure restricted by GSA ADP Schedule Contract with  
#  IBM Corp.  
#   </copyright> 
#
# Pre-uninstallation check script for all components
# A check is performed to see if there are any fixpack filesets applied to
# the base component which is currently being uninstalled.  If the fixpack
# has been applied, the uninstallation of this component is aborted to prevent
# the situation where the base fileset has been uninstalled leaving an
# uninstallable fixpack.

FIXPACK_BACKUPDIR="${MQ_INSTALLATION_PATH}/maintenance"

find $FIXPACK_BACKUPDIR -type d -maxdepth 2 -print 2>/dev/null | \
      while read directory ; do
  component=`basename $directory`
  if [ "$RPM_PACKAGE_NAME" = "$component" ]; then
    # safety check - are there actually files under this directory?
    num_files=`find "$directory" -type f -print 2>/dev/null | wc -l`
    if [ $num_files -gt 0 ]; then
      cat << EOF

  ERROR:  There appears to be a fixpack installed on this machine for this
          component.

  Please ensure you have removed all fixpacks for the \"$RPM_PACKAGE_NAME\"
  component before trying to remove this package.

EOF
      exit 1
    fi
  fi
done

# Removing product links

%postun
RPM_PACKAGE_SUMMARY="WebSphere MQ Managed File Transfer Base Component"
RPM_PACKAGE_NAME="MQSeriesFTBase"
RPM_PACKAGE_VERSION="7.5.0"
RPM_PACKAGE_RELEASE="2"
PACKAGEPLATFORMS="x86_64"
MQ_DEFAULT_INSTALLATION_PATH=/opt/mqm
MQ_INSTALLATION_PATH=${RPM_INSTALL_PREFIX}
MQ_RUNSCRIPT=
echo ${SHELLOPTS} | grep xtrace > /dev/null
if [ $? -eq 0 ] ; then 
  env | sort
  uname -a
  id
  MQ_RUNSCRIPT="sh -x"
fi

