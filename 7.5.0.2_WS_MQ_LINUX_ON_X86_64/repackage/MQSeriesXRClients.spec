Summary: WebSphere MQ Telemetry Clients
Name: MQSeriesXRClients
Version: 7.5.0
Release: 2
License: Commercial
ExclusiveArch: x86_64
Group: Applications/Networking
AutoReqProv: no
Vendor: IBM
Prefix: /opt/mqm
Requires: MQSeriesRuntime = 7.5.0-2
%global __strip /bin/true
%global _rpmdir /build/slot1/p750_P/inst.images/amd64_linux_2/images/
%global _tmppath /build/slot1/p750_P/obj/amd64_linux_2/install/unix/linux_2
BuildRoot: /build/slot1/p750_P/obj/amd64_linux_2/ship

%description
IBM WebSphere MQ for Developers for Linux for x86_64
5724-H72
%clean
rm -rf $RPM_BUILD_ROOT

%install
install -d $RPM_BUILD_ROOT/opt/mqm
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr
install -d $RPM_BUILD_ROOT/opt/mqm/properties
install -d $RPM_BUILD_ROOT/opt/mqm/properties/version
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/MQServer
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/MQServer/javadoc
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/MQServer/javadoc/com
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/MQServer/javadoc/com/ibm
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/MQServer/javadoc/com/ibm/mq
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/MQServer/javadoc/com/ibm/mq/mqxr
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/MQServer/javadoc/com/ibm/mq/mqxr/class-use
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/MQServer/javadoc/resources
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/WebContent
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/WebContent/WebSocket
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/WebContent/WebSocket/symbols
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/WebContent/WebSocket/symbols/src
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/advanced
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/advanced/DeviceDaemon
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/aix
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/linux_ARM_glibc
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/linux_ARM_uclibc
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/linux_ia32
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/linux_ia64
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/linux_s390x
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/windows_ia32
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/apks
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/assets
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/bin
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/gen
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/libs
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/color
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/drawable
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/values-large
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/values-sw600dp
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/drawable-hdpi
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/drawable-ldpi
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/drawable-mdpi
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/drawable-xhdpi
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/layout
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/menu
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/raw
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/values
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/service
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/aix
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/include
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/linux_ARM_glibc
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/linux_ARM_uclibc
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/linux_ia32
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/linux_ia64
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/linux_s390x
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/samples
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/windows_ia32
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/class-use
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/logging
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/logging/class-use
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/persist
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/persist/class-use
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/util
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/util/class-use
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/class-use
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/resources
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/samples
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/samples/org
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/samples/org/eclipse
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/samples/org/eclipse/paho
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/samples/org/eclipse/paho/sample
install -d $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/samples/org/eclipse/paho/sample/mqttv3app
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/properties/version/IBM_WebSphere_MQ_Telemetry_Clients.7.5.0.cmptag $RPM_BUILD_ROOT/opt/mqm/properties/version/IBM_WebSphere_MQ_Telemetry_Clients.7.5.0.cmptag
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/MQServer/MQXR.jar $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/MQServer/MQXR.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/MQServer/javadoc/allclasses-frame.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/MQServer/javadoc/allclasses-frame.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/MQServer/javadoc/allclasses-noframe.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/MQServer/javadoc/allclasses-noframe.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/MQServer/javadoc/com/ibm/mq/mqxr/AuthCallback.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/MQServer/javadoc/com/ibm/mq/mqxr/AuthCallback.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/MQServer/javadoc/com/ibm/mq/mqxr/class-use/AuthCallback.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/MQServer/javadoc/com/ibm/mq/mqxr/class-use/AuthCallback.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/MQServer/javadoc/com/ibm/mq/mqxr/package-frame.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/MQServer/javadoc/com/ibm/mq/mqxr/package-frame.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/MQServer/javadoc/com/ibm/mq/mqxr/package-summary.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/MQServer/javadoc/com/ibm/mq/mqxr/package-summary.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/MQServer/javadoc/com/ibm/mq/mqxr/package-tree.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/MQServer/javadoc/com/ibm/mq/mqxr/package-tree.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/MQServer/javadoc/com/ibm/mq/mqxr/package-use.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/MQServer/javadoc/com/ibm/mq/mqxr/package-use.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/MQServer/javadoc/constant-values.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/MQServer/javadoc/constant-values.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/MQServer/javadoc/deprecated-list.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/MQServer/javadoc/deprecated-list.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/MQServer/javadoc/help-doc.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/MQServer/javadoc/help-doc.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/MQServer/javadoc/index-all.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/MQServer/javadoc/index-all.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/MQServer/javadoc/index.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/MQServer/javadoc/index.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/MQServer/javadoc/overview-tree.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/MQServer/javadoc/overview-tree.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/MQServer/javadoc/package-list $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/MQServer/javadoc/package-list
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/MQServer/javadoc/resources/inherit.gif $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/MQServer/javadoc/resources/inherit.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/MQServer/javadoc/stylesheet.css $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/MQServer/javadoc/stylesheet.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/REDIST.TXT $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/REDIST.TXT
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/WebContent/EnterperiseMessagingButton.gif $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/WebContent/EnterperiseMessagingButton.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/WebContent/CommunityButton.gif $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/WebContent/CommunityButton.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/WebContent/References.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/WebContent/References.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/WebContent/IBMMessagingBanner.gif $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/WebContent/IBMMessagingBanner.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/WebContent/Links.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/WebContent/Links.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/WebContent/PlayButton.gif $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/WebContent/PlayButton.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/WebContent/websphere-brandmark.gif $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/WebContent/websphere-brandmark.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/WebContent/mqttLogo.png $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/WebContent/mqttLogo.png
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/WebContent/core.js $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/WebContent/core.js
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/WebContent/WebSocket/Tutorial31_Diagnostics.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/WebContent/WebSocket/Tutorial31_Diagnostics.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/WebContent/WebSocket/Tutorial31_CompleteWebPage.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/WebContent/WebSocket/Tutorial31_CompleteWebPage.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/WebContent/WebSocket/Tutorial31_Messages.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/WebContent/WebSocket/Tutorial31_Messages.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/WebContent/WebSocket/Tutorial31_Start.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/WebContent/WebSocket/Tutorial31_Start.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/WebContent/WebSocket/Tutorial31_Subscribe.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/WebContent/WebSocket/Tutorial31_Subscribe.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/WebContent/WebSocket/WebMessagingUtility.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/WebContent/WebSocket/WebMessagingUtility.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/WebContent/WebSocket/files.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/WebContent/WebSocket/files.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/WebContent/WebSocket/index.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/WebContent/WebSocket/index.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/WebContent/WebSocket/mqttws31.js $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/WebContent/WebSocket/mqttws31.js
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/WebContent/WebSocket/symbols/Messaging.Client.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/WebContent/WebSocket/symbols/Messaging.Client.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/WebContent/WebSocket/symbols/Messaging.Message.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/WebContent/WebSocket/symbols/Messaging.Message.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/WebContent/WebSocket/symbols/Messaging.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/WebContent/WebSocket/symbols/Messaging.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/WebContent/WebSocket/symbols/_global_.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/WebContent/WebSocket/symbols/_global_.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/WebContent/WebSocket/symbols/src/mqttws31.js.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/WebContent/WebSocket/symbols/src/mqttws31.js.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/WebContent/WebSocket/tutorial.js $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/WebContent/WebSocket/tutorial.js
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/WebContent/index.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/WebContent/index.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/WebContent/structure.svg $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/WebContent/structure.svg
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/WebContent/style.css $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/WebContent/style.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/aix/amqtdd $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/aix/amqtdd
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/linux_ARM_glibc/amqtdd $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/linux_ARM_glibc/amqtdd
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/linux_ARM_uclibc/amqtdd $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/linux_ARM_uclibc/amqtdd
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/linux_ia32/amqtdd $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/linux_ia32/amqtdd
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/linux_ia64/amqtdd $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/linux_ia64/amqtdd
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/linux_s390x/amqtdd $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/linux_s390x/amqtdd
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_cs.utf16 $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_cs.utf16
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_cs.utf8 $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_cs.utf8
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_de.utf16 $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_de.utf16
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_de.utf8 $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_de.utf8
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_en.utf16 $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_en.utf16
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_en.utf8 $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_en.utf8
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_es.utf16 $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_es.utf16
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_es.utf8 $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_es.utf8
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_fr.utf16 $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_fr.utf16
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_fr.utf8 $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_fr.utf8
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_hu.utf16 $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_hu.utf16
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_hu.utf8 $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_hu.utf8
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_it.utf16 $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_it.utf16
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_it.utf8 $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_it.utf8
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_ja.utf16 $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_ja.utf16
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_ja.utf8 $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_ja.utf8
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_ko.utf16 $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_ko.utf16
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_ko.utf8 $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_ko.utf8
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_pl.utf16 $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_pl.utf16
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_pl.utf8 $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_pl.utf8
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_pt_BR.utf16 $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_pt_BR.utf16
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_pt_BR.utf8 $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_pt_BR.utf8
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_ru.utf16 $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_ru.utf16
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_ru.utf8 $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_ru.utf8
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_zh_CN.utf16 $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_zh_CN.utf16
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_zh_CN.utf8 $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_zh_CN.utf8
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_zh_TW.utf16 $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_zh_TW.utf16
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_zh_TW.utf8 $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_zh_TW.utf8
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/windows_ia32/amqtdd.exe $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/windows_ia32/amqtdd.exe
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/apks/MqttExerciser.apk $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/apks/MqttExerciser.apk
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/.classpath $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/.classpath
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/.project $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/.project
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/AndroidManifest.xml $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/AndroidManifest.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/arrow-web.png $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/arrow-web.png
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/ic_launcher-web.png $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/ic_launcher-web.png
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/proguard-project.txt $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/proguard-project.txt
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/project.properties $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/project.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/drawable-hdpi/arrow.png $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/drawable-hdpi/arrow.png
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/drawable-hdpi/ic_launcher.png $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/drawable-hdpi/ic_launcher.png
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/drawable-ldpi/arrow.png $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/drawable-ldpi/arrow.png
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/drawable-ldpi/ic_launcher.png $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/drawable-ldpi/ic_launcher.png
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/drawable-mdpi/arrow.png $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/drawable-mdpi/arrow.png
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/drawable-mdpi/ic_launcher.png $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/drawable-mdpi/ic_launcher.png
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/drawable-xhdpi/arrow.png $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/drawable-xhdpi/arrow.png
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/drawable-xhdpi/ic_launcher.png $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/drawable-xhdpi/ic_launcher.png
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/layout/activity_advanced.xml $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/layout/activity_advanced.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/layout/activity_connection_details.xml $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/layout/activity_connection_details.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/layout/activity_new_connection.xml $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/layout/activity_new_connection.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/layout/activity_publish.xml $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/layout/activity_publish.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/layout/activity_subscribe.xml $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/layout/activity_subscribe.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/layout/client_connections.xml $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/layout/client_connections.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/layout/connection_text_view.xml $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/layout/connection_text_view.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/layout/list_view_text_view.xml $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/layout/list_view_text_view.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/menu/activity_advanced.xml $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/menu/activity_advanced.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/menu/activity_connection_details.xml $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/menu/activity_connection_details.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/menu/activity_connection_details_disconnected.xml $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/menu/activity_connection_details_disconnected.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/menu/activity_connections.xml $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/menu/activity_connections.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/menu/activity_connections_logging.xml $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/menu/activity_connections_logging.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/menu/activity_last_will.xml $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/menu/activity_last_will.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/menu/activity_new_connection.xml $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/menu/activity_new_connection.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/menu/activity_publish.xml $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/menu/activity_publish.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/menu/activity_publish_disconnected.xml $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/menu/activity_publish_disconnected.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/menu/activity_subscribe.xml $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/menu/activity_subscribe.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/menu/activity_subscribe_disconnected.xml $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/menu/activity_subscribe_disconnected.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/raw/jsr47android.properties $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/raw/jsr47android.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/values/attrs_arrow.xml $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/values/attrs_arrow.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/values/strings.xml $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/values/strings.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/ActionListener.java $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/ActionListener.java
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/ActivityConstants.java $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/ActivityConstants.java
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/Advanced.java $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/Advanced.java
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/ClientConnections.java $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/ClientConnections.java
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/Connection.java $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/Connection.java
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/ConnectionDetails.java $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/ConnectionDetails.java
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/Connections.java $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/Connections.java
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/HistoryFragment.java $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/HistoryFragment.java
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/LastWill.java $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/LastWill.java
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/Listener.java $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/Listener.java
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/MqttCallbackHandler.java $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/MqttCallbackHandler.java
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/NewConnection.java $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/NewConnection.java
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/Notify.java $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/Notify.java
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/PublishFragment.java $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/PublishFragment.java
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/SubscribeFragment.java $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/SubscribeFragment.java
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/package.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/package.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/service/DatabaseMessageStore.java $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/service/DatabaseMessageStore.java
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/service/MessageStore.java $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/service/MessageStore.java
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/service/MqttClientAndroidService.java $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/service/MqttClientAndroidService.java
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/service/MqttDeliveryTokenAndroidService.java $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/service/MqttDeliveryTokenAndroidService.java
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/service/MqttService.java $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/service/MqttService.java
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/service/MqttServiceBinder.java $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/service/MqttServiceBinder.java
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/service/MqttServiceClient.java $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/service/MqttServiceClient.java
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/service/MqttServiceConstants.java $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/service/MqttServiceConstants.java
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/service/MqttTokenAndroidService.java $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/service/MqttTokenAndroidService.java
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/service/MqttTraceHandler.java $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/service/MqttTraceHandler.java
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/service/ParcelableMqttMessage.java $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/service/ParcelableMqttMessage.java
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/service/Status.java $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/service/Status.java
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/service/package.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/service/package.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/aix/libmqttv3c.so $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/aix/libmqttv3c.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/_m_q_t_t_client_8h.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/_m_q_t_t_client_8h.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/_m_q_t_t_client_8h_source.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/_m_q_t_t_client_8h_source.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/_m_q_t_t_client_persistence_8h.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/_m_q_t_t_client_persistence_8h.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/_m_q_t_t_client_persistence_8h_source.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/_m_q_t_t_client_persistence_8h_source.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/annotated.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/annotated.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/async.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/async.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/bc_s.png $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/bc_s.png
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/bdwn.png $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/bdwn.png
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/classes.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/classes.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/closed.png $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/closed.png
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/doxygen.css $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/doxygen.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/doxygen.png $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/doxygen.png
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/files.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/files.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/functions.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/functions.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/functions_vars.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/functions_vars.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/globals.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/globals.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/globals_defs.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/globals_defs.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/globals_func.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/globals_func.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/globals_type.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/globals_type.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/index.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/index.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/nav_f.png $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/nav_f.png
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/nav_h.png $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/nav_h.png
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/open.png $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/open.png
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/pages.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/pages.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/pubasync.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/pubasync.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/pubsync.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/pubsync.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/qos.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/qos.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/struct_m_q_t_t_client__connect_options.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/struct_m_q_t_t_client__connect_options.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/struct_m_q_t_t_client___s_s_l_options.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/struct_m_q_t_t_client___s_s_l_options.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/struct_m_q_t_t_client__name_value.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/struct_m_q_t_t_client__name_value.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/struct_m_q_t_t_client__message.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/struct_m_q_t_t_client__message.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/struct_m_q_t_t_client__persistence.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/struct_m_q_t_t_client__persistence.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/struct_m_q_t_t_client__will_options.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/struct_m_q_t_t_client__will_options.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/subasync.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/subasync.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/tab_a.png $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/tab_a.png
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/tab_b.png $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/tab_b.png
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/tab_h.png $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/tab_h.png
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/tab_s.png $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/tab_s.png
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/tabs.css $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/tabs.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/wildcard.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/wildcard.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/_m_q_t_t_async_8h.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/_m_q_t_t_async_8h.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/_m_q_t_t_async_8h_source.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/_m_q_t_t_async_8h_source.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/_m_q_t_t_client_persistence_8h.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/_m_q_t_t_client_persistence_8h.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/_m_q_t_t_client_persistence_8h_source.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/_m_q_t_t_client_persistence_8h_source.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/annotated.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/annotated.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/async.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/async.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/bc_s.png $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/bc_s.png
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/bdwn.png $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/bdwn.png
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/classes.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/classes.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/closed.png $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/closed.png
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/doxygen.css $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/doxygen.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/doxygen.png $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/doxygen.png
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/files.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/files.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/functions.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/functions.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/functions_vars.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/functions_vars.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/globals.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/globals.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/globals_defs.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/globals_defs.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/globals_enum.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/globals_enum.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/globals_eval.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/globals_eval.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/globals_func.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/globals_func.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/globals_type.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/globals_type.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/index.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/index.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/nav_f.png $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/nav_f.png
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/nav_h.png $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/nav_h.png
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/open.png $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/open.png
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/pages.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/pages.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/publish.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/publish.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/qos.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/qos.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/struct_m_q_t_t_async___s_s_l_options.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/struct_m_q_t_t_async___s_s_l_options.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/struct_m_q_t_t_async__connect_options.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/struct_m_q_t_t_async__connect_options.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/struct_m_q_t_t_async__disconnect_options.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/struct_m_q_t_t_async__disconnect_options.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/struct_m_q_t_t_async__failure_data.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/struct_m_q_t_t_async__failure_data.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/struct_m_q_t_t_async__message.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/struct_m_q_t_t_async__message.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/struct_m_q_t_t_async__name_value.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/struct_m_q_t_t_async__name_value.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/struct_m_q_t_t_async__response_options.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/struct_m_q_t_t_async__response_options.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/struct_m_q_t_t_async__success_data.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/struct_m_q_t_t_async__success_data.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/struct_m_q_t_t_async__will_options.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/struct_m_q_t_t_async__will_options.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/struct_m_q_t_t_client__persistence.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/struct_m_q_t_t_client__persistence.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/subscribe.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/subscribe.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/tab_a.png $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/tab_a.png
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/tab_b.png $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/tab_b.png
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/tab_h.png $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/tab_h.png
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/tab_s.png $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/tab_s.png
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/tabs.css $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/tabs.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/wildcard.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/wildcard.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/include/MQTTAsync.h $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/include/MQTTAsync.h
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/include/MQTTClient.h $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/include/MQTTClient.h
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/include/MQTTClientPersistence.h $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/include/MQTTClientPersistence.h
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/linux_ARM_glibc/libmqttv3c.so $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/linux_ARM_glibc/libmqttv3c.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/linux_ARM_uclibc/libmqttv3c.so $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/linux_ARM_uclibc/libmqttv3c.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/linux_ia32/MQTTVersion $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/linux_ia32/MQTTVersion
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/linux_ia32/libmqttv3a.so $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/linux_ia32/libmqttv3a.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/linux_ia32/libmqttv3as.so $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/linux_ia32/libmqttv3as.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/linux_ia32/libmqttv3c.so $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/linux_ia32/libmqttv3c.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/linux_ia32/libmqttv3cs.so $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/linux_ia32/libmqttv3cs.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/linux_ia64/MQTTVersion $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/linux_ia64/MQTTVersion
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/linux_ia64/libmqttv3a.so $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/linux_ia64/libmqttv3a.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/linux_ia64/libmqttv3as.so $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/linux_ia64/libmqttv3as.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/linux_ia64/libmqttv3c.so $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/linux_ia64/libmqttv3c.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/linux_ia64/libmqttv3cs.so $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/linux_ia64/libmqttv3cs.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/linux_s390x/libmqttv3c.so $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/linux_s390x/libmqttv3c.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/samples/MQTTV3ASSample.c $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/samples/MQTTV3ASSample.c
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/samples/MQTTV3ASample.c $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/samples/MQTTV3ASample.c
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/samples/MQTTV3SSample.c $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/samples/MQTTV3SSample.c
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/samples/MQTTV3Sample.c $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/samples/MQTTV3Sample.c
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/source.zip $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/source.zip
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/windows_ia32/MQTTVersion.exe $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/windows_ia32/MQTTVersion.exe
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/windows_ia32/mqttv3a.dll $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/windows_ia32/mqttv3a.dll
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/windows_ia32/mqttv3a.lib $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/windows_ia32/mqttv3a.lib
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/windows_ia32/mqttv3as.dll $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/windows_ia32/mqttv3as.dll
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/windows_ia32/mqttv3as.lib $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/windows_ia32/mqttv3as.lib
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/windows_ia32/mqttv3c.dll $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/windows_ia32/mqttv3c.dll
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/windows_ia32/mqttv3c.lib $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/windows_ia32/mqttv3c.lib
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/windows_ia32/mqttv3cs.dll $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/windows_ia32/mqttv3cs.dll
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/c/windows_ia32/mqttv3cs.lib $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/c/windows_ia32/mqttv3cs.lib
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/com.ibm.micro.client.mqttv3.jar $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/com.ibm.micro.client.mqttv3.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/com.ibm.micro.client.mqttv3_7.5.0.2.jar $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/com.ibm.micro.client.mqttv3_7.5.0.2.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/allclasses-frame.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/allclasses-frame.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/allclasses-noframe.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/allclasses-noframe.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/MqttCallback.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/MqttCallback.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/MqttClient.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/MqttClient.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/MqttClientPersistence.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/MqttClientPersistence.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/MqttConnectOptions.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/MqttConnectOptions.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/MqttDefaultFilePersistence.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/MqttDefaultFilePersistence.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/MqttDeliveryToken.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/MqttDeliveryToken.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/MqttException.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/MqttException.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/MqttMessage.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/MqttMessage.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/MqttPersistable.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/MqttPersistable.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/MqttPersistenceException.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/MqttPersistenceException.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/MqttSecurityException.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/MqttSecurityException.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/MqttTopic.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/MqttTopic.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/class-use/MqttCallback.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/class-use/MqttCallback.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/class-use/MqttClient.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/class-use/MqttClient.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/class-use/MqttClientPersistence.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/class-use/MqttClientPersistence.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/class-use/MqttConnectOptions.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/class-use/MqttConnectOptions.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/class-use/MqttDefaultFilePersistence.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/class-use/MqttDefaultFilePersistence.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/class-use/MqttDeliveryToken.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/class-use/MqttDeliveryToken.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/class-use/MqttException.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/class-use/MqttException.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/class-use/MqttMessage.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/class-use/MqttMessage.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/class-use/MqttPersistable.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/class-use/MqttPersistable.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/class-use/MqttPersistenceException.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/class-use/MqttPersistenceException.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/class-use/MqttSecurityException.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/class-use/MqttSecurityException.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/class-use/MqttTopic.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/class-use/MqttTopic.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/package-frame.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/package-frame.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/package-summary.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/package-summary.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/package-tree.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/package-tree.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/package-use.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/package-use.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/constant-values.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/constant-values.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/deprecated-list.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/deprecated-list.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/help-doc.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/help-doc.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/index-all.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/index-all.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/index.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/index.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/IMqttActionListener.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/IMqttActionListener.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/IMqttAsyncClient.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/IMqttAsyncClient.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/IMqttClient.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/IMqttClient.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/IMqttDeliveryToken.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/IMqttDeliveryToken.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/IMqttToken.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/IMqttToken.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/MqttAsyncClient.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/MqttAsyncClient.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/MqttCallback.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/MqttCallback.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/MqttClient.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/MqttClient.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/MqttClientPersistence.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/MqttClientPersistence.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/MqttConnectOptions.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/MqttConnectOptions.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/MqttDeliveryToken.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/MqttDeliveryToken.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/MqttException.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/MqttException.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/MqttMessage.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/MqttMessage.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/MqttPersistable.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/MqttPersistable.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/MqttPersistenceException.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/MqttPersistenceException.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/MqttToken.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/MqttToken.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/MqttSecurityException.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/MqttSecurityException.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/MqttTopic.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/MqttTopic.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/IMqttActionListener.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/IMqttActionListener.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/IMqttAsyncClient.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/IMqttAsyncClient.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/IMqttClient.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/IMqttClient.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/IMqttDeliveryToken.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/IMqttDeliveryToken.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/IMqttToken.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/IMqttToken.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/MqttAsyncClient.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/MqttAsyncClient.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/MqttCallback.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/MqttCallback.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/MqttClient.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/MqttClient.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/MqttClientPersistence.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/MqttClientPersistence.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/MqttConnectOptions.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/MqttConnectOptions.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/MqttDeliveryToken.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/MqttDeliveryToken.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/MqttException.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/MqttException.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/MqttMessage.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/MqttMessage.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/MqttPersistable.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/MqttPersistable.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/MqttPersistenceException.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/MqttPersistenceException.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/MqttToken.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/MqttToken.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/MqttSecurityException.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/MqttSecurityException.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/MqttTopic.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/MqttTopic.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/logging/JSR47Logger.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/logging/JSR47Logger.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/logging/Logger.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/logging/Logger.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/logging/LoggerFactory.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/logging/LoggerFactory.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/logging/SimpleLogFormatter.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/logging/SimpleLogFormatter.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/logging/class-use/JSR47Logger.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/logging/class-use/JSR47Logger.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/logging/class-use/Logger.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/logging/class-use/Logger.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/logging/class-use/LoggerFactory.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/logging/class-use/LoggerFactory.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/logging/class-use/SimpleLogFormatter.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/logging/class-use/SimpleLogFormatter.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/logging/package-frame.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/logging/package-frame.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/logging/package-summary.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/logging/package-summary.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/logging/package-tree.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/logging/package-tree.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/logging/package-use.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/logging/package-use.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/package-frame.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/package-frame.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/package-summary.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/package-summary.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/package-tree.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/package-tree.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/package-use.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/package-use.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/persist/MemoryPersistence.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/persist/MemoryPersistence.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/persist/MqttDefaultFilePersistence.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/persist/MqttDefaultFilePersistence.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/persist/class-use/MemoryPersistence.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/persist/class-use/MemoryPersistence.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/persist/class-use/MqttDefaultFilePersistence.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/persist/class-use/MqttDefaultFilePersistence.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/persist/package-frame.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/persist/package-frame.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/persist/package-summary.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/persist/package-summary.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/persist/package-tree.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/persist/package-tree.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/persist/package-use.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/persist/package-use.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/util/Debug.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/util/Debug.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/util/class-use/Debug.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/util/class-use/Debug.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/util/package-frame.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/util/package-frame.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/util/package-summary.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/util/package-summary.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/util/package-tree.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/util/package-tree.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/util/package-use.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/util/package-use.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/Sample.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/Sample.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/SampleAsyncCallBack.Disconnector.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/SampleAsyncCallBack.Disconnector.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/SampleAsyncCallBack.MqttConnector.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/SampleAsyncCallBack.MqttConnector.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/SampleAsyncCallBack.Publisher.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/SampleAsyncCallBack.Publisher.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/SampleAsyncCallBack.Subscriber.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/SampleAsyncCallBack.Subscriber.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/SampleAsyncCallBack.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/SampleAsyncCallBack.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/SampleAsyncWait.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/SampleAsyncWait.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/class-use/Sample.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/class-use/Sample.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/class-use/SampleAsyncCallBack.Disconnector.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/class-use/SampleAsyncCallBack.Disconnector.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/class-use/SampleAsyncCallBack.MqttConnector.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/class-use/SampleAsyncCallBack.MqttConnector.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/class-use/SampleAsyncCallBack.Publisher.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/class-use/SampleAsyncCallBack.Publisher.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/class-use/SampleAsyncCallBack.Subscriber.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/class-use/SampleAsyncCallBack.Subscriber.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/class-use/SampleAsyncCallBack.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/class-use/SampleAsyncCallBack.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/class-use/SampleAsyncWait.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/class-use/SampleAsyncWait.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/package-frame.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/package-frame.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/package-summary.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/package-summary.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/package-tree.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/package-tree.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/package-use.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/package-use.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/overview-frame.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/overview-frame.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/overview-summary.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/overview-summary.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/overview-tree.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/overview-tree.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/package-list $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/package-list
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/resources/inherit.gif $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/resources/inherit.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/serialized-form.html $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/serialized-form.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/stylesheet.css $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/stylesheet.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/org.eclipse.paho.client.mqttv3.jar $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/org.eclipse.paho.client.mqttv3.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/org.eclipse.paho.client.mqttv3.trace.jar $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/org.eclipse.paho.client.mqttv3.trace.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/org.eclipse.paho.client.mqttv3.tracesource.jar $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/org.eclipse.paho.client.mqttv3.tracesource.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/org.eclipse.paho.client.mqttv3source.jar $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/org.eclipse.paho.client.mqttv3source.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/org.eclipse.paho.sample.mqttv3app.jar $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/org.eclipse.paho.sample.mqttv3app.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/org.eclipse.paho.sample.mqttv3appsource.jar $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/org.eclipse.paho.sample.mqttv3appsource.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/samples/MQTTV3Sample.java $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/samples/MQTTV3Sample.java
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/samples/org/eclipse/paho/sample/mqttv3app/Sample.java $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/samples/org/eclipse/paho/sample/mqttv3app/Sample.java
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/samples/org/eclipse/paho/sample/mqttv3app/SampleAsyncCallBack.java $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/samples/org/eclipse/paho/sample/mqttv3app/SampleAsyncCallBack.java
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/clients/java/samples/org/eclipse/paho/sample/mqttv3app/SampleAsyncWait.java $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/clients/java/samples/org/eclipse/paho/sample/mqttv3app/SampleAsyncWait.java
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqxr/SDK/readme.txt $RPM_BUILD_ROOT/opt/mqm/mqxr/SDK/readme.txt

%files
%dir %attr(555,mqm,mqm) "/opt/mqm"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqxr"
%dir %attr(555,mqm,mqm) "/opt/mqm/properties"
%dir %attr(555,mqm,mqm) "/opt/mqm/properties/version"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/MQServer"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/MQServer/javadoc"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/MQServer/javadoc/com"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/MQServer/javadoc/com/ibm"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/MQServer/javadoc/com/ibm/mq"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/MQServer/javadoc/com/ibm/mq/mqxr"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/MQServer/javadoc/com/ibm/mq/mqxr/class-use"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/MQServer/javadoc/resources"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/WebContent"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/WebContent/WebSocket"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/WebContent/WebSocket/symbols"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/WebContent/WebSocket/symbols/src"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/advanced"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/advanced/DeviceDaemon"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/aix"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/linux_ARM_glibc"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/linux_ARM_uclibc"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/linux_ia32"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/linux_ia64"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/linux_s390x"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/windows_ia32"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/apks"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/assets"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/bin"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/gen"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/libs"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/color"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/drawable"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/values-large"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/values-sw600dp"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/drawable-hdpi"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/drawable-ldpi"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/drawable-mdpi"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/drawable-xhdpi"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/layout"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/menu"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/raw"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/values"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/service"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/c"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/aix"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/include"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/linux_ARM_glibc"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/linux_ARM_uclibc"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/linux_ia32"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/linux_ia64"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/linux_s390x"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/samples"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/windows_ia32"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/java"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/class-use"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/logging"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/logging/class-use"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/persist"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/persist/class-use"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/util"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/util/class-use"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/class-use"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/resources"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/samples"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/samples/org"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/samples/org/eclipse"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/samples/org/eclipse/paho"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/samples/org/eclipse/paho/sample"
%dir %attr(555,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/samples/org/eclipse/paho/sample/mqttv3app"
%attr(444,mqm,mqm) "/opt/mqm/properties/version/IBM_WebSphere_MQ_Telemetry_Clients.7.5.0.cmptag"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/MQServer/MQXR.jar"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/MQServer/javadoc/allclasses-frame.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/MQServer/javadoc/allclasses-noframe.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/MQServer/javadoc/com/ibm/mq/mqxr/AuthCallback.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/MQServer/javadoc/com/ibm/mq/mqxr/class-use/AuthCallback.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/MQServer/javadoc/com/ibm/mq/mqxr/package-frame.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/MQServer/javadoc/com/ibm/mq/mqxr/package-summary.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/MQServer/javadoc/com/ibm/mq/mqxr/package-tree.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/MQServer/javadoc/com/ibm/mq/mqxr/package-use.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/MQServer/javadoc/constant-values.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/MQServer/javadoc/deprecated-list.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/MQServer/javadoc/help-doc.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/MQServer/javadoc/index-all.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/MQServer/javadoc/index.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/MQServer/javadoc/overview-tree.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/MQServer/javadoc/package-list"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/MQServer/javadoc/resources/inherit.gif"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/MQServer/javadoc/stylesheet.css"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/REDIST.TXT"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/WebContent/EnterperiseMessagingButton.gif"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/WebContent/CommunityButton.gif"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/WebContent/References.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/WebContent/IBMMessagingBanner.gif"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/WebContent/Links.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/WebContent/PlayButton.gif"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/WebContent/websphere-brandmark.gif"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/WebContent/mqttLogo.png"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/WebContent/core.js"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/WebContent/WebSocket/Tutorial31_Diagnostics.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/WebContent/WebSocket/Tutorial31_CompleteWebPage.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/WebContent/WebSocket/Tutorial31_Messages.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/WebContent/WebSocket/Tutorial31_Start.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/WebContent/WebSocket/Tutorial31_Subscribe.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/WebContent/WebSocket/WebMessagingUtility.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/WebContent/WebSocket/files.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/WebContent/WebSocket/index.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/WebContent/WebSocket/mqttws31.js"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/WebContent/WebSocket/symbols/Messaging.Client.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/WebContent/WebSocket/symbols/Messaging.Message.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/WebContent/WebSocket/symbols/Messaging.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/WebContent/WebSocket/symbols/_global_.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/WebContent/WebSocket/symbols/src/mqttws31.js.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/WebContent/WebSocket/tutorial.js"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/WebContent/index.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/WebContent/structure.svg"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/WebContent/style.css"
%attr(555,bin,bin) "/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/aix/amqtdd"
%attr(555,bin,bin) %verify(not md5 mtime) "/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/linux_ARM_glibc/amqtdd"
%attr(555,bin,bin) %verify(not md5 mtime) "/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/linux_ARM_uclibc/amqtdd"
%attr(555,bin,bin) %verify(not md5 mtime) "/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/linux_ia32/amqtdd"
%attr(555,bin,bin) %verify(not md5 mtime) "/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/linux_ia64/amqtdd"
%attr(555,bin,bin) %verify(not md5 mtime) "/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/linux_s390x/amqtdd"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_cs.utf16"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_cs.utf8"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_de.utf16"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_de.utf8"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_en.utf16"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_en.utf8"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_es.utf16"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_es.utf8"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_fr.utf16"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_fr.utf8"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_hu.utf16"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_hu.utf8"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_it.utf16"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_it.utf8"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_ja.utf16"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_ja.utf8"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_ko.utf16"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_ko.utf8"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_pl.utf16"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_pl.utf8"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_pt_BR.utf16"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_pt_BR.utf8"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_ru.utf16"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_ru.utf8"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_zh_CN.utf16"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_zh_CN.utf8"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_zh_TW.utf16"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/messages/Messages_zh_TW.utf8"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/advanced/DeviceDaemon/windows_ia32/amqtdd.exe"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/apks/MqttExerciser.apk"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/.classpath"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/.project"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/AndroidManifest.xml"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/arrow-web.png"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/ic_launcher-web.png"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/proguard-project.txt"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/project.properties"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/drawable-hdpi/arrow.png"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/drawable-hdpi/ic_launcher.png"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/drawable-ldpi/arrow.png"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/drawable-ldpi/ic_launcher.png"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/drawable-mdpi/arrow.png"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/drawable-mdpi/ic_launcher.png"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/drawable-xhdpi/arrow.png"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/drawable-xhdpi/ic_launcher.png"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/layout/activity_advanced.xml"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/layout/activity_connection_details.xml"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/layout/activity_new_connection.xml"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/layout/activity_publish.xml"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/layout/activity_subscribe.xml"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/layout/client_connections.xml"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/layout/connection_text_view.xml"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/layout/list_view_text_view.xml"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/menu/activity_advanced.xml"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/menu/activity_connection_details.xml"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/menu/activity_connection_details_disconnected.xml"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/menu/activity_connections.xml"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/menu/activity_connections_logging.xml"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/menu/activity_last_will.xml"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/menu/activity_new_connection.xml"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/menu/activity_publish.xml"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/menu/activity_publish_disconnected.xml"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/menu/activity_subscribe.xml"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/menu/activity_subscribe_disconnected.xml"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/raw/jsr47android.properties"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/values/attrs_arrow.xml"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/res/values/strings.xml"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/ActionListener.java"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/ActivityConstants.java"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/Advanced.java"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/ClientConnections.java"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/Connection.java"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/ConnectionDetails.java"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/Connections.java"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/HistoryFragment.java"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/LastWill.java"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/Listener.java"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/MqttCallbackHandler.java"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/NewConnection.java"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/Notify.java"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/PublishFragment.java"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/SubscribeFragment.java"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/package.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/service/DatabaseMessageStore.java"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/service/MessageStore.java"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/service/MqttClientAndroidService.java"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/service/MqttDeliveryTokenAndroidService.java"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/service/MqttService.java"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/service/MqttServiceBinder.java"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/service/MqttServiceClient.java"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/service/MqttServiceConstants.java"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/service/MqttTokenAndroidService.java"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/service/MqttTraceHandler.java"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/service/ParcelableMqttMessage.java"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/service/Status.java"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/Android/samples/mqttExerciser/src/com/ibm/msg/android/service/package.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/aix/libmqttv3c.so"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/_m_q_t_t_client_8h.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/_m_q_t_t_client_8h_source.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/_m_q_t_t_client_persistence_8h.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/_m_q_t_t_client_persistence_8h_source.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/annotated.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/async.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/bc_s.png"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/bdwn.png"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/classes.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/closed.png"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/doxygen.css"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/doxygen.png"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/files.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/functions.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/functions_vars.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/globals.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/globals_defs.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/globals_func.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/globals_type.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/index.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/nav_f.png"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/nav_h.png"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/open.png"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/pages.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/pubasync.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/pubsync.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/qos.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/struct_m_q_t_t_client__connect_options.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/struct_m_q_t_t_client___s_s_l_options.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/struct_m_q_t_t_client__name_value.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/struct_m_q_t_t_client__message.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/struct_m_q_t_t_client__persistence.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/struct_m_q_t_t_client__will_options.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/subasync.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/tab_a.png"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/tab_b.png"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/tab_h.png"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/tab_s.png"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/tabs.css"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/wildcard.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/_m_q_t_t_async_8h.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/_m_q_t_t_async_8h_source.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/_m_q_t_t_client_persistence_8h.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/_m_q_t_t_client_persistence_8h_source.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/annotated.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/async.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/bc_s.png"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/bdwn.png"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/classes.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/closed.png"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/doxygen.css"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/doxygen.png"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/files.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/functions.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/functions_vars.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/globals.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/globals_defs.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/globals_enum.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/globals_eval.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/globals_func.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/globals_type.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/index.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/nav_f.png"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/nav_h.png"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/open.png"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/pages.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/publish.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/qos.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/struct_m_q_t_t_async___s_s_l_options.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/struct_m_q_t_t_async__connect_options.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/struct_m_q_t_t_async__disconnect_options.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/struct_m_q_t_t_async__failure_data.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/struct_m_q_t_t_async__message.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/struct_m_q_t_t_async__name_value.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/struct_m_q_t_t_async__response_options.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/struct_m_q_t_t_async__success_data.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/struct_m_q_t_t_async__will_options.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/struct_m_q_t_t_client__persistence.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/subscribe.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/tab_a.png"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/tab_b.png"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/tab_h.png"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/tab_s.png"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/tabs.css"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/doc/html/MQTTAsync/html/wildcard.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/include/MQTTAsync.h"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/include/MQTTClient.h"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/include/MQTTClientPersistence.h"
%attr(444,bin,bin) %verify(not md5 mtime) "/opt/mqm/mqxr/SDK/clients/c/linux_ARM_glibc/libmqttv3c.so"
%attr(444,bin,bin) %verify(not md5 mtime) "/opt/mqm/mqxr/SDK/clients/c/linux_ARM_uclibc/libmqttv3c.so"
%attr(555,bin,bin) %verify(not md5 mtime) "/opt/mqm/mqxr/SDK/clients/c/linux_ia32/MQTTVersion"
%attr(444,bin,bin) %verify(not md5 mtime) "/opt/mqm/mqxr/SDK/clients/c/linux_ia32/libmqttv3a.so"
%attr(444,bin,bin) %verify(not md5 mtime) "/opt/mqm/mqxr/SDK/clients/c/linux_ia32/libmqttv3as.so"
%attr(444,bin,bin) %verify(not md5 mtime) "/opt/mqm/mqxr/SDK/clients/c/linux_ia32/libmqttv3c.so"
%attr(444,bin,bin) %verify(not md5 mtime) "/opt/mqm/mqxr/SDK/clients/c/linux_ia32/libmqttv3cs.so"
%attr(555,bin,bin) %verify(not md5 mtime) "/opt/mqm/mqxr/SDK/clients/c/linux_ia64/MQTTVersion"
%attr(444,bin,bin) %verify(not md5 mtime) "/opt/mqm/mqxr/SDK/clients/c/linux_ia64/libmqttv3a.so"
%attr(444,bin,bin) %verify(not md5 mtime) "/opt/mqm/mqxr/SDK/clients/c/linux_ia64/libmqttv3as.so"
%attr(444,bin,bin) %verify(not md5 mtime) "/opt/mqm/mqxr/SDK/clients/c/linux_ia64/libmqttv3c.so"
%attr(444,bin,bin) %verify(not md5 mtime) "/opt/mqm/mqxr/SDK/clients/c/linux_ia64/libmqttv3cs.so"
%attr(444,bin,bin) %verify(not md5 mtime) "/opt/mqm/mqxr/SDK/clients/c/linux_s390x/libmqttv3c.so"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/samples/MQTTV3ASSample.c"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/samples/MQTTV3ASample.c"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/samples/MQTTV3SSample.c"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/samples/MQTTV3Sample.c"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/source.zip"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/windows_ia32/MQTTVersion.exe"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/windows_ia32/mqttv3a.dll"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/windows_ia32/mqttv3a.lib"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/windows_ia32/mqttv3as.dll"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/windows_ia32/mqttv3as.lib"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/windows_ia32/mqttv3c.dll"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/windows_ia32/mqttv3c.lib"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/windows_ia32/mqttv3cs.dll"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/c/windows_ia32/mqttv3cs.lib"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/com.ibm.micro.client.mqttv3.jar"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/com.ibm.micro.client.mqttv3_7.5.0.2.jar"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/allclasses-frame.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/allclasses-noframe.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/MqttCallback.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/MqttClient.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/MqttClientPersistence.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/MqttConnectOptions.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/MqttDefaultFilePersistence.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/MqttDeliveryToken.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/MqttException.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/MqttMessage.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/MqttPersistable.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/MqttPersistenceException.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/MqttSecurityException.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/MqttTopic.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/class-use/MqttCallback.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/class-use/MqttClient.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/class-use/MqttClientPersistence.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/class-use/MqttConnectOptions.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/class-use/MqttDefaultFilePersistence.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/class-use/MqttDeliveryToken.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/class-use/MqttException.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/class-use/MqttMessage.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/class-use/MqttPersistable.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/class-use/MqttPersistenceException.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/class-use/MqttSecurityException.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/class-use/MqttTopic.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/package-frame.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/package-summary.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/package-tree.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/com/ibm/micro/client/mqttv3/package-use.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/constant-values.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/deprecated-list.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/help-doc.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/index-all.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/index.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/IMqttActionListener.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/IMqttAsyncClient.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/IMqttClient.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/IMqttDeliveryToken.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/IMqttToken.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/MqttAsyncClient.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/MqttCallback.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/MqttClient.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/MqttClientPersistence.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/MqttConnectOptions.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/MqttDeliveryToken.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/MqttException.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/MqttMessage.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/MqttPersistable.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/MqttPersistenceException.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/MqttToken.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/MqttSecurityException.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/MqttTopic.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/IMqttActionListener.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/IMqttAsyncClient.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/IMqttClient.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/IMqttDeliveryToken.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/IMqttToken.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/MqttAsyncClient.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/MqttCallback.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/MqttClient.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/MqttClientPersistence.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/MqttConnectOptions.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/MqttDeliveryToken.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/MqttException.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/MqttMessage.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/MqttPersistable.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/MqttPersistenceException.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/MqttToken.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/MqttSecurityException.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/class-use/MqttTopic.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/logging/JSR47Logger.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/logging/Logger.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/logging/LoggerFactory.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/logging/SimpleLogFormatter.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/logging/class-use/JSR47Logger.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/logging/class-use/Logger.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/logging/class-use/LoggerFactory.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/logging/class-use/SimpleLogFormatter.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/logging/package-frame.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/logging/package-summary.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/logging/package-tree.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/logging/package-use.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/package-frame.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/package-summary.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/package-tree.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/package-use.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/persist/MemoryPersistence.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/persist/MqttDefaultFilePersistence.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/persist/class-use/MemoryPersistence.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/persist/class-use/MqttDefaultFilePersistence.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/persist/package-frame.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/persist/package-summary.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/persist/package-tree.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/persist/package-use.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/util/Debug.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/util/class-use/Debug.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/util/package-frame.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/util/package-summary.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/util/package-tree.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/client/mqttv3/util/package-use.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/Sample.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/SampleAsyncCallBack.Disconnector.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/SampleAsyncCallBack.MqttConnector.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/SampleAsyncCallBack.Publisher.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/SampleAsyncCallBack.Subscriber.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/SampleAsyncCallBack.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/SampleAsyncWait.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/class-use/Sample.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/class-use/SampleAsyncCallBack.Disconnector.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/class-use/SampleAsyncCallBack.MqttConnector.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/class-use/SampleAsyncCallBack.Publisher.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/class-use/SampleAsyncCallBack.Subscriber.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/class-use/SampleAsyncCallBack.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/class-use/SampleAsyncWait.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/package-frame.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/package-summary.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/package-tree.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/org/eclipse/paho/sample/mqttv3app/package-use.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/overview-frame.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/overview-summary.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/overview-tree.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/package-list"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/resources/inherit.gif"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/serialized-form.html"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/doc/javadoc/stylesheet.css"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/org.eclipse.paho.client.mqttv3.jar"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/org.eclipse.paho.client.mqttv3.trace.jar"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/org.eclipse.paho.client.mqttv3.tracesource.jar"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/org.eclipse.paho.client.mqttv3source.jar"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/org.eclipse.paho.sample.mqttv3app.jar"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/org.eclipse.paho.sample.mqttv3appsource.jar"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/samples/MQTTV3Sample.java"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/samples/org/eclipse/paho/sample/mqttv3app/Sample.java"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/samples/org/eclipse/paho/sample/mqttv3app/SampleAsyncCallBack.java"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/clients/java/samples/org/eclipse/paho/sample/mqttv3app/SampleAsyncWait.java"
%attr(444,bin,bin) "/opt/mqm/mqxr/SDK/readme.txt"

%pre
RPM_PACKAGE_SUMMARY="WebSphere MQ Telemetry Clients"
RPM_PACKAGE_NAME="MQSeriesXRClients"
RPM_PACKAGE_VERSION="7.5.0"
RPM_PACKAGE_RELEASE="2"
PACKAGEPLATFORMS="x86_64"
MQ_DEFAULT_INSTALLATION_PATH=/opt/mqm
MQ_INSTALLATION_PATH=${RPM_INSTALL_PREFIX}
MQ_RUNSCRIPT=
echo ${SHELLOPTS} | grep xtrace > /dev/null
if [ $? -eq 0 ] ; then 
  env | sort
  uname -a
  id
  MQ_RUNSCRIPT="sh -x"
fi

# @(#) MQMBID sn=p750-002-130704 su=_UmspIOSZEeK1oKoKL_dPJA pn=install/unix/linux_2/preinstall.sh
#   <copyright 
#   notice="lm-source-program" 
#   pids="5724-H72," 
#   years="2005,2012" 
#   crc="2186337890" > 
#  Licensed Materials - Property of IBM  
#   
#  5724-H72, 
#   
#  (C) Copyright IBM Corp. 2005, 2012 All Rights Reserved.  
#   
#  US Government Users Restricted Rights - Use, duplication or  
#  disclosure restricted by GSA ADP Schedule Contract with  
#  IBM Corp.  
#   </copyright> 
# Preinstallation script
# Checks to see if the mqm userid and groups exists, and if now will create
# 
# Check that this package is not being installed to a location where 
# a different VR exists
# 

# Set up environment variables
TMPDIR="/tmp"
RC=0
DUMMY=""

create_mqm_group()
{
  echo "Creating group mqm"
  ErrorText=`groupadd mqm 2>&1`
  RC=$?
  if [ $RC -ne 0 ]; then
    echo "ERROR:  Failed to add 'mqm' group:" >&2
    echo $ErrorText >&2
    exit $RC
  fi
}
# Determine if the group exists on the system.  The solution used here is to
# create a file in the temporary location, attempt to change its group id to
# mqm.  This method should work independent of the multitude of ways groups can
# be defined across distributions and network environments
TMPFILE="${TMPDIR}/amq_grouptest"
touch $TMPFILE 2>/dev/null
if [ -f "$TMPFILE" ]; then
  chown :mqm $TMPFILE 2>/dev/null
  if [ $? -ne 0 ]; then
    create_mqm_group
  fi
  rm -f $TMPFILE
else  # Unable to create temporary file.  This is also needed by other install
      # processes - so it is fair to abort the install at this point
  echo "ERROR: Unable to write to ${TMPDIR} - aborting install"
  exit 1
fi

# Determine if the user id "mqm" exists, and is in the group mqm
ErrorText=`id -u mqm 2>&1`
RC=$?
if [ $RC -ne 0 ]; then
  echo "Creating user mqm"
  ErrorText=`useradd -r -d /var/mqm -g mqm mqm 2>&1`
  RC=$?
  if [ $RC -ne 0 ]; then
    echo "ERROR:  Failed to add 'mqm' user:" >&2
    echo $ErrorText >&2
    exit $RC
  fi
else    # Check the user mqm is in the group mqm
  ErrorText=`id -Gn mqm | grep -w mqm 2>&1`
  if [ $? -ne 0 ]; then
    ErrorText=`usermod -G mqm mqm 2>&1`
    if [ $RC -ne 0 ]; then
      echo "ERROR:  Failed to add user mqm to group mqm:" >&2
      echo $ErrorText >&1
      echo $RC
    fi
  fi
fi

#######################################################################################################
# Check the install path does not exceed the Websphere MQ maximum length of 256
#######################################################################################################
if [ ${#MQ_INSTALLATION_PATH} -gt 256 ]; then
  echo ""
  echo "ERROR:   Specified path exceeds WebSphere MQ maximum length of 256"
  echo ""
  exit 1
fi

#######################################################################################################
# Check the install path does not contain unsupported charaters
#######################################################################################################
echo ${MQ_INSTALLATION_PATH} | grep "[:%#]" > /dev/null
if [ $? -eq 0 ] ; then
  echo ""
  echo "ERROR:   Specified path contains an unsupported character"
  echo ""
  exit 1
fi

#######################################################################################################
# Runtime checks
#######################################################################################################
echo ${RPM_PACKAGE_NAME} | grep  "MQSeriesRuntime" > /dev/null
if [ $? -eq 0 ] ; then
  #####################################################################################################
  # Check that the install path is empty
  #####################################################################################################
  if [ -d ${MQ_INSTALLATION_PATH} ] ; then
    LS_ALL=`ls -A ${MQ_INSTALLATION_PATH}`
    if [ "${LS_ALL}" ] && [ "${LS_ALL}" != "lost+found" ] ; then
      echo ""
      echo "ERROR:   Specified path is not empty"
      echo ""
      exit 1
    fi
  fi
#######################################################################################################
# Non Runtime checks
#######################################################################################################
else
  #####################################################################################################
  # Check the version/release of the product already at MQ_INSTALLATION_PATH is the same as this one
  #####################################################################################################
  if [ -x ${MQ_INSTALLATION_PATH}/bin/dspmqver ] ; then
    INSTALLED_VR=$(${MQ_INSTALLATION_PATH}/bin/dspmqver -f2 -b | awk -F. '{print $1 "." $2}')
    PACKAGE_VR=`echo ${RPM_PACKAGE_VERSION} | awk -F. '{print $1 "." $2}'`
    if [ ${INSTALLED_VR} != ${PACKAGE_VR} ] ; then
      echo ""
      echo "ERROR:   This package is not applicable to the WebSphere MQ installation at ${MQ_INSTALLATION_PATH}"
      echo ""
      exit 1
    fi
  else
    echo ""
    echo "ERROR:   There is no MQSeriesRuntime installed at ${MQ_INSTALLATION_PATH}"
    echo ""
    exit 1
  fi
fi

#   <copyright 
#   notice="lm-source-program" 
#   pids="5724-H72," 
#   years="2005,2012" 
#   crc="1114153681" > 
#  Licensed Materials - Property of IBM  
#   
#  5724-H72, 
#   
#  (C) Copyright IBM Corp. 2005, 2012 All Rights Reserved.  
#   
#  US Government Users Restricted Rights - Use, duplication or  
#  disclosure restricted by GSA ADP Schedule Contract with  
#  IBM Corp.  
#   </copyright> 
# Preinstallation script
# Check's to see if the license agreement has been accepted

if [ ! -r /tmp/mq_license_${RPM_PACKAGE_VERSION}/license/status.dat ] && [ ! -r ${MQ_INSTALLATION_PATH}/licenses/status.dat ] ; then

    cat << +++EOM+++
ERROR:  Product cannot be installed until the license
        agreement has been accepted.
        Run the 'mqlicense' script, which is in the root
        directory of the install media, or see the
        installation instructions in the product 
        Infocenter for more information
+++EOM+++

   exit 1
fi

# @(#) MQMBID sn=p750-002-130704 su=_UmspIOSZEeK1oKoKL_dPJA pn=install/unix/linux_2/xrclients_preinstall.sh
# Pre-install script for the mqxr package
# This script checks whether xr701 is installed using the existence
# of a directory specific to that release to do so. The user must uninstall
# 7.0.1 before installing 7.1 in the same location if this is the default
# location.
#   <copyright 
#   notice="lm-source-program" 
#   pids="5724-H72," 
#   years="2010,2012" 
#   crc="608018138" > 
#  Licensed Materials - Property of IBM  
#   
#  5724-H72, 
#   
#  (C) Copyright IBM Corp. 2010, 2012 All Rights Reserved.  
#   
#  US Government Users Restricted Rights - Use, duplication or  
#  disclosure restricted by GSA ADP Schedule Contract with  
#  IBM Corp.  
#   </copyright> 
MQXR_ROOT=${MQ_INSTALLATION_PATH}/mqxr
if [ \( ${MQ_INSTALLATION_PATH} = ${MQ_DEFAULT_INSTALLATION_PATH} \) -a  \( -x ${MQXR_ROOT}/license \) ]
then
 echo "You appear to have WebSphere MQ Telemetry v.7.0.1 installed \
 at the following location: ${MQXR_ROOT}."
 echo "Please uninstall it before continuing with this installation."
 exit 1
fi
echo > /dev/null 2>/dev/null

%post
RPM_PACKAGE_SUMMARY="WebSphere MQ Telemetry Clients"
RPM_PACKAGE_NAME="MQSeriesXRClients"
RPM_PACKAGE_VERSION="7.5.0"
RPM_PACKAGE_RELEASE="2"
PACKAGEPLATFORMS="x86_64"
MQ_DEFAULT_INSTALLATION_PATH=/opt/mqm
MQ_INSTALLATION_PATH=${RPM_INSTALL_PREFIX}
MQ_RUNSCRIPT=
echo ${SHELLOPTS} | grep xtrace > /dev/null
if [ $? -eq 0 ] ; then 
  env | sort
  uname -a
  id
  MQ_RUNSCRIPT="sh -x"
fi
if [ ${MQ_INSTALLATION_PATH} !=  ${MQ_DEFAULT_INSTALLATION_PATH} ] ; then 
  if [ -x ${MQ_INSTALLATION_PATH}/bin/amqicrel ] ; then 
     ${MQ_RUNSCRIPT} ${MQ_INSTALLATION_PATH}/bin/amqicrel ${MQ_INSTALLATION_PATH} ${RPM_PACKAGE_NAME}-${RPM_PACKAGE_VERSION}-${RPM_PACKAGE_RELEASE}
  fi
fi

%preun
RPM_PACKAGE_SUMMARY="WebSphere MQ Telemetry Clients"
RPM_PACKAGE_NAME="MQSeriesXRClients"
RPM_PACKAGE_VERSION="7.5.0"
RPM_PACKAGE_RELEASE="2"
PACKAGEPLATFORMS="x86_64"
MQ_DEFAULT_INSTALLATION_PATH=/opt/mqm
MQ_INSTALLATION_PATH=${RPM_INSTALL_PREFIX}
MQ_RUNSCRIPT=
echo ${SHELLOPTS} | grep xtrace > /dev/null
if [ $? -eq 0 ] ; then 
  env | sort
  uname -a
  id
  MQ_RUNSCRIPT="sh -x"
fi

#   <copyright 
#   notice="lm-source-program" 
#   pids="5724-H72," 
#   years="2005,2012" 
#   crc="122768040" > 
#  Licensed Materials - Property of IBM  
#   
#  5724-H72, 
#   
#  (C) Copyright IBM Corp. 2005, 2012 All Rights Reserved.  
#   
#  US Government Users Restricted Rights - Use, duplication or  
#  disclosure restricted by GSA ADP Schedule Contract with  
#  IBM Corp.  
#   </copyright> 
#
# Pre-uninstallation script
# Checks for already running Q Managers, and if it finds one, stops the
# uninstall.

# If amqiclen exists (should do during uninstall) then run it to clean up
# IPCC resources. If amqiclen returns an error then a queue manager is still
# running so stop the uninstall.
if [ -x ${MQ_INSTALLATION_PATH}/bin/amqiclen ] && [ -f /var/mqm/mqs.ini ]
then
    ${MQ_INSTALLATION_PATH}/bin/amqiclen -x -v > /tmp/amqiclen.$$.out 2>&1
    amqiclen_rc=$?
    if [ $amqiclen_rc -ne 0 ]
    then
      echo "ERROR:    WebSphere MQ shared resources for this installation are still in use." >&2
      echo "          It is likely that one or more queue managers are still running." >&2
      echo "          Return code from from amqiclen was \"$amqiclen_rc\", output was:" >&2
      cat /tmp/amqiclen.$$.out >&2
      rm /tmp/amqiclen.$$.out
      exit 1
    fi
    rm /tmp/amqiclen.$$.out
fi

#   <copyright 
#   notice="lm-source-program" 
#   pids="5724-H72," 
#   years="2005,2012" 
#   crc="1595222582" > 
#  Licensed Materials - Property of IBM  
#   
#  5724-H72, 
#   
#  (C) Copyright IBM Corp. 2005, 2012 All Rights Reserved.  
#   
#  US Government Users Restricted Rights - Use, duplication or  
#  disclosure restricted by GSA ADP Schedule Contract with  
#  IBM Corp.  
#   </copyright> 
#
# Pre-uninstallation check script for all components
# A check is performed to see if there are any fixpack filesets applied to
# the base component which is currently being uninstalled.  If the fixpack
# has been applied, the uninstallation of this component is aborted to prevent
# the situation where the base fileset has been uninstalled leaving an
# uninstallable fixpack.

FIXPACK_BACKUPDIR="${MQ_INSTALLATION_PATH}/maintenance"

find $FIXPACK_BACKUPDIR -type d -maxdepth 2 -print 2>/dev/null | \
      while read directory ; do
  component=`basename $directory`
  if [ "$RPM_PACKAGE_NAME" = "$component" ]; then
    # safety check - are there actually files under this directory?
    num_files=`find "$directory" -type f -print 2>/dev/null | wc -l`
    if [ $num_files -gt 0 ]; then
      cat << EOF

  ERROR:  There appears to be a fixpack installed on this machine for this
          component.

  Please ensure you have removed all fixpacks for the \"$RPM_PACKAGE_NAME\"
  component before trying to remove this package.

EOF
      exit 1
    fi
  fi
done

# Removing product links

%postun
RPM_PACKAGE_SUMMARY="WebSphere MQ Telemetry Clients"
RPM_PACKAGE_NAME="MQSeriesXRClients"
RPM_PACKAGE_VERSION="7.5.0"
RPM_PACKAGE_RELEASE="2"
PACKAGEPLATFORMS="x86_64"
MQ_DEFAULT_INSTALLATION_PATH=/opt/mqm
MQ_INSTALLATION_PATH=${RPM_INSTALL_PREFIX}
MQ_RUNSCRIPT=
echo ${SHELLOPTS} | grep xtrace > /dev/null
if [ $? -eq 0 ] ; then 
  env | sort
  uname -a
  id
  MQ_RUNSCRIPT="sh -x"
fi

