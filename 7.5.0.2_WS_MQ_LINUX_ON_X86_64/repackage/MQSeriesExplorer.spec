Summary: WebSphere MQ Explorer
Name: MQSeriesExplorer
Version: 7.5.0
Release: 2
License: Commercial
ExclusiveArch: x86_64
Group: Applications/Networking
AutoReqProv: no
Vendor: IBM
Prefix: /opt/mqm
Requires: MQSeriesRuntime = 7.5.0-2
Requires: MQSeriesJRE = 7.5.0-2
Requires: gtk2 >= 2.2.4-0
%global __strip /bin/true
%global _rpmdir /build/slot1/p750_P/inst.images/amd64_linux_2/images/
%global _tmppath /build/slot1/p750_P/obj/amd64_linux_2/install/unix/linux_2
BuildRoot: /build/slot1/p750_P/obj/amd64_linux_2/ship

%description
IBM WebSphere MQ for Developers for Linux for x86_64
5724-H72
%clean
rm -rf $RPM_BUILD_ROOT

%install
install -d $RPM_BUILD_ROOT/opt/mqm
install -d $RPM_BUILD_ROOT/opt/mqm/properties
install -d $RPM_BUILD_ROOT/opt/mqm/properties/version
install -d $RPM_BUILD_ROOT/opt/mqm/bin
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.eclipse.platform.feature.nl2_7.5.0.201307041350
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.eclipse.platform.feature_7.5.0.201307041350
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.eclipse.platform.feature.nl1_7.5.0.201307041350
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.runtime.thirdparty_7.5.0.201307041350
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.runtime.thirdparty_7.5.0.201307041350/lib
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.runtime.thirdparty_7.5.0.201307041350/META-INF
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/bin
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/etc
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/etc/checkstyle
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/META-INF
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/about_files
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl2_1.1.2.v201102251823
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl2_1.1.2.v201102251823/META-INF
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl1_3.3.0.v201102251823
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl1_3.3.0.v201102251823/META-INF
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl2_3.3.0.v201102251823
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl2_3.3.0.v201102251823/META-INF
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl1_1.1.2.v201102251823
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl1_1.1.2.v201102251823/META-INF
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry_3.3.0.v20100520
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry_3.3.0.v20100520/META-INF
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.weaver_1.6.9.0
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.weaver_1.6.9.0/META-INF
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.runtime_1.6.9.0
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.runtime_1.6.9.0/META-INF
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.runtime_7.5.0.201307041350
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.runtime_7.5.0.201307041350/lib
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.runtime_7.5.0.201307041350/META-INF
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64_1.1.2.R36x_v20101019_1345
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64_1.1.2.R36x_v20101019_1345/META-INF
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/configuration
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/configuration/org.eclipse.equinox.simpleconfigurator
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/configuration/org.eclipse.update
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/dropins
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/about_files
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/links
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/p2
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/.data
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/.data/org.eclipse.equinox.internal.p2.touchpoint.eclipse.actions
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/.data/.settings
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/.settings
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.core
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.core/cache
install -d $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.core/cache/binary
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/properties/version/IBM_WebSphere_MQ_Explorer.7.5.0.cmptag $RPM_BUILD_ROOT/opt/mqm/properties/version/IBM_WebSphere_MQ_Explorer.7.5.0.cmptag
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/strmqcfg $RPM_BUILD_ROOT/opt/mqm/bin/strmqcfg
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/MQExplorer $RPM_BUILD_ROOT/opt/mqm/bin/MQExplorer
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/MQExplorer.ini $RPM_BUILD_ROOT/opt/mqm/bin/MQExplorer.ini
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_ko.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_ko.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_zh_CN.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_zh_CN.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_pl.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_pl.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_hu.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_hu.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_de.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_de.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_lt.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_lt.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_cs.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_cs.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_ru.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_ru.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_it.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_it.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_gr.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_gr.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_es.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_es.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_tr.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_tr.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_ja_JP.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_ja_JP.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_zh_TW.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_zh_TW.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_pt_BR.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_pt_BR.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_fr.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_fr.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_si.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_si.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_id.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_id.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_es.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_es.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/license_cs.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/license_cs.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/license_it.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/license_it.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_tr.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_tr.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_zh_CN.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_zh_CN.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/license_zh_CN.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/license_zh_CN.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/license_pt_BR.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/license_pt_BR.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/category.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/category.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_fr.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_fr.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/license_fr.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/license_fr.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/license_zh_TW.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/license_zh_TW.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/license_ja_JP.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/license_ja_JP.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_si.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_si.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_id.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_id.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/license_hu.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/license_hu.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_ko.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_ko.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_pl.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_pl.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/license_pl.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/license_pl.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/license_ru.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/license_ru.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_hu.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_hu.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_de.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_de.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/license_es.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/license_es.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/license_de.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/license_de.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_ja_JP.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_ja_JP.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_lt.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_lt.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_cs.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_cs.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/license.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/license.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_zh_TW.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_zh_TW.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_ru.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_ru.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_it.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_it.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_pt_BR.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_pt_BR.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/license_ko.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/license_ko.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_gr.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_gr.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.eclipse.platform.feature.nl2_7.5.0.201307041350/feature.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.eclipse.platform.feature.nl2_7.5.0.201307041350/feature.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.eclipse.platform.feature.nl2_7.5.0.201307041350/feature.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.eclipse.platform.feature.nl2_7.5.0.201307041350/feature.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.eclipse.platform.feature_7.5.0.201307041350/feature.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.eclipse.platform.feature_7.5.0.201307041350/feature.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.eclipse.platform.feature_7.5.0.201307041350/feature.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.eclipse.platform.feature_7.5.0.201307041350/feature.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.eclipse.platform.feature.nl1_7.5.0.201307041350/feature.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.eclipse.platform.feature.nl1_7.5.0.201307041350/feature.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.eclipse.platform.feature.nl1_7.5.0.201307041350/feature.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.eclipse.platform.feature.nl1_7.5.0.201307041350/feature.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.nl1_3.6.2.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.nl1_3.6.2.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.coretests.cluster_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.coretests.cluster_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.runtime.thirdparty_7.5.0.201307041350/lib/jta.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.runtime.thirdparty_7.5.0.201307041350/lib/jta.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.runtime.thirdparty_7.5.0.201307041350/lib/connector.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.runtime.thirdparty_7.5.0.201307041350/lib/connector.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.runtime.thirdparty_7.5.0.201307041350/lib/providerutil.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.runtime.thirdparty_7.5.0.201307041350/lib/providerutil.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.runtime.thirdparty_7.5.0.201307041350/lib/fscontext.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.runtime.thirdparty_7.5.0.201307041350/lib/fscontext.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.runtime.thirdparty_7.5.0.201307041350/lib/jms.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.runtime.thirdparty_7.5.0.201307041350/lib/jms.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.runtime.thirdparty_7.5.0.201307041350/META-INF/MANIFEST.MF $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.runtime.thirdparty_7.5.0.201307041350/META-INF/MANIFEST.MF
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/bin/ant $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/bin/ant
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/bin/antRun.bat $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/bin/antRun.bat
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/bin/runrc.cmd $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/bin/runrc.cmd
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/bin/lcp.bat $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/bin/lcp.bat
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/bin/antRun $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/bin/antRun
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/bin/complete-ant-cmd.pl $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/bin/complete-ant-cmd.pl
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/bin/ant.bat $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/bin/ant.bat
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/bin/envset.cmd $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/bin/envset.cmd
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/bin/antenv.cmd $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/bin/antenv.cmd
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/bin/ant.cmd $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/bin/ant.cmd
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/bin/antRun.pl $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/bin/antRun.pl
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/bin/runant.pl $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/bin/runant.pl
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/bin/runant.py $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/bin/runant.py
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-jdepend.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-jdepend.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-apache-regexp.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-apache-regexp.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-nodeps.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-nodeps.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-jai.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-jai.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-stylebook.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-stylebook.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-weblogic.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-weblogic.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-antlr.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-antlr.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-launcher.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-launcher.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-commons-logging.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-commons-logging.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-starteam.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-starteam.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-apache-bsf.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-apache-bsf.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-jsch.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-jsch.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-swing.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-swing.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-jmf.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-jmf.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-commons-net.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-commons-net.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-trax.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-trax.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-apache-bcel.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-apache-bcel.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-apache-oro.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-apache-oro.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-apache-resolver.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-apache-resolver.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-netrexx.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-netrexx.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-apache-log4j.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-apache-log4j.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-javamail.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-javamail.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-junit.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-junit.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/etc/mmetrics-frames.xsl $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/etc/mmetrics-frames.xsl
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/etc/tagdiff.xsl $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/etc/tagdiff.xsl
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/etc/maudit-frames.xsl $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/etc/maudit-frames.xsl
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/etc/log.xsl $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/etc/log.xsl
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/etc/checkstyle/checkstyle-frames.xsl $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/etc/checkstyle/checkstyle-frames.xsl
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/etc/checkstyle/checkstyle-xdoc.xsl $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/etc/checkstyle/checkstyle-xdoc.xsl
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/etc/checkstyle/checkstyle-text.xsl $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/etc/checkstyle/checkstyle-text.xsl
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/etc/changelog.xsl $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/etc/changelog.xsl
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/etc/jdepend.xsl $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/etc/jdepend.xsl
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/etc/junit-frames-xalan1.xsl $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/etc/junit-frames-xalan1.xsl
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/etc/jdepend-frames.xsl $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/etc/jdepend-frames.xsl
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/etc/coverage-frames.xsl $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/etc/coverage-frames.xsl
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/etc/junit-frames.xsl $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/etc/junit-frames.xsl
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/etc/junit-noframes.xsl $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/etc/junit-noframes.xsl
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/META-INF/ECLIPSEF.SF $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/META-INF/ECLIPSEF.SF
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/META-INF/eclipse.inf $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/META-INF/eclipse.inf
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/META-INF/ECLIPSEF.RSA $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/META-INF/ECLIPSEF.RSA
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/META-INF/MANIFEST.MF $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/META-INF/MANIFEST.MF
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/about_files/NOTICE $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/about_files/NOTICE
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/about_files/asl-v20.txt $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/about_files/asl-v20.txt
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/about_files/LICENSE.sax.txt $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/about_files/LICENSE.sax.txt
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/about_files/LICENSE.dom.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/about_files/LICENSE.dom.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/about.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/about.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/plugin.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/plugin.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.jmsadmin.tests.nl1_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.jmsadmin.tests.nl1_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl2_1.1.2.v201102251823/about.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl2_1.1.2.v201102251823/about.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl2_1.1.2.v201102251823/META-INF/MANIFEST.MF $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl2_1.1.2.v201102251823/META-INF/MANIFEST.MF
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl2_1.1.2.v201102251823/launcher.gtk.linux.x86_64_pl.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl2_1.1.2.v201102251823/launcher.gtk.linux.x86_64_pl.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl2_1.1.2.v201102251823/nl_fragment.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl2_1.1.2.v201102251823/nl_fragment.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl2_1.1.2.v201102251823/launcher.gtk.linux.x86_64_hu.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl2_1.1.2.v201102251823/launcher.gtk.linux.x86_64_hu.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl2_1.1.2.v201102251823/launcher.gtk.linux.x86_64_cs.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl2_1.1.2.v201102251823/launcher.gtk.linux.x86_64_cs.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl2_1.1.2.v201102251823/launcher.gtk.linux.x86_64_ru.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl2_1.1.2.v201102251823/launcher.gtk.linux.x86_64_ru.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.infopop.nl1_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.infopop.nl1_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.core.nl2_2.0.3.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.core.nl2_2.0.3.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl1_3.3.0.v201102251823/about.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl1_3.3.0.v201102251823/about.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl1_3.3.0.v201102251823/META-INF/MANIFEST.MF $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl1_3.3.0.v201102251823/META-INF/MANIFEST.MF
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl1_3.3.0.v201102251823/nl_fragment.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl1_3.3.0.v201102251823/nl_fragment.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl1_3.3.0.v201102251823/fragment_fr.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl1_3.3.0.v201102251823/fragment_fr.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl1_3.3.0.v201102251823/fragment_zh.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl1_3.3.0.v201102251823/fragment_zh.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl1_3.3.0.v201102251823/fragment_ko.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl1_3.3.0.v201102251823/fragment_ko.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl1_3.3.0.v201102251823/fragment_zh_TW.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl1_3.3.0.v201102251823/fragment_zh_TW.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl1_3.3.0.v201102251823/fragment_pt_BR.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl1_3.3.0.v201102251823/fragment_pt_BR.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl1_3.3.0.v201102251823/fragment_de.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl1_3.3.0.v201102251823/fragment_de.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl1_3.3.0.v201102251823/fragment_zh_HK.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl1_3.3.0.v201102251823/fragment_zh_HK.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl1_3.3.0.v201102251823/fragment_it.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl1_3.3.0.v201102251823/fragment_it.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl1_3.3.0.v201102251823/fragment_ja.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl1_3.3.0.v201102251823/fragment_ja.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl1_3.3.0.v201102251823/fragment_es.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl1_3.3.0.v201102251823/fragment_es.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer.ssl.nl2_1.0.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer.ssl.nl2_1.0.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.concurrent.nl1_1.0.100.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.concurrent.nl1_1.0.100.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl2_3.3.0.v201102251823/fragment_ru.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl2_3.3.0.v201102251823/fragment_ru.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl2_3.3.0.v201102251823/META-INF/MANIFEST.MF $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl2_3.3.0.v201102251823/META-INF/MANIFEST.MF
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl2_3.3.0.v201102251823/nl_fragment.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl2_3.3.0.v201102251823/nl_fragment.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl2_3.3.0.v201102251823/fragment_pl.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl2_3.3.0.v201102251823/fragment_pl.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl2_3.3.0.v201102251823/fragment_hu.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl2_3.3.0.v201102251823/fragment_hu.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl2_3.3.0.v201102251823/fragment_cs.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl2_3.3.0.v201102251823/fragment_cs.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl2_3.3.0.v201102251823/about.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl2_3.3.0.v201102251823/about.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.variables.nl1_3.2.400.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.variables.nl1_3.2.400.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.clusterplugin.nl1_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.clusterplugin.nl1_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.mapping.mqjms.nl1_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.mapping.mqjms.nl1_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.artifact.repository.nl1_1.1.1.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.artifact.repository.nl1_1.1.1.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.telemetry.ui_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.telemetry.ui_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.text.nl1_3.5.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.text.nl1_3.5.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.registry.nl1_3.5.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.registry.nl1_3.5.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.jobs.nl1_3.5.1.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.jobs.nl1_3.5.1.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.app.nl1_1.3.1.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.app.nl1_1.3.1.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.common_3.6.0.v20100503.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.common_3.6.0.v20100503.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl1_1.1.2.v201102251823/about.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl1_1.1.2.v201102251823/about.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl1_1.1.2.v201102251823/META-INF/MANIFEST.MF $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl1_1.1.2.v201102251823/META-INF/MANIFEST.MF
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl1_1.1.2.v201102251823/launcher.gtk.linux.x86_64_fr.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl1_1.1.2.v201102251823/launcher.gtk.linux.x86_64_fr.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl1_1.1.2.v201102251823/nl_fragment.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl1_1.1.2.v201102251823/nl_fragment.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl1_1.1.2.v201102251823/launcher.gtk.linux.x86_64_zh_TW.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl1_1.1.2.v201102251823/launcher.gtk.linux.x86_64_zh_TW.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl1_1.1.2.v201102251823/launcher.gtk.linux.x86_64_zh.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl1_1.1.2.v201102251823/launcher.gtk.linux.x86_64_zh.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl1_1.1.2.v201102251823/launcher.gtk.linux.x86_64_ko.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl1_1.1.2.v201102251823/launcher.gtk.linux.x86_64_ko.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl1_1.1.2.v201102251823/launcher.gtk.linux.x86_64_pt_BR.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl1_1.1.2.v201102251823/launcher.gtk.linux.x86_64_pt_BR.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl1_1.1.2.v201102251823/launcher.gtk.linux.x86_64_de.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl1_1.1.2.v201102251823/launcher.gtk.linux.x86_64_de.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl1_1.1.2.v201102251823/launcher.gtk.linux.x86_64_zh_HK.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl1_1.1.2.v201102251823/launcher.gtk.linux.x86_64_zh_HK.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl1_1.1.2.v201102251823/launcher.gtk.linux.x86_64_it.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl1_1.1.2.v201102251823/launcher.gtk.linux.x86_64_it.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl1_1.1.2.v201102251823/launcher.gtk.linux.x86_64_ja.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl1_1.1.2.v201102251823/launcher.gtk.linux.x86_64_ja.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl1_1.1.2.v201102251823/launcher.gtk.linux.x86_64_es.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl1_1.1.2.v201102251823/launcher.gtk.linux.x86_64_es.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.nl1_3.5.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.nl1_3.5.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.swt.nl1_3.6.2.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.swt.nl1_3.6.2.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.compare.core.nl1_3.5.101.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.compare.core.nl1_3.5.101.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.resources_3.6.1.R36x_v20110131-1630.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.resources_3.6.1.R36x_v20110131-1630.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.garbagecollector_1.0.100.v20100503.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.garbagecollector_1.0.100.v20100503.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.preferences.nl1_3.3.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.preferences.nl1_3.3.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.base.nl1_3.5.3.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.base.nl1_3.5.3.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.resources.nl2_3.6.1.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.resources.nl2_3.6.1.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.branding_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.branding_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.debug.core.nl2_3.6.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.debug.core.nl2_3.6.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.telemetry.mqtt.utility_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.telemetry.mqtt.utility_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.observable_1.3.0.I20100601-0800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.observable_1.3.0.I20100601-0800.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry_3.3.0.v20100520/about.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry_3.3.0.v20100520/about.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry_3.3.0.v20100520/META-INF/ECLIPSEF.SF $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry_3.3.0.v20100520/META-INF/ECLIPSEF.SF
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry_3.3.0.v20100520/META-INF/eclipse.inf $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry_3.3.0.v20100520/META-INF/eclipse.inf
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry_3.3.0.v20100520/META-INF/ECLIPSEF.RSA $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry_3.3.0.v20100520/META-INF/ECLIPSEF.RSA
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry_3.3.0.v20100520/META-INF/MANIFEST.MF $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry_3.3.0.v20100520/META-INF/MANIFEST.MF
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry_3.3.0.v20100520/runtime_registry_compatibility.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry_3.3.0.v20100520/runtime_registry_compatibility.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry_3.3.0.v20100520/fragment.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry_3.3.0.v20100520/fragment.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry_3.3.0.v20100520/.api_description $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry_3.3.0.v20100520/.api_description
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.ui.rcp.nl1_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.ui.rcp.nl1_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.ide.application.nl2_1.0.201.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.ide.application.nl2_1.0.201.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filesystem.linux.x86_64_1.2.0.v20100505-1235.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filesystem.linux.x86_64_1.2.0.v20100505-1235.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.telemetry.mqtt.utility.nl1_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.telemetry.mqtt.utility.nl1_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.sat4j.core_2.2.0.v20100429.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.sat4j.core_2.2.0.v20100429.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.variables_3.2.400.v20100505.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.variables_3.2.400.v20100505.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.jmsadmin.infopop_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.jmsadmin.infopop_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.engine.nl1_2.0.1.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.engine.nl1_2.0.1.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.ds.nl2_1.2.1.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.ds.nl2_1.2.1.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.nl2_1.3.100.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.nl2_1.3.100.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.webapp.nl1_3.5.3.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.webapp.nl1_3.5.3.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.mapping.mqjms_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.mapping.mqjms_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.weaver_1.6.9.0/META-INF/MANIFEST.MF $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.weaver_1.6.9.0/META-INF/MANIFEST.MF
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.weaver_1.6.9.0/about.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.weaver_1.6.9.0/about.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.weaver_1.6.9.0/plugin.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.weaver_1.6.9.0/plugin.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.weaver_1.6.9.0/aspectjweaver.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.weaver_1.6.9.0/aspectjweaver.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.update.core.nl1_3.2.402.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.update.core.nl1_3.2.402.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.importexport.nl1_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.importexport.nl1_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.sdk.scheduler_1.0.0.v20100507-1815.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.sdk.scheduler_1.0.0.v20100507-1815.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.runtime_1.6.9.0/META-INF/MANIFEST.MF $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.runtime_1.6.9.0/META-INF/MANIFEST.MF
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.runtime_1.6.9.0/plugin.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.runtime_1.6.9.0/plugin.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.runtime_1.6.9.0/aspectjrt.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.runtime_1.6.9.0/aspectjrt.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.runtime_1.6.9.0/about.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.runtime_1.6.9.0/about.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.contenttype.nl1_3.4.100.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.contenttype.nl1_3.4.100.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.commands.nl2_3.6.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.commands.nl2_3.6.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.frameworkadmin.equinox.nl2_1.0.200.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.frameworkadmin.equinox.nl2_1.0.200.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.runtime_7.5.0.201307041350/lib/com.ibm.wmqfte.bootstrap.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.runtime_7.5.0.201307041350/lib/com.ibm.wmqfte.bootstrap.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.runtime_7.5.0.201307041350/lib/com.ibm.wmqfte.cmdline.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.runtime_7.5.0.201307041350/lib/com.ibm.wmqfte.cmdline.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.runtime_7.5.0.201307041350/lib/com.ibm.wmqfte.common.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.runtime_7.5.0.201307041350/lib/com.ibm.wmqfte.common.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.runtime_7.5.0.201307041350/lib/com.ibm.wmqfte.native.jni.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.runtime_7.5.0.201307041350/lib/com.ibm.wmqfte.native.jni.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.runtime_7.5.0.201307041350/META-INF/MANIFEST.MF $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.runtime_7.5.0.201307041350/META-INF/MANIFEST.MF
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.metadata.nl1_2.0.1.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.metadata.nl1_2.0.1.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.weaving.caching.j9_1.0.0.v20100503.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.weaving.caching.j9_1.0.0.v20100503.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.directorywatcher.nl1_1.0.203.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.directorywatcher.nl1_1.0.203.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface.text.nl1_3.6.1.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface.text.nl1_3.6.1.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.jasper_5.5.17.v201004212143.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.jasper_5.5.17.v201004212143.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.publisher_1.1.2.v20100824-2220.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.publisher_1.1.2.v20100824-2220.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.extensionlocation.nl2_1.2.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.extensionlocation.nl2_1.2.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.messageplugin.internal.nl1_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.messageplugin.internal.nl1_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.repository.nl1_2.0.2.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.repository.nl1_2.0.2.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.jsp.jasper.registry_1.0.200.v20100503.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.jsp.jasper.registry_1.0.200.v20100503.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.swt_3.6.2.v3659c.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.swt_3.6.2.v3659c.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.servlet.nl2_1.1.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.servlet.nl2_1.1.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui_3.6.2.M20110203-1100.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui_3.6.2.M20110203-1100.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.swt.gtk.linux.x86_64.nl2_3.6.2.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.swt.gtk.linux.x86_64.nl2_3.6.2.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.ide.nl2_3.6.2.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.ide.nl2_3.6.2.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.updatechecker.nl2_1.1.101.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.updatechecker.nl2_1.1.101.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.reconciler.dropins.nl2_1.1.2.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.reconciler.dropins.nl2_1.1.2.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.identity.nl1_3.1.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.identity.nl1_3.1.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.oam_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.oam_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.concurrent.nl2_1.0.100.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.concurrent.nl2_1.0.100.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.variables.nl2_3.2.400.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.variables.nl2_3.2.400.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64_1.1.2.R36x_v20101019_1345/META-INF/ECLIPSEF.SF $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64_1.1.2.R36x_v20101019_1345/META-INF/ECLIPSEF.SF
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64_1.1.2.R36x_v20101019_1345/META-INF/eclipse.inf $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64_1.1.2.R36x_v20101019_1345/META-INF/eclipse.inf
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64_1.1.2.R36x_v20101019_1345/META-INF/ECLIPSEF.RSA $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64_1.1.2.R36x_v20101019_1345/META-INF/ECLIPSEF.RSA
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64_1.1.2.R36x_v20101019_1345/META-INF/MANIFEST.MF $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64_1.1.2.R36x_v20101019_1345/META-INF/MANIFEST.MF
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64_1.1.2.R36x_v20101019_1345/launcher.gtk.linux.x86_64.properties $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64_1.1.2.R36x_v20101019_1345/launcher.gtk.linux.x86_64.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64_1.1.2.R36x_v20101019_1345/eclipse_1310.so $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64_1.1.2.R36x_v20101019_1345/eclipse_1310.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64_1.1.2.R36x_v20101019_1345/about.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64_1.1.2.R36x_v20101019_1345/about.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.osgi.services.nl1_3.2.100.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.osgi.services.nl1_3.2.100.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.workbench.texteditor.nl1_3.6.1.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.workbench.texteditor.nl1_3.6.1.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface.nl1_3.6.2.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface.nl1_3.6.2.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.ui.nl1_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.ui.nl1_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.jmsadmin.sample.menus_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.jmsadmin.sample.menus_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.console.nl1_3.5.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.console.nl1_3.5.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.icu_4.2.1.v20100412.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.icu_4.2.1.v20100412.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.weaving.aspectj_1.0.0.v20100503.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.weaving.aspectj_1.0.0.v20100503.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer.nl2_3.1.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer.nl2_3.1.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.compare.core.nl2_3.5.101.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.compare.core.nl2_3.5.101.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.auth_3.2.200.v20100517.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.auth_3.2.200.v20100517.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer_3.1.0.v20100906-1425.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer_3.1.0.v20100906-1425.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.sdk_1.0.100.v20100513.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.sdk_1.0.100.v20100513.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filesystem_1.3.1.R36x_v20100727-0745.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filesystem_1.3.1.R36x_v20100727-0745.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.mapping.mqjms.infopop.nl1_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.mapping.mqjms.infopop.nl1_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.metadata.generator.nl1_1.0.202.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.metadata.generator.nl1_1.0.202.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.jmsadmin_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.jmsadmin_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.nl2_3.6.2.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.nl2_3.6.2.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.simpleconfigurator.manipulator.nl1_2.0.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.simpleconfigurator.manipulator.nl1_2.0.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.swt.gtk.linux.x86_64_3.6.2.v3659b.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.swt.gtk.linux.x86_64_3.6.2.v3659b.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility_3.2.100.v20100505.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility_3.2.100.v20100505.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.sample.simple_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.sample.simple_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding_1.3.100.I20100601-0800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding_1.3.100.I20100601-0800.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.artifact.repository.nl2_1.1.1.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.artifact.repository.nl2_1.1.1.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.text.nl2_3.5.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.text.nl2_3.5.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.jmsadmin.nl1_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.jmsadmin.nl1_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.security.nl1_1.0.200.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.security.nl1_1.0.200.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.registry.nl2_3.5.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.registry.nl2_3.5.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.jobs.nl2_3.5.1.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.jobs.nl2_3.5.1.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.weaving.caching_1.0.0.v20100503.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.weaving.caching_1.0.0.v20100503.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.app.nl2_1.3.1.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.app.nl2_1.3.1.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.draw2d.nl1_3.6.2.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.draw2d.nl1_3.6.2.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.lucene_1.9.1.v20100518-1140.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.lucene_1.9.1.v20100518-1140.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.nl2_3.5.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.nl2_3.5.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.swt.nl2_3.6.2.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.swt.nl2_3.6.2.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.sets.infopop.nl1_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.sets.infopop.nl1_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.telemetry.ui.nl1_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.telemetry.ui.nl1_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.util_1.0.200.v20100503.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.util_1.0.200.v20100503.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.touchpoint.eclipse.nl1_2.0.3.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.touchpoint.eclipse.nl1_2.0.3.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.preferences.nl2_3.3.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.preferences.nl2_3.3.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.ui.rcp_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.ui.rcp_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.compare.core_3.5.101.R36x_v20100929-0800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.compare.core_3.5.101.R36x_v20100929-0800.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.base.nl2_3.5.3.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.base.nl2_3.5.3.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.console_3.5.0.v20100526.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.console_3.5.0.v20100526.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher_1.1.1.R36x_v20101122_1400.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher_1.1.1.R36x_v20101122_1400.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.update.core.nl2_3.2.402.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.update.core.nl2_3.2.402.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.update.configurator_3.3.100.v20100512.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.update.configurator_3.3.100.v20100512.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.repository_2.0.2.R36x_v20110111-1500.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.repository_2.0.2.R36x_v20110111-1500.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.contenttype.nl2_3.4.100.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.contenttype.nl2_3.4.100.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.topicstatus.infopop.nl1_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.topicstatus.infopop.nl1_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filesystem.linux.x86_64.nl1_1.2.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filesystem.linux.x86_64.nl1_1.2.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.directorywatcher.nl2_1.0.203.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.directorywatcher.nl2_1.0.203.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface_3.6.2.M20110210-1200.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface_3.6.2.M20110210-1200.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.telemetry.mqtt.utility_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.telemetry.mqtt.utility_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.garbagecollector.nl1_1.0.100.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.garbagecollector.nl1_1.0.100.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.topicstatus.nl1_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.topicstatus.nl1_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.doc.nl1_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.doc.nl1_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.engine.nl2_2.0.1.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.engine.nl2_2.0.1.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.pubsub.nl1_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.pubsub.nl1_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.webapp.nl2_3.5.3.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.webapp.nl2_3.5.3.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.ssl.nl1_1.0.100.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.ssl.nl1_1.0.100.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.property_1.3.0.I20100601-0800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.property_1.3.0.I20100601-0800.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help_3.5.0.v20100524.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help_3.5.0.v20100524.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.sdk.nl1_1.0.100.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.sdk.nl1_1.0.100.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.traceplugin.internal_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.traceplugin.internal_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.passwords_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.passwords_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.engine_2.0.1.R36x_v20110201.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.engine_2.0.1.R36x_v20110201.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.coretests_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.coretests_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.metadata.nl2_2.0.1.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.metadata.nl2_2.0.1.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.osgi.services.nl2_3.2.100.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.osgi.services.nl2_3.2.100.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.core_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.core_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.navigator_3.5.0.I20100601-0800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.navigator_3.5.0.I20100601-0800.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface.text.nl2_3.6.1.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface.text.nl2_3.6.1.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.registry.nl1_1.1.1.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.registry.nl1_1.1.1.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.jobs_3.5.1.R36x_v20100824.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.jobs_3.5.1.R36x_v20100824.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.mortbay.jetty.server_6.1.23.v201004211559.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.mortbay.jetty.server_6.1.23.v201004211559.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.debug.ui_3.6.3.v20101201_r362.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.debug.ui_3.6.3.v20101201_r362.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.coretests.nl1_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.coretests.nl1_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.publisher.nl1_1.1.2.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.publisher.nl1_1.1.2.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.core_2.0.3.R36x_v20110111.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.core_2.0.3.R36x_v20110111.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.trace.weaving_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.trace.weaving_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ant.core.nl1_3.2.201.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ant.core.nl1_3.2.201.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ant.core_3.2.201.v20110203_r362.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ant.core_3.2.201.v20110203_r362.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.repository.nl2_2.0.2.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.repository.nl2_2.0.2.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.expressions.nl1_3.4.200.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.expressions.nl1_3.4.200.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.core.nl1_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.core.nl1_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.metadata.generator.nl2_1.0.202.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.metadata.generator.nl2_1.0.202.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.identity.nl2_3.1.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.identity.nl2_3.1.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.update.configurator.nl1_3.3.100.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.update.configurator.nl1_3.3.100.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.security.nl2_1.0.200.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.security.nl2_1.0.200.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.extensionlocation_1.2.0.v20100518.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.extensionlocation_1.2.0.v20100518.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.util.nl1_1.0.200.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.util.nl1_1.0.200.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.workbench.texteditor.nl2_3.6.1.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.workbench.texteditor.nl2_3.6.1.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.metadata_2.0.1.R36x_v20101202.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.metadata_2.0.1.R36x_v20101202.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface.nl2_3.6.2.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface.nl2_3.6.2.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.update.ui_3.2.300.v20100512.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.update.ui_3.2.300.v20100512.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.console.nl2_3.5.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.console.nl2_3.5.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.artifact.repository_1.1.1.R36x_v20100901.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.artifact.repository_1.1.1.R36x_v20100901.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.frameworkadmin.nl1_2.0.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.frameworkadmin.nl1_2.0.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.net.nl1_1.2.100.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.net.nl1_1.2.100.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.telemetry.mqtt.utility.nl1_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.telemetry.mqtt.utility.nl1_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.text_3.5.0.v20100601-1300.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.text_3.5.0.v20100601-1300.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.jarprocessor.nl1_1.0.200.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.jarprocessor.nl1_1.0.200.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.simpleconfigurator.manipulator.nl2_2.0.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.simpleconfigurator.manipulator.nl2_2.0.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.common.nl1_3.6.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.common.nl1_3.6.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.expressions_3.4.200.v20100505.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.expressions_3.4.200.v20100505.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.security.ui_1.0.200.v20100503.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.security.ui_1.0.200.v20100503.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.net.linux.x86_64.nl1_1.1.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.net.linux.x86_64.nl1_1.1.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.registry_1.1.1.R36x_v20101103.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.registry_1.1.1.R36x_v20101103.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.nl1_3.1.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.nl1_3.1.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.coretests.cluster.nl1_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.coretests.cluster.nl1_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.nl1_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.nl1_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.weaving.hook_1.0.0.v20100503.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.weaving.hook_1.0.0.v20100503.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.garbagecollector.nl2_1.0.100.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.garbagecollector.nl2_1.0.100.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.draw2d.nl2_3.6.2.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.draw2d.nl2_3.6.2.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.net.linux.x86_64_1.1.0.I20100505-1245.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.net.linux.x86_64_1.1.0.I20100505-1245.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.frameworkadmin.equinox_1.0.200.v20100505.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.frameworkadmin.equinox_1.0.200.v20100505.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.workbench_3.6.2.M20110210-1200.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.workbench_3.6.2.M20110210-1200.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filesystem.nl1_1.3.1.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filesystem.nl1_1.3.1.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.osgi.nl1_3.6.2.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.osgi.nl1_3.6.2.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.oam.infopop_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.oam.infopop_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui_2.0.0.v20100518.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui_2.0.0.v20100518.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.touchpoint.eclipse.nl2_2.0.3.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.touchpoint.eclipse.nl2_2.0.3.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.ssl.nl2_1.0.100.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.ssl.nl2_1.0.100.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.observable.nl1_1.3.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.observable.nl1_1.3.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.nl1_1.1.1.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.nl1_1.1.1.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer.ssl_1.0.0.v20100906-1425.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer.ssl_1.0.0.v20100906-1425.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.osgi_3.6.2.R36x_v20110210.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.osgi_3.6.2.R36x_v20110210.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.director_2.0.3.R36x_v20101117-1018.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.director_2.0.3.R36x_v20101117-1018.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.topicstatus_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.topicstatus_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.sdk.nl2_1.0.100.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.sdk.nl2_1.0.100.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.ui_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.ui_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.property.nl1_1.3.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.property.nl1_1.3.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.explorer_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.explorer_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.servlet_1.1.0.v20100503.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.servlet_1.1.0.v20100503.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.metadata.repository.nl1_1.1.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.metadata.repository.nl1_1.1.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filesystem.linux.x86_64.nl2_1.2.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filesystem.linux.x86_64.nl2_1.2.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.ams_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.ams_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.branding.nl1_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.branding.nl1_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.jsp.jasper.registry.nl1_1.0.200.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.jsp.jasper.registry.nl1_1.0.200.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.navigator.nl1_3.5.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.navigator.nl1_3.5.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.jetty_2.0.0.v20100503.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.jetty_2.0.0.v20100503.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filebuffers_3.5.100.v20100520-0800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filebuffers_3.5.100.v20100520-0800.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.jmsadmin.tests_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.jmsadmin.tests_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.explorer.context_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.explorer.context_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.security_1.0.200.v20100503.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.security_1.0.200.v20100503.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ant.core.nl2_3.2.201.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ant.core.nl2_3.2.201.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.commands_3.6.0.I20100512-1500.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.commands_3.6.0.I20100512-1500.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.expressions.nl2_3.4.200.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.expressions.nl2_3.4.200.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.nl1_3.4.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.nl1_3.4.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.lucene.analysis_1.9.1.v20100518-1140.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.lucene.analysis_1.9.1.v20100518-1140.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface.databinding_1.4.0.I20100601-0800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface.databinding_1.4.0.I20100601-0800.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.nl1_2.0.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.nl1_2.0.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.frameworkadmin_2.0.0.v20100503.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.frameworkadmin_2.0.0.v20100503.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.app_1.3.1.R36x_v20100803.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.app_1.3.1.R36x_v20100803.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.operations_2.0.0.v20100510.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.operations_2.0.0.v20100510.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.views_3.5.1.M20110202-0800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.views_3.5.1.M20110202-0800.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface.databinding.nl1_1.4.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface.databinding.nl1_1.4.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.commonservices_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.commonservices_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.sets.infopop_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.sets.infopop_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.update.configurator.nl2_3.3.100.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.update.configurator.nl2_3.3.100.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.workbench.texteditor_3.6.1.r361_v20100714-0800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.workbench.texteditor_3.6.1.r361_v20100714-0800.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.sdk.scheduler.nl1_1.0.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.sdk.scheduler.nl1_1.0.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.jsp.jasper_1.0.201.R36x_v20101103.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.jsp.jasper_1.0.201.R36x_v20101103.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.commons.el_1.0.0.v201004212143.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.commons.el_1.0.0.v201004212143.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.ds_1.2.1.R36x_v20100803.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.ds_1.2.1.R36x_v20100803.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.filetransfer_4.0.0.v20100906-1425.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.filetransfer_4.0.0.v20100906-1425.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.util.nl2_1.0.200.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.util.nl2_1.0.200.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.registry.nl2_1.1.1.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.registry.nl2_1.1.1.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.pcf.event_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.pcf.event_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.publisher.nl2_1.1.2.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.publisher.nl2_1.1.2.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.draw2d_3.6.2.v20110128-0100.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.draw2d_3.6.2.v20110128-0100.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.osgi.services_3.2.100.v20100503.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.osgi.services_3.2.100.v20100503.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.filetransfer.nl1_4.0.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.filetransfer.nl1_4.0.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.servicedef.infopop.nl1_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.servicedef.infopop.nl1_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.net.nl2_1.2.100.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.net.nl2_1.2.100.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.ui.infopop_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.ui.infopop_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.oam.tests_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.oam.tests_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.nl1_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.nl1_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.debug.ui.nl1_3.6.3.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.debug.ui.nl1_3.6.3.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.ui.nl1_3.5.3.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.ui.nl1_3.5.3.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.views.nl1_3.5.1.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.views.nl1_3.5.1.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.forms.nl1_3.5.2.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.forms.nl1_3.5.2.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.jarprocessor.nl2_1.0.200.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.jarprocessor.nl2_1.0.200.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.simpleconfigurator.nl1_1.0.200.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.simpleconfigurator.nl1_1.0.200.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.pubsub.infopop_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.pubsub.infopop_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.workbench.nl1_3.6.2.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.workbench.nl1_3.6.2.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filebuffers.nl1_3.5.100.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filebuffers.nl1_3.5.100.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.jetty.nl1_2.0.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.jetty.nl1_2.0.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.operations.nl1_2.0.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.operations.nl1_2.0.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.mapping.mqjms.infopop_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.mapping.mqjms.infopop_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.plugins_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.plugins_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.jsp.jasper.nl1_1.0.201.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.jsp.jasper.nl1_1.0.201.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.contenttype_3.4.100.v20100505-1235.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.contenttype_3.4.100.v20100505-1235.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.frameworkadmin.nl2_2.0.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.frameworkadmin.nl2_2.0.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.update.ui.nl1_3.2.300.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.update.ui.nl1_3.2.300.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.nl1_3.6.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.nl1_3.6.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.ssl_1.0.100.v20100906-1425.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.ssl_1.0.100.v20100906-1425.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.director.nl1_2.0.3.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.director.nl1_2.0.3.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.editors.nl1_3.6.1.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.editors.nl1_3.6.1.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.apache.commons.logging_1.0.4.v201005080501.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.apache.commons.logging_1.0.4.v201005080501.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.icu.nl1_4.2.1.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.icu.nl1_4.2.1.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.debug.core_3.6.0.v20100519.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.debug.core_3.6.0.v20100519.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.auth.nl1_3.2.200.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.auth.nl1_3.2.200.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.jarprocessor_1.0.200.v20100503a.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.jarprocessor_1.0.200.v20100503a.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.jmsadmin.infopop.nl1_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.jmsadmin.infopop.nl1_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.core.nl1_2.0.3.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.core.nl1_2.0.3.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime_3.6.0.v20100505.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime_3.6.0.v20100505.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.ide_3.6.2.M20101201-0800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.ide_3.6.2.M20101201-0800.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.ui_3.5.3.r36_20101116.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.ui_3.5.3.r36_20101116.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.common.nl2_3.6.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.common.nl2_3.6.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer.ssl.nl1_1.0.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer.ssl.nl1_1.0.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf_3.1.0.v20100906-1425.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf_3.1.0.v20100906-1425.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.net.linux.x86_64.nl2_1.1.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.net.linux.x86_64.nl2_1.1.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.simpleconfigurator_1.0.200.v20100503.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.simpleconfigurator_1.0.200.v20100503.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.webapp_3.5.4.r36_20120404.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.webapp_3.5.4.r36_20120404.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.reconciler.dropins_1.1.2.R36x_v20110114.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.reconciler.dropins_1.1.2.R36x_v20110114.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.nl2_3.1.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.nl2_3.1.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.jsp.jasper.registry.nl2_1.0.200.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.jsp.jasper.registry.nl2_1.0.200.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.sample_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.sample_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.sets_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.sets_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/javax.servlet_2.5.0.v200910301333.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/javax.servlet_2.5.0.v200910301333.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.servicedef_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.servicedef_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filesystem.nl2_1.3.1.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filesystem.nl2_1.3.1.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.nl1_3.2.100.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.nl1_3.2.100.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.osgi.nl2_3.6.2.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.osgi.nl2_3.6.2.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/javax.servlet.jsp_2.0.0.v200806031607.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/javax.servlet.jsp_2.0.0.v200806031607.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.directorywatcher_1.0.203.R36x_v20101220.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.directorywatcher_1.0.203.R36x_v20101220.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.observable.nl2_1.3.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.observable.nl2_1.3.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.nl2_1.1.1.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.nl2_1.1.1.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.micro.client.mqttv3.wrapper_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.micro.client.mqttv3.wrapper_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.resources.nl1_3.6.1.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.resources.nl1_3.6.1.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.sets.nl1_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.sets.nl1_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.debug.core.nl1_3.6.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.debug.core.nl1_3.6.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.compare_3.5.101.R36x_v20100929-0800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.compare_3.5.101.R36x_v20100929-0800.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.property.nl2_1.3.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.property.nl2_1.3.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.oam.tests.nl1_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.oam.tests.nl1_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface.text_3.6.1.r361_v20100825-0800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface.text_3.6.1.r361_v20100825-0800.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.net_1.2.100.I20100511-0800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.net_1.2.100.I20100511-0800.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.metadata.repository.nl2_1.1.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.metadata.repository.nl2_1.1.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.pubsub_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.pubsub_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.pubsub.infopop.nl1_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.pubsub.infopop.nl1_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.touchpoint.eclipse_2.0.3.R36x_v20101202.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.touchpoint.eclipse_2.0.3.R36x_v20101202.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.ide.application_1.0.201.M20100707-0800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.ide.application_1.0.201.M20100707-0800.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.importexport_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.importexport_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.navigator.nl2_3.5.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.navigator.nl2_3.5.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.ds.nl1_1.2.1.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.ds.nl1_1.2.1.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.sample.menus_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.sample.menus_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.infopop_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.infopop_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.runtime_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.runtime_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.ams.nl1_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.ams.nl1_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.nl2_3.4.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.nl2_3.4.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.nl2_2.0.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.nl2_2.0.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.simpleconfigurator.nl2_1.0.200.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.simpleconfigurator.nl2_1.0.200.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.mortbay.jetty.util_6.1.23.v201004211559.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.mortbay.jetty.util_6.1.23.v201004211559.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.base_3.5.3.v201102101200.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.base_3.5.3.v201102101200.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface.databinding.nl2_1.4.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface.databinding.nl2_1.4.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.metadata.repository_1.1.0.v20100513.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.metadata.repository_1.1.0.v20100513.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.messageplugin.internal_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.messageplugin.internal_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro_3.4.0.v20100427.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro_3.4.0.v20100427.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.commands.nl1_3.6.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.commands.nl1_3.6.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filebuffers.nl2_3.5.100.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filebuffers.nl2_3.5.100.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.sdk.scheduler.nl2_1.0.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.sdk.scheduler.nl2_1.0.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.simpleconfigurator.manipulator_2.0.0.v20100503.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.simpleconfigurator.manipulator_2.0.0.v20100503.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.ide.application.nl1_1.0.201.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.ide.application.nl1_1.0.201.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.servicedef.infopop_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.servicedef.infopop_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.update.core_3.2.402.R36x_v20100629.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.update.core_3.2.402.R36x_v20100629.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.updatechecker_1.1.101.R36x_v20100823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.updatechecker_1.1.101.R36x_v20100823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.extensionlocation.nl1_1.2.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.extensionlocation.nl1_1.2.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.jsp.jasper.nl2_1.0.201.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.jsp.jasper.nl2_1.0.201.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.traceplugin.internal.nl1_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.traceplugin.internal.nl1_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.metadata.generator_1.0.202.R36x_v20101208-1400.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.metadata.generator_1.0.202.R36x_v20101208-1400.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.editors_3.6.1.r361_v20100825-0800.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.editors_3.6.1.r361_v20100825-0800.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.registry_3.5.0.v20100503.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.registry_3.5.0.v20100503.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.nl1_1.3.100.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.nl1_1.3.100.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.filetransfer.nl2_4.0.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.filetransfer.nl2_4.0.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.update.ui.nl2_3.2.300.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.update.ui.nl2_3.2.300.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.topicstatus.infopop_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.topicstatus.infopop_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.servlet.nl1_1.1.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.servlet.nl1_1.1.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.swt.gtk.linux.x86_64.nl1_3.6.2.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.swt.gtk.linux.x86_64.nl1_3.6.2.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.ide.nl1_3.6.2.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.ide.nl1_3.6.2.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.debug.ui.nl2_3.6.3.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.debug.ui.nl2_3.6.3.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.ui.nl2_3.5.3.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.ui.nl2_3.5.3.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.views.nl2_3.5.1.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.views.nl2_3.5.1.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.reconciler.dropins.nl1_1.1.2.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.reconciler.dropins.nl1_1.1.2.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.forms.nl2_3.5.2.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.forms.nl2_3.5.2.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.auth.nl2_3.2.200.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.auth.nl2_3.2.200.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.servicedef.nl1_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.servicedef.nl1_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.doc_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.doc_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.preferences_3.3.0.v20100503.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.preferences_3.3.0.v20100503.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.oam.infopop.nl1_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.oam.infopop.nl1_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.ui.infopop.nl1_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.ui.infopop.nl1_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.workbench.nl2_3.6.2.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.workbench.nl2_3.6.2.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.jetty.nl2_2.0.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.jetty.nl2_2.0.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.operations.nl2_2.0.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.operations.nl2_2.0.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.oam.nl1_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.oam.nl1_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.frameworkadmin.equinox.nl1_1.0.200.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.frameworkadmin.equinox.nl1_1.0.200.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.concurrent_1.0.100.v20100503.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.concurrent_1.0.100.v20100503.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.identity_3.1.0.v20100906-1425.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.identity_3.1.0.v20100906-1425.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.forms_3.5.2.r36_v20100702.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.forms_3.5.2.r36_v20100702.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer.nl1_3.1.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer.nl1_3.1.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.nl2_3.2.100.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.nl2_3.2.100.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.nl2_3.6.0.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.nl2_3.6.0.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.clusterplugin_7.5.0.201307041350.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.clusterplugin_7.5.0.201307041350.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.editors.nl2_3.6.1.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.editors.nl2_3.6.1.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.director.nl2_2.0.3.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.director.nl2_2.0.3.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.sat4j.pb_2.2.0.v20100429.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.sat4j.pb_2.2.0.v20100429.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.updatechecker.nl1_1.1.101.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.updatechecker.nl1_1.1.101.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.icu.nl2_4.2.1.v201102251823.jar $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.icu.nl2_4.2.1.v201102251823.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/configuration/org.eclipse.equinox.simpleconfigurator/bundles.info $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/configuration/org.eclipse.equinox.simpleconfigurator/bundles.info
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/configuration/org.eclipse.update/platform.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/configuration/org.eclipse.update/platform.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/configuration/config.ini $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/configuration/config.ini
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/icon.xpm $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/icon.xpm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/about_files/pixman-licenses.txt $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/about_files/pixman-licenses.txt
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/about_files/about_cairo.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/about_files/about_cairo.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/about_files/mpl-v11.txt $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/about_files/mpl-v11.txt
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/1372942375905.profile.gz $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/1372942375905.profile.gz
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/1372942377306.profile.gz $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/1372942377306.profile.gz
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/.lock $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/.lock
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/1372942325041.profile.gz $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/1372942325041.profile.gz
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/.data/org.eclipse.equinox.internal.p2.touchpoint.eclipse.actions/jvmargs $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/.data/org.eclipse.equinox.internal.p2.touchpoint.eclipse.actions/jvmargs
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/.data/.settings/org.eclipse.equinox.p2.artifact.repository.prefs $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/.data/.settings/org.eclipse.equinox.p2.artifact.repository.prefs
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/.data/.settings/org.eclipse.equinox.p2.metadata.repository.prefs $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/.data/.settings/org.eclipse.equinox.p2.metadata.repository.prefs
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/1372942324873.profile.gz $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/1372942324873.profile.gz
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/.settings/org.eclipse.equinox.p2.artifact.repository.prefs $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/.settings/org.eclipse.equinox.p2.artifact.repository.prefs
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/.settings/org.eclipse.equinox.p2.metadata.repository.prefs $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/.settings/org.eclipse.equinox.p2.metadata.repository.prefs
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.core/cache/artifacts.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.core/cache/artifacts.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.core/cache/binary/com.ibm.mq.explorer.ui.rcp.product_root.gtk.linux.x86_64_7.5.0.201307041350 $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.core/cache/binary/com.ibm.mq.explorer.ui.rcp.product_root.gtk.linux.x86_64_7.5.0.201307041350
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.core/cache/binary/com.ibm.mq.explorer.feature_root.gtk.linux.x86_64_7.5.0.201307041350 $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.core/cache/binary/com.ibm.mq.explorer.feature_root.gtk.linux.x86_64_7.5.0.201307041350
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/runwithtrace $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/runwithtrace
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/about.html $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/about.html
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/MQExplorerSDK.zip $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/MQExplorerSDK.zip
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/artifacts.xml $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/artifacts.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/mqexplorer/eclipse/libcairo-swt.so $RPM_BUILD_ROOT/opt/mqm/mqexplorer/eclipse/libcairo-swt.so

%files
%dir %attr(555,mqm,mqm) "/opt/mqm"
%dir %attr(555,mqm,mqm) "/opt/mqm/properties"
%dir %attr(555,mqm,mqm) "/opt/mqm/properties/version"
%dir %attr(555,mqm,mqm) "/opt/mqm/bin"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.eclipse.platform.feature.nl2_7.5.0.201307041350"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.eclipse.platform.feature_7.5.0.201307041350"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.eclipse.platform.feature.nl1_7.5.0.201307041350"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.runtime.thirdparty_7.5.0.201307041350"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.runtime.thirdparty_7.5.0.201307041350/lib"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.runtime.thirdparty_7.5.0.201307041350/META-INF"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/bin"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/etc"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/etc/checkstyle"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/META-INF"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/about_files"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl2_1.1.2.v201102251823"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl2_1.1.2.v201102251823/META-INF"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl1_3.3.0.v201102251823"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl1_3.3.0.v201102251823/META-INF"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl2_3.3.0.v201102251823"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl2_3.3.0.v201102251823/META-INF"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl1_1.1.2.v201102251823"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl1_1.1.2.v201102251823/META-INF"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry_3.3.0.v20100520"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry_3.3.0.v20100520/META-INF"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.weaver_1.6.9.0"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.weaver_1.6.9.0/META-INF"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.runtime_1.6.9.0"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.runtime_1.6.9.0/META-INF"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.runtime_7.5.0.201307041350"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.runtime_7.5.0.201307041350/lib"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.runtime_7.5.0.201307041350/META-INF"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64_1.1.2.R36x_v20101019_1345"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64_1.1.2.R36x_v20101019_1345/META-INF"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/configuration"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/configuration/org.eclipse.equinox.simpleconfigurator"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/configuration/org.eclipse.update"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/dropins"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/about_files"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/links"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/p2"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/.data"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/.data/org.eclipse.equinox.internal.p2.touchpoint.eclipse.actions"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/.data/.settings"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/.settings"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.core"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.core/cache"
%dir %attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.core/cache/binary"
%attr(444,mqm,mqm) "/opt/mqm/properties/version/IBM_WebSphere_MQ_Explorer.7.5.0.cmptag"
%attr(555,mqm,mqm) "/opt/mqm/bin/strmqcfg"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/MQExplorer"
%attr(444,mqm,mqm) "/opt/mqm/bin/MQExplorer.ini"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_ko.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_zh_CN.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_pl.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_hu.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_de.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_lt.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_cs.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_ru.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_it.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_gr.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_es.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_tr.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_ja_JP.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_zh_TW.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_pt_BR.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_fr.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_si.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature.nl1_7.5.0.201307041350/feature_id.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_es.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/license_cs.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/license_it.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_tr.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_zh_CN.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/license_zh_CN.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/license_pt_BR.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/category.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_fr.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/license_fr.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/license_zh_TW.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/license_ja_JP.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_si.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_id.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/license_hu.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_ko.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_pl.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/license_pl.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/license_ru.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_hu.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_de.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/license_es.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/license_de.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_ja_JP.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_lt.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_cs.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/license.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_zh_TW.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_ru.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_it.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_pt_BR.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/license_ko.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature_gr.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.explorer.feature_7.5.0.201307041350/feature.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.eclipse.platform.feature.nl2_7.5.0.201307041350/feature.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.eclipse.platform.feature.nl2_7.5.0.201307041350/feature.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.eclipse.platform.feature_7.5.0.201307041350/feature.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.eclipse.platform.feature_7.5.0.201307041350/feature.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.eclipse.platform.feature.nl1_7.5.0.201307041350/feature.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/features/com.ibm.mq.eclipse.platform.feature.nl1_7.5.0.201307041350/feature.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.nl1_3.6.2.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.coretests.cluster_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.runtime.thirdparty_7.5.0.201307041350/lib/jta.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.runtime.thirdparty_7.5.0.201307041350/lib/connector.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.runtime.thirdparty_7.5.0.201307041350/lib/providerutil.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.runtime.thirdparty_7.5.0.201307041350/lib/fscontext.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.runtime.thirdparty_7.5.0.201307041350/lib/jms.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.runtime.thirdparty_7.5.0.201307041350/META-INF/MANIFEST.MF"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/bin/ant"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/bin/antRun.bat"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/bin/runrc.cmd"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/bin/lcp.bat"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/bin/antRun"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/bin/complete-ant-cmd.pl"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/bin/ant.bat"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/bin/envset.cmd"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/bin/antenv.cmd"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/bin/ant.cmd"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/bin/antRun.pl"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/bin/runant.pl"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/bin/runant.py"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-jdepend.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-apache-regexp.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-nodeps.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-jai.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-stylebook.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-weblogic.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-antlr.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-launcher.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-commons-logging.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-starteam.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-apache-bsf.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-jsch.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-swing.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-jmf.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-commons-net.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-trax.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-apache-bcel.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-apache-oro.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-apache-resolver.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-netrexx.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-apache-log4j.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-javamail.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/lib/ant-junit.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/etc/mmetrics-frames.xsl"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/etc/tagdiff.xsl"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/etc/maudit-frames.xsl"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/etc/log.xsl"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/etc/checkstyle/checkstyle-frames.xsl"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/etc/checkstyle/checkstyle-xdoc.xsl"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/etc/checkstyle/checkstyle-text.xsl"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/etc/changelog.xsl"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/etc/jdepend.xsl"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/etc/junit-frames-xalan1.xsl"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/etc/jdepend-frames.xsl"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/etc/coverage-frames.xsl"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/etc/junit-frames.xsl"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/etc/junit-noframes.xsl"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/META-INF/ECLIPSEF.SF"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/META-INF/eclipse.inf"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/META-INF/ECLIPSEF.RSA"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/META-INF/MANIFEST.MF"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/about_files/NOTICE"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/about_files/asl-v20.txt"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/about_files/LICENSE.sax.txt"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/about_files/LICENSE.dom.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/about.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.ant_1.7.1.v20100518-1145/plugin.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.jmsadmin.tests.nl1_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl2_1.1.2.v201102251823/about.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl2_1.1.2.v201102251823/META-INF/MANIFEST.MF"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl2_1.1.2.v201102251823/launcher.gtk.linux.x86_64_pl.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl2_1.1.2.v201102251823/nl_fragment.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl2_1.1.2.v201102251823/launcher.gtk.linux.x86_64_hu.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl2_1.1.2.v201102251823/launcher.gtk.linux.x86_64_cs.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl2_1.1.2.v201102251823/launcher.gtk.linux.x86_64_ru.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.infopop.nl1_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.core.nl2_2.0.3.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl1_3.3.0.v201102251823/about.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl1_3.3.0.v201102251823/META-INF/MANIFEST.MF"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl1_3.3.0.v201102251823/nl_fragment.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl1_3.3.0.v201102251823/fragment_fr.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl1_3.3.0.v201102251823/fragment_zh.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl1_3.3.0.v201102251823/fragment_ko.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl1_3.3.0.v201102251823/fragment_zh_TW.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl1_3.3.0.v201102251823/fragment_pt_BR.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl1_3.3.0.v201102251823/fragment_de.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl1_3.3.0.v201102251823/fragment_zh_HK.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl1_3.3.0.v201102251823/fragment_it.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl1_3.3.0.v201102251823/fragment_ja.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl1_3.3.0.v201102251823/fragment_es.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer.ssl.nl2_1.0.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.concurrent.nl1_1.0.100.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl2_3.3.0.v201102251823/fragment_ru.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl2_3.3.0.v201102251823/META-INF/MANIFEST.MF"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl2_3.3.0.v201102251823/nl_fragment.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl2_3.3.0.v201102251823/fragment_pl.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl2_3.3.0.v201102251823/fragment_hu.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl2_3.3.0.v201102251823/fragment_cs.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry.nl2_3.3.0.v201102251823/about.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.variables.nl1_3.2.400.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.clusterplugin.nl1_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.mapping.mqjms.nl1_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.artifact.repository.nl1_1.1.1.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.telemetry.ui_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.text.nl1_3.5.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.registry.nl1_3.5.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.jobs.nl1_3.5.1.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.app.nl1_1.3.1.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.common_3.6.0.v20100503.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl1_1.1.2.v201102251823/about.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl1_1.1.2.v201102251823/META-INF/MANIFEST.MF"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl1_1.1.2.v201102251823/launcher.gtk.linux.x86_64_fr.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl1_1.1.2.v201102251823/nl_fragment.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl1_1.1.2.v201102251823/launcher.gtk.linux.x86_64_zh_TW.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl1_1.1.2.v201102251823/launcher.gtk.linux.x86_64_zh.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl1_1.1.2.v201102251823/launcher.gtk.linux.x86_64_ko.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl1_1.1.2.v201102251823/launcher.gtk.linux.x86_64_pt_BR.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl1_1.1.2.v201102251823/launcher.gtk.linux.x86_64_de.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl1_1.1.2.v201102251823/launcher.gtk.linux.x86_64_zh_HK.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl1_1.1.2.v201102251823/launcher.gtk.linux.x86_64_it.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl1_1.1.2.v201102251823/launcher.gtk.linux.x86_64_ja.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64.nl1_1.1.2.v201102251823/launcher.gtk.linux.x86_64_es.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.nl1_3.5.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.swt.nl1_3.6.2.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.compare.core.nl1_3.5.101.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.resources_3.6.1.R36x_v20110131-1630.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.garbagecollector_1.0.100.v20100503.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.preferences.nl1_3.3.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.base.nl1_3.5.3.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.resources.nl2_3.6.1.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.branding_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.debug.core.nl2_3.6.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.telemetry.mqtt.utility_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.observable_1.3.0.I20100601-0800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry_3.3.0.v20100520/about.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry_3.3.0.v20100520/META-INF/ECLIPSEF.SF"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry_3.3.0.v20100520/META-INF/eclipse.inf"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry_3.3.0.v20100520/META-INF/ECLIPSEF.RSA"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry_3.3.0.v20100520/META-INF/MANIFEST.MF"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry_3.3.0.v20100520/runtime_registry_compatibility.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry_3.3.0.v20100520/fragment.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.registry_3.3.0.v20100520/.api_description"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.ui.rcp.nl1_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.ide.application.nl2_1.0.201.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filesystem.linux.x86_64_1.2.0.v20100505-1235.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.telemetry.mqtt.utility.nl1_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.sat4j.core_2.2.0.v20100429.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.variables_3.2.400.v20100505.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.jmsadmin.infopop_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.engine.nl1_2.0.1.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.ds.nl2_1.2.1.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.nl2_1.3.100.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.webapp.nl1_3.5.3.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.mapping.mqjms_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.weaver_1.6.9.0/META-INF/MANIFEST.MF"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.weaver_1.6.9.0/about.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.weaver_1.6.9.0/plugin.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.weaver_1.6.9.0/aspectjweaver.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.update.core.nl1_3.2.402.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.importexport.nl1_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.sdk.scheduler_1.0.0.v20100507-1815.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.runtime_1.6.9.0/META-INF/MANIFEST.MF"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.runtime_1.6.9.0/plugin.properties"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.runtime_1.6.9.0/aspectjrt.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.aspectj.runtime_1.6.9.0/about.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.contenttype.nl1_3.4.100.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.commands.nl2_3.6.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.frameworkadmin.equinox.nl2_1.0.200.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.runtime_7.5.0.201307041350/lib/com.ibm.wmqfte.bootstrap.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.runtime_7.5.0.201307041350/lib/com.ibm.wmqfte.cmdline.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.runtime_7.5.0.201307041350/lib/com.ibm.wmqfte.common.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.runtime_7.5.0.201307041350/lib/com.ibm.wmqfte.native.jni.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.runtime_7.5.0.201307041350/META-INF/MANIFEST.MF"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.metadata.nl1_2.0.1.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.weaving.caching.j9_1.0.0.v20100503.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.directorywatcher.nl1_1.0.203.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface.text.nl1_3.6.1.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.jasper_5.5.17.v201004212143.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.publisher_1.1.2.v20100824-2220.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.extensionlocation.nl2_1.2.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.messageplugin.internal.nl1_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.repository.nl1_2.0.2.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.jsp.jasper.registry_1.0.200.v20100503.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.swt_3.6.2.v3659c.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.servlet.nl2_1.1.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui_3.6.2.M20110203-1100.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.swt.gtk.linux.x86_64.nl2_3.6.2.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.ide.nl2_3.6.2.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.updatechecker.nl2_1.1.101.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.reconciler.dropins.nl2_1.1.2.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.identity.nl1_3.1.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.oam_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.concurrent.nl2_1.0.100.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.variables.nl2_3.2.400.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64_1.1.2.R36x_v20101019_1345/META-INF/ECLIPSEF.SF"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64_1.1.2.R36x_v20101019_1345/META-INF/eclipse.inf"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64_1.1.2.R36x_v20101019_1345/META-INF/ECLIPSEF.RSA"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64_1.1.2.R36x_v20101019_1345/META-INF/MANIFEST.MF"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64_1.1.2.R36x_v20101019_1345/launcher.gtk.linux.x86_64.properties"
%attr(444,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64_1.1.2.R36x_v20101019_1345/eclipse_1310.so"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.gtk.linux.x86_64_1.1.2.R36x_v20101019_1345/about.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.osgi.services.nl1_3.2.100.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.workbench.texteditor.nl1_3.6.1.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface.nl1_3.6.2.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.ui.nl1_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.jmsadmin.sample.menus_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.console.nl1_3.5.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.icu_4.2.1.v20100412.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.weaving.aspectj_1.0.0.v20100503.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer.nl2_3.1.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.compare.core.nl2_3.5.101.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.auth_3.2.200.v20100517.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer_3.1.0.v20100906-1425.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.sdk_1.0.100.v20100513.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filesystem_1.3.1.R36x_v20100727-0745.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.mapping.mqjms.infopop.nl1_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.metadata.generator.nl1_1.0.202.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.jmsadmin_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.nl2_3.6.2.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.simpleconfigurator.manipulator.nl1_2.0.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.swt.gtk.linux.x86_64_3.6.2.v3659b.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility_3.2.100.v20100505.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.sample.simple_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding_1.3.100.I20100601-0800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.artifact.repository.nl2_1.1.1.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.text.nl2_3.5.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.jmsadmin.nl1_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.security.nl1_1.0.200.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.registry.nl2_3.5.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.jobs.nl2_3.5.1.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.weaving.caching_1.0.0.v20100503.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.app.nl2_1.3.1.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.draw2d.nl1_3.6.2.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.lucene_1.9.1.v20100518-1140.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.nl2_3.5.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.swt.nl2_3.6.2.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.sets.infopop.nl1_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.telemetry.ui.nl1_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.util_1.0.200.v20100503.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.touchpoint.eclipse.nl1_2.0.3.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.preferences.nl2_3.3.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.ui.rcp_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.compare.core_3.5.101.R36x_v20100929-0800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.base.nl2_3.5.3.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.console_3.5.0.v20100526.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher_1.1.1.R36x_v20101122_1400.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.update.core.nl2_3.2.402.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.update.configurator_3.3.100.v20100512.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.repository_2.0.2.R36x_v20110111-1500.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.contenttype.nl2_3.4.100.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.topicstatus.infopop.nl1_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filesystem.linux.x86_64.nl1_1.2.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.directorywatcher.nl2_1.0.203.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface_3.6.2.M20110210-1200.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.telemetry.mqtt.utility_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.garbagecollector.nl1_1.0.100.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.topicstatus.nl1_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.doc.nl1_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.engine.nl2_2.0.1.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.pubsub.nl1_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.webapp.nl2_3.5.3.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.ssl.nl1_1.0.100.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.property_1.3.0.I20100601-0800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help_3.5.0.v20100524.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.sdk.nl1_1.0.100.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.traceplugin.internal_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.passwords_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.engine_2.0.1.R36x_v20110201.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.coretests_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.metadata.nl2_2.0.1.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.osgi.services.nl2_3.2.100.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.core_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.navigator_3.5.0.I20100601-0800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface.text.nl2_3.6.1.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.registry.nl1_1.1.1.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.jobs_3.5.1.R36x_v20100824.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.mortbay.jetty.server_6.1.23.v201004211559.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.debug.ui_3.6.3.v20101201_r362.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.coretests.nl1_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.publisher.nl1_1.1.2.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.core_2.0.3.R36x_v20110111.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.trace.weaving_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ant.core.nl1_3.2.201.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ant.core_3.2.201.v20110203_r362.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.repository.nl2_2.0.2.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.expressions.nl1_3.4.200.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.core.nl1_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.metadata.generator.nl2_1.0.202.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.identity.nl2_3.1.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.update.configurator.nl1_3.3.100.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.security.nl2_1.0.200.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.extensionlocation_1.2.0.v20100518.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.util.nl1_1.0.200.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.workbench.texteditor.nl2_3.6.1.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.metadata_2.0.1.R36x_v20101202.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface.nl2_3.6.2.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.update.ui_3.2.300.v20100512.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.console.nl2_3.5.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.artifact.repository_1.1.1.R36x_v20100901.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.frameworkadmin.nl1_2.0.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.net.nl1_1.2.100.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.telemetry.mqtt.utility.nl1_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.text_3.5.0.v20100601-1300.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.jarprocessor.nl1_1.0.200.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.simpleconfigurator.manipulator.nl2_2.0.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.common.nl1_3.6.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.expressions_3.4.200.v20100505.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.security.ui_1.0.200.v20100503.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.net.linux.x86_64.nl1_1.1.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.registry_1.1.1.R36x_v20101103.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.nl1_3.1.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.coretests.cluster.nl1_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.nl1_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.weaving.hook_1.0.0.v20100503.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.garbagecollector.nl2_1.0.100.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.draw2d.nl2_3.6.2.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.net.linux.x86_64_1.1.0.I20100505-1245.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.frameworkadmin.equinox_1.0.200.v20100505.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.workbench_3.6.2.M20110210-1200.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filesystem.nl1_1.3.1.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.osgi.nl1_3.6.2.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.oam.infopop_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui_2.0.0.v20100518.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.touchpoint.eclipse.nl2_2.0.3.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.ssl.nl2_1.0.100.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.observable.nl1_1.3.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.nl1_1.1.1.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer.ssl_1.0.0.v20100906-1425.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.osgi_3.6.2.R36x_v20110210.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.director_2.0.3.R36x_v20101117-1018.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.topicstatus_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.sdk.nl2_1.0.100.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.ui_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.property.nl1_1.3.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.explorer_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.servlet_1.1.0.v20100503.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.metadata.repository.nl1_1.1.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filesystem.linux.x86_64.nl2_1.2.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.ams_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.branding.nl1_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.jsp.jasper.registry.nl1_1.0.200.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.navigator.nl1_3.5.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.jetty_2.0.0.v20100503.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filebuffers_3.5.100.v20100520-0800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.jmsadmin.tests_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.wmqfte.explorer.context_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.security_1.0.200.v20100503.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ant.core.nl2_3.2.201.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.commands_3.6.0.I20100512-1500.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.expressions.nl2_3.4.200.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.nl1_3.4.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.lucene.analysis_1.9.1.v20100518-1140.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface.databinding_1.4.0.I20100601-0800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.nl1_2.0.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.frameworkadmin_2.0.0.v20100503.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.app_1.3.1.R36x_v20100803.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.operations_2.0.0.v20100510.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.views_3.5.1.M20110202-0800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface.databinding.nl1_1.4.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.commonservices_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.sets.infopop_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.update.configurator.nl2_3.3.100.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.workbench.texteditor_3.6.1.r361_v20100714-0800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.sdk.scheduler.nl1_1.0.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.jsp.jasper_1.0.201.R36x_v20101103.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.commons.el_1.0.0.v201004212143.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.ds_1.2.1.R36x_v20100803.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.filetransfer_4.0.0.v20100906-1425.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.util.nl2_1.0.200.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.registry.nl2_1.1.1.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.pcf.event_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.publisher.nl2_1.1.2.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.draw2d_3.6.2.v20110128-0100.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.osgi.services_3.2.100.v20100503.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.filetransfer.nl1_4.0.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.servicedef.infopop.nl1_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.net.nl2_1.2.100.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.ui.infopop_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.oam.tests_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.nl1_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.debug.ui.nl1_3.6.3.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.ui.nl1_3.5.3.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.views.nl1_3.5.1.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.forms.nl1_3.5.2.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.jarprocessor.nl2_1.0.200.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.simpleconfigurator.nl1_1.0.200.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.pubsub.infopop_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.workbench.nl1_3.6.2.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filebuffers.nl1_3.5.100.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.jetty.nl1_2.0.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.operations.nl1_2.0.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.mapping.mqjms.infopop_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.plugins_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.jsp.jasper.nl1_1.0.201.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.contenttype_3.4.100.v20100505-1235.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.frameworkadmin.nl2_2.0.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.update.ui.nl1_3.2.300.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.nl1_3.6.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.ssl_1.0.100.v20100906-1425.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.director.nl1_2.0.3.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.editors.nl1_3.6.1.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.apache.commons.logging_1.0.4.v201005080501.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.icu.nl1_4.2.1.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.debug.core_3.6.0.v20100519.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.auth.nl1_3.2.200.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.jarprocessor_1.0.200.v20100503a.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.jmsadmin.infopop.nl1_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.core.nl1_2.0.3.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime_3.6.0.v20100505.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.ide_3.6.2.M20101201-0800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.ui_3.5.3.r36_20101116.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.common.nl2_3.6.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer.ssl.nl1_1.0.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf_3.1.0.v20100906-1425.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.net.linux.x86_64.nl2_1.1.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.simpleconfigurator_1.0.200.v20100503.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.webapp_3.5.4.r36_20120404.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.reconciler.dropins_1.1.2.R36x_v20110114.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.nl2_3.1.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.jsp.jasper.registry.nl2_1.0.200.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.sample_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.sets_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/javax.servlet_2.5.0.v200910301333.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.servicedef_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filesystem.nl2_1.3.1.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.nl1_3.2.100.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.osgi.nl2_3.6.2.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/javax.servlet.jsp_2.0.0.v200806031607.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.directorywatcher_1.0.203.R36x_v20101220.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.observable.nl2_1.3.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.launcher.nl2_1.1.1.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.micro.client.mqttv3.wrapper_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.resources.nl1_3.6.1.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.sets.nl1_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.debug.core.nl1_3.6.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.compare_3.5.101.R36x_v20100929-0800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.property.nl2_1.3.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.oam.tests.nl1_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface.text_3.6.1.r361_v20100825-0800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.net_1.2.100.I20100511-0800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.metadata.repository.nl2_1.1.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.pubsub_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.pubsub.infopop.nl1_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.touchpoint.eclipse_2.0.3.R36x_v20101202.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.ide.application_1.0.201.M20100707-0800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.importexport_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.navigator.nl2_3.5.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.ds.nl1_1.2.1.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.sample.menus_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.tests.infopop_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.runtime_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.ams.nl1_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro.nl2_3.4.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.nl2_2.0.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.simpleconfigurator.nl2_1.0.200.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.mortbay.jetty.util_6.1.23.v201004211559.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.base_3.5.3.v201102101200.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.jface.databinding.nl2_1.4.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.metadata.repository_1.1.0.v20100513.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.messageplugin.internal_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.intro_3.4.0.v20100427.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.commands.nl1_3.6.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.filebuffers.nl2_3.5.100.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.ui.sdk.scheduler.nl2_1.0.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.simpleconfigurator.manipulator_2.0.0.v20100503.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.ide.application.nl1_1.0.201.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.servicedef.infopop_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.update.core_3.2.402.R36x_v20100629.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.updatechecker_1.1.101.R36x_v20100823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.extensionlocation.nl1_1.2.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.jsp.jasper.nl2_1.0.201.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.traceplugin.internal.nl1_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.metadata.generator_1.0.202.R36x_v20101208-1400.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.editors_3.6.1.r361_v20100825-0800.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.registry_3.5.0.v20100503.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.databinding.nl1_1.3.100.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.filetransfer.nl2_4.0.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.update.ui.nl2_3.2.300.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.qmgradmin.topicstatus.infopop_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.servlet.nl1_1.1.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.swt.gtk.linux.x86_64.nl1_3.6.2.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.ide.nl1_3.6.2.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.debug.ui.nl2_3.6.3.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.help.ui.nl2_3.5.3.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.views.nl2_3.5.1.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.reconciler.dropins.nl1_1.1.2.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.forms.nl2_3.5.2.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.auth.nl2_3.2.200.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.servicedef.nl1_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.doc_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.preferences_3.3.0.v20100503.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.oam.infopop.nl1_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.ui.infopop.nl1_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.workbench.nl2_3.6.2.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.http.jetty.nl2_2.0.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.operations.nl2_2.0.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.oam.nl1_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.frameworkadmin.equinox.nl1_1.0.200.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.concurrent_1.0.100.v20100503.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.identity_3.1.0.v20100906-1425.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.forms_3.5.2.r36_v20100702.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ecf.provider.filetransfer.nl1_3.1.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.compatibility.nl2_3.2.100.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.core.runtime.nl2_3.6.0.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.mq.explorer.clusterplugin_7.5.0.201307041350.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.ui.editors.nl2_3.6.1.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.director.nl2_2.0.3.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.sat4j.pb_2.2.0.v20100429.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/org.eclipse.equinox.p2.updatechecker.nl1_1.1.101.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/plugins/com.ibm.icu.nl2_4.2.1.v201102251823.jar"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/configuration/org.eclipse.equinox.simpleconfigurator/bundles.info"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/configuration/org.eclipse.update/platform.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/configuration/config.ini"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/icon.xpm"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/about_files/pixman-licenses.txt"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/about_files/about_cairo.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/about_files/mpl-v11.txt"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/1372942375905.profile.gz"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/1372942377306.profile.gz"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/.lock"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/1372942325041.profile.gz"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/.data/org.eclipse.equinox.internal.p2.touchpoint.eclipse.actions/jvmargs"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/.data/.settings/org.eclipse.equinox.p2.artifact.repository.prefs"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/.data/.settings/org.eclipse.equinox.p2.metadata.repository.prefs"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/profileRegistry/profile.profile/1372942324873.profile.gz"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/.settings/org.eclipse.equinox.p2.artifact.repository.prefs"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.engine/.settings/org.eclipse.equinox.p2.metadata.repository.prefs"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.core/cache/artifacts.xml"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.core/cache/binary/com.ibm.mq.explorer.ui.rcp.product_root.gtk.linux.x86_64_7.5.0.201307041350"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/p2/org.eclipse.equinox.p2.core/cache/binary/com.ibm.mq.explorer.feature_root.gtk.linux.x86_64_7.5.0.201307041350"
%attr(555,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/runwithtrace"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/about.html"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/MQExplorerSDK.zip"
%attr(444,mqm,mqm) "/opt/mqm/mqexplorer/eclipse/artifacts.xml"
%attr(444,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/mqexplorer/eclipse/libcairo-swt.so"

%pre
RPM_PACKAGE_SUMMARY="WebSphere MQ Explorer"
RPM_PACKAGE_NAME="MQSeriesExplorer"
RPM_PACKAGE_VERSION="7.5.0"
RPM_PACKAGE_RELEASE="2"
PACKAGEPLATFORMS="x86_64"
MQ_DEFAULT_INSTALLATION_PATH=/opt/mqm
MQ_INSTALLATION_PATH=${RPM_INSTALL_PREFIX}
MQ_RUNSCRIPT=
echo ${SHELLOPTS} | grep xtrace > /dev/null
if [ $? -eq 0 ] ; then 
  env | sort
  uname -a
  id
  MQ_RUNSCRIPT="sh -x"
fi

# @(#) MQMBID sn=p750-002-130704 su=_UmspIOSZEeK1oKoKL_dPJA pn=install/unix/linux_2/preinstall.sh
#   <copyright 
#   notice="lm-source-program" 
#   pids="5724-H72," 
#   years="2005,2012" 
#   crc="2186337890" > 
#  Licensed Materials - Property of IBM  
#   
#  5724-H72, 
#   
#  (C) Copyright IBM Corp. 2005, 2012 All Rights Reserved.  
#   
#  US Government Users Restricted Rights - Use, duplication or  
#  disclosure restricted by GSA ADP Schedule Contract with  
#  IBM Corp.  
#   </copyright> 
# Preinstallation script
# Checks to see if the mqm userid and groups exists, and if now will create
# 
# Check that this package is not being installed to a location where 
# a different VR exists
# 

# Set up environment variables
TMPDIR="/tmp"
RC=0
DUMMY=""

create_mqm_group()
{
  echo "Creating group mqm"
  ErrorText=`groupadd mqm 2>&1`
  RC=$?
  if [ $RC -ne 0 ]; then
    echo "ERROR:  Failed to add 'mqm' group:" >&2
    echo $ErrorText >&2
    exit $RC
  fi
}
# Determine if the group exists on the system.  The solution used here is to
# create a file in the temporary location, attempt to change its group id to
# mqm.  This method should work independent of the multitude of ways groups can
# be defined across distributions and network environments
TMPFILE="${TMPDIR}/amq_grouptest"
touch $TMPFILE 2>/dev/null
if [ -f "$TMPFILE" ]; then
  chown :mqm $TMPFILE 2>/dev/null
  if [ $? -ne 0 ]; then
    create_mqm_group
  fi
  rm -f $TMPFILE
else  # Unable to create temporary file.  This is also needed by other install
      # processes - so it is fair to abort the install at this point
  echo "ERROR: Unable to write to ${TMPDIR} - aborting install"
  exit 1
fi

# Determine if the user id "mqm" exists, and is in the group mqm
ErrorText=`id -u mqm 2>&1`
RC=$?
if [ $RC -ne 0 ]; then
  echo "Creating user mqm"
  ErrorText=`useradd -r -d /var/mqm -g mqm mqm 2>&1`
  RC=$?
  if [ $RC -ne 0 ]; then
    echo "ERROR:  Failed to add 'mqm' user:" >&2
    echo $ErrorText >&2
    exit $RC
  fi
else    # Check the user mqm is in the group mqm
  ErrorText=`id -Gn mqm | grep -w mqm 2>&1`
  if [ $? -ne 0 ]; then
    ErrorText=`usermod -G mqm mqm 2>&1`
    if [ $RC -ne 0 ]; then
      echo "ERROR:  Failed to add user mqm to group mqm:" >&2
      echo $ErrorText >&1
      echo $RC
    fi
  fi
fi

#######################################################################################################
# Check the install path does not exceed the Websphere MQ maximum length of 256
#######################################################################################################
if [ ${#MQ_INSTALLATION_PATH} -gt 256 ]; then
  echo ""
  echo "ERROR:   Specified path exceeds WebSphere MQ maximum length of 256"
  echo ""
  exit 1
fi

#######################################################################################################
# Check the install path does not contain unsupported charaters
#######################################################################################################
echo ${MQ_INSTALLATION_PATH} | grep "[:%#]" > /dev/null
if [ $? -eq 0 ] ; then
  echo ""
  echo "ERROR:   Specified path contains an unsupported character"
  echo ""
  exit 1
fi

#######################################################################################################
# Runtime checks
#######################################################################################################
echo ${RPM_PACKAGE_NAME} | grep  "MQSeriesRuntime" > /dev/null
if [ $? -eq 0 ] ; then
  #####################################################################################################
  # Check that the install path is empty
  #####################################################################################################
  if [ -d ${MQ_INSTALLATION_PATH} ] ; then
    LS_ALL=`ls -A ${MQ_INSTALLATION_PATH}`
    if [ "${LS_ALL}" ] && [ "${LS_ALL}" != "lost+found" ] ; then
      echo ""
      echo "ERROR:   Specified path is not empty"
      echo ""
      exit 1
    fi
  fi
#######################################################################################################
# Non Runtime checks
#######################################################################################################
else
  #####################################################################################################
  # Check the version/release of the product already at MQ_INSTALLATION_PATH is the same as this one
  #####################################################################################################
  if [ -x ${MQ_INSTALLATION_PATH}/bin/dspmqver ] ; then
    INSTALLED_VR=$(${MQ_INSTALLATION_PATH}/bin/dspmqver -f2 -b | awk -F. '{print $1 "." $2}')
    PACKAGE_VR=`echo ${RPM_PACKAGE_VERSION} | awk -F. '{print $1 "." $2}'`
    if [ ${INSTALLED_VR} != ${PACKAGE_VR} ] ; then
      echo ""
      echo "ERROR:   This package is not applicable to the WebSphere MQ installation at ${MQ_INSTALLATION_PATH}"
      echo ""
      exit 1
    fi
  else
    echo ""
    echo "ERROR:   There is no MQSeriesRuntime installed at ${MQ_INSTALLATION_PATH}"
    echo ""
    exit 1
  fi
fi

#   <copyright 
#   notice="lm-source-program" 
#   pids="5724-H72," 
#   years="2005,2012" 
#   crc="1114153681" > 
#  Licensed Materials - Property of IBM  
#   
#  5724-H72, 
#   
#  (C) Copyright IBM Corp. 2005, 2012 All Rights Reserved.  
#   
#  US Government Users Restricted Rights - Use, duplication or  
#  disclosure restricted by GSA ADP Schedule Contract with  
#  IBM Corp.  
#   </copyright> 
# Preinstallation script
# Check's to see if the license agreement has been accepted

if [ ! -r /tmp/mq_license_${RPM_PACKAGE_VERSION}/license/status.dat ] && [ ! -r ${MQ_INSTALLATION_PATH}/licenses/status.dat ] ; then

    cat << +++EOM+++
ERROR:  Product cannot be installed until the license
        agreement has been accepted.
        Run the 'mqlicense' script, which is in the root
        directory of the install media, or see the
        installation instructions in the product 
        Infocenter for more information
+++EOM+++

   exit 1
fi

echo > /dev/null 2>/dev/null

%post
RPM_PACKAGE_SUMMARY="WebSphere MQ Explorer"
RPM_PACKAGE_NAME="MQSeriesExplorer"
RPM_PACKAGE_VERSION="7.5.0"
RPM_PACKAGE_RELEASE="2"
PACKAGEPLATFORMS="x86_64"
MQ_DEFAULT_INSTALLATION_PATH=/opt/mqm
MQ_INSTALLATION_PATH=${RPM_INSTALL_PREFIX}
MQ_RUNSCRIPT=
echo ${SHELLOPTS} | grep xtrace > /dev/null
if [ $? -eq 0 ] ; then 
  env | sort
  uname -a
  id
  MQ_RUNSCRIPT="sh -x"
fi
if [ ${MQ_INSTALLATION_PATH} !=  ${MQ_DEFAULT_INSTALLATION_PATH} ] ; then 
  if [ -x ${MQ_INSTALLATION_PATH}/bin/amqicrel ] ; then 
     ${MQ_RUNSCRIPT} ${MQ_INSTALLATION_PATH}/bin/amqicrel ${MQ_INSTALLATION_PATH} ${RPM_PACKAGE_NAME}-${RPM_PACKAGE_VERSION}-${RPM_PACKAGE_RELEASE}
  fi
fi

# @(#) MQMBID sn=p750-002-130704 su=_UmspIOSZEeK1oKoKL_dPJA pn=install/unix/linux_2/mqexplorer_postinstall.sh
#   <copyright 
#   notice="lm-source-program" 
#   pids="5724-H72," 
#   years="2011,2012" 
#   crc="1016225224" > 
#  Licensed Materials - Property of IBM  
#   
#  5724-H72, 
#   
#  (C) Copyright IBM Corp. 2011, 2012 All Rights Reserved.  
#   
#  US Government Users Restricted Rights - Use, duplication or  
#  disclosure restricted by GSA ADP Schedule Contract with  
#  IBM Corp.  
#   </copyright> 

EXPLORER_DIR=${MQ_INSTALLATION_PATH}/mqexplorer/eclipse

# Update ini file to point to 'bin' directory of installed JRE
INI_FILE=${MQ_INSTALLATION_PATH}/bin/MQExplorer.ini
if [ -f "${INI_FILE}" ]
then
  # need to use 64bit JRE on 64bit platforms
  JRE_DIR=${MQ_INSTALLATION_PATH}/java/jre64/jre/bin
  if [ ! -d "${JRE_DIR}" ]
  then
    JRE_DIR=${MQ_INSTALLATION_PATH}/java/jre/jre/bin
  fi
  # replace -vm path within ini file
  chmod 644 "${INI_FILE}"
  sed -i -e "s#../java/jre/bin#${JRE_DIR}#" "${INI_FILE}"
  chmod 444 "${INI_FILE}"
fi

# Query the name of this installation
INSTALLATION_NAME=default
OUTPUT=`"${MQ_INSTALLATION_PATH}/bin/dspmqver" -b -f 512`
if [ $? -eq 0 ]
then
  # ensure name contains only letters and digits
  INVALID_CHARS=`echo -n "${OUTPUT}" | tr -d "[:alnum:]"`
  if [ -z "${INVALID_CHARS}" ]
  then
    INSTALLATION_NAME=${OUTPUT}
  fi
fi

# Update config to qualify workspace directory with installation name
CONFIG_FILE=${EXPLORER_DIR}/configuration/config.ini
if [ -f "${CONFIG_FILE}" ]
then
  # Add "-" and installation name to config file to change the workspace location
  chmod 644 "${CONFIG_FILE}"
  sed -i -e "s#IBM/WebSphereMQ/workspace#IBM/WebSphereMQ/workspace-${INSTALLATION_NAME}#" "${CONFIG_FILE}"
  # Also set eclipse.home.location appropriately
  sed -i -e "s#@MQ.INSTALLATION.PATH#${MQ_INSTALLATION_PATH}#" "${CONFIG_FILE}"
  chmod 444 "${CONFIG_FILE}"
fi

# Contribute to system menus
DESKTOP_DIR=/usr/share/applications
if [ -d "${DESKTOP_DIR}" ]
then
  # Write out .desktop file qualified by installation name
  DESKTOP_FILE=${DESKTOP_DIR}/IBM_WebSphere_MQ_Explorer-${INSTALLATION_NAME}.desktop
  echo "[Desktop Entry]" > "${DESKTOP_FILE}"
  echo "Version=1.0" >> "${DESKTOP_FILE}"
  echo "Type=Application" >> "${DESKTOP_FILE}"
  echo "Encoding=UTF-8" >> "${DESKTOP_FILE}"
  echo "Exec=${MQ_INSTALLATION_PATH}/bin/MQExplorer" >> "${DESKTOP_FILE}"
  echo "Icon=${EXPLORER_DIR}/icon.xpm" >> "${DESKTOP_FILE}"
  echo "Name=IBM WebSphere MQ Explorer (${INSTALLATION_NAME})" >> "${DESKTOP_FILE}"
  echo "Comment=IBM WebSphere MQ Explorer (${INSTALLATION_NAME})" >> "${DESKTOP_FILE}"
  echo "Categories=Development" >> "${DESKTOP_FILE}"
  chmod 644 "${DESKTOP_FILE}"
fi

# Create configuration in install directory using MQExplorer -initialize

if [ -x ${MQ_INSTALLATION_PATH}/bin/MQExplorer ] ; then 
# Ensure no windows are opened
  unset DISPLAY 
  ${MQ_INSTALLATION_PATH}/bin/MQExplorer -initialize -nosplash -data /tmp/$$workspace
  rm -rf /tmp/$$workspace
  if [ -d ${EXPLORER_DIR}/configuration ] ; then 
    chown -R mqm:mqm ${EXPLORER_DIR}/configuration
    chmod -R 444 ${EXPLORER_DIR}/configuration
    find ${EXPLORER_DIR}/configuration -type d -print0 | xargs -0 chmod 555
  fi
  if [ -d ${EXPLORER_DIR}/p2 ] ; then 
    chown -R mqm:mqm ${EXPLORER_DIR}/p2
    chmod -R 444 ${EXPLORER_DIR}/p2
    find ${EXPLORER_DIR}/p2 -type d -print0 | xargs -0 chmod 555
  fi
fi

# Invoke setmqinst to refresh symlinks if this installation is the primary
setmqinst_out=`LD_LIBRARY_PATH="" ${MQ_INSTALLATION_PATH}/bin/setmqinst -r -p ${MQ_INSTALLATION_PATH} 2>&1`
rc=$?
if [ $rc -ne 0 ] ; then
  echo "ERROR: Return code \"$rc\" from setmqinst for \"-r -p ${MQ_INSTALLATION_PATH}\", output is:" >&2
  echo "       ${setmqinst_out}" >&2
fi
echo > /dev/null


%preun
RPM_PACKAGE_SUMMARY="WebSphere MQ Explorer"
RPM_PACKAGE_NAME="MQSeriesExplorer"
RPM_PACKAGE_VERSION="7.5.0"
RPM_PACKAGE_RELEASE="2"
PACKAGEPLATFORMS="x86_64"
MQ_DEFAULT_INSTALLATION_PATH=/opt/mqm
MQ_INSTALLATION_PATH=${RPM_INSTALL_PREFIX}
MQ_RUNSCRIPT=
echo ${SHELLOPTS} | grep xtrace > /dev/null
if [ $? -eq 0 ] ; then 
  env | sort
  uname -a
  id
  MQ_RUNSCRIPT="sh -x"
fi

#   <copyright 
#   notice="lm-source-program" 
#   pids="5724-H72," 
#   years="2005,2012" 
#   crc="122768040" > 
#  Licensed Materials - Property of IBM  
#   
#  5724-H72, 
#   
#  (C) Copyright IBM Corp. 2005, 2012 All Rights Reserved.  
#   
#  US Government Users Restricted Rights - Use, duplication or  
#  disclosure restricted by GSA ADP Schedule Contract with  
#  IBM Corp.  
#   </copyright> 
#
# Pre-uninstallation script
# Checks for already running Q Managers, and if it finds one, stops the
# uninstall.

# If amqiclen exists (should do during uninstall) then run it to clean up
# IPCC resources. If amqiclen returns an error then a queue manager is still
# running so stop the uninstall.
if [ -x ${MQ_INSTALLATION_PATH}/bin/amqiclen ] && [ -f /var/mqm/mqs.ini ]
then
    ${MQ_INSTALLATION_PATH}/bin/amqiclen -x -v > /tmp/amqiclen.$$.out 2>&1
    amqiclen_rc=$?
    if [ $amqiclen_rc -ne 0 ]
    then
      echo "ERROR:    WebSphere MQ shared resources for this installation are still in use." >&2
      echo "          It is likely that one or more queue managers are still running." >&2
      echo "          Return code from from amqiclen was \"$amqiclen_rc\", output was:" >&2
      cat /tmp/amqiclen.$$.out >&2
      rm /tmp/amqiclen.$$.out
      exit 1
    fi
    rm /tmp/amqiclen.$$.out
fi

# @(#) MQMBID sn=p750-002-130704 su=_UmspIOSZEeK1oKoKL_dPJA pn=install/unix/linux_2/mqexplorer_preuninstall.sh
#   <copyright 
#   notice="lm-source-program" 
#   pids="5724-H72," 
#   years="2011,2012" 
#   crc="2397397538" > 
#  Licensed Materials - Property of IBM  
#   
#  5724-H72, 
#   
#  (C) Copyright IBM Corp. 2011, 2012 All Rights Reserved.  
#   
#  US Government Users Restricted Rights - Use, duplication or  
#  disclosure restricted by GSA ADP Schedule Contract with  
#  IBM Corp.  
#   </copyright> 

EXPLORER_DIR=${MQ_INSTALLATION_PATH}/mqexplorer/eclipse

# Remove any extra configuration data which may have been created
if [ -d ${EXPLORER_DIR}/configuration ] ; then 
  rm -Rf ${EXPLORER_DIR}/configuration
fi
if [ -d ${EXPLORER_DIR}/p2 ] ; then 
  rm -Rf ${EXPLORER_DIR}/p2
fi
#   <copyright 
#   notice="lm-source-program" 
#   pids="5724-H72," 
#   years="2005,2012" 
#   crc="1595222582" > 
#  Licensed Materials - Property of IBM  
#   
#  5724-H72, 
#   
#  (C) Copyright IBM Corp. 2005, 2012 All Rights Reserved.  
#   
#  US Government Users Restricted Rights - Use, duplication or  
#  disclosure restricted by GSA ADP Schedule Contract with  
#  IBM Corp.  
#   </copyright> 
#
# Pre-uninstallation check script for all components
# A check is performed to see if there are any fixpack filesets applied to
# the base component which is currently being uninstalled.  If the fixpack
# has been applied, the uninstallation of this component is aborted to prevent
# the situation where the base fileset has been uninstalled leaving an
# uninstallable fixpack.

FIXPACK_BACKUPDIR="${MQ_INSTALLATION_PATH}/maintenance"

find $FIXPACK_BACKUPDIR -type d -maxdepth 2 -print 2>/dev/null | \
      while read directory ; do
  component=`basename $directory`
  if [ "$RPM_PACKAGE_NAME" = "$component" ]; then
    # safety check - are there actually files under this directory?
    num_files=`find "$directory" -type f -print 2>/dev/null | wc -l`
    if [ $num_files -gt 0 ]; then
      cat << EOF

  ERROR:  There appears to be a fixpack installed on this machine for this
          component.

  Please ensure you have removed all fixpacks for the \"$RPM_PACKAGE_NAME\"
  component before trying to remove this package.

EOF
      exit 1
    fi
  fi
done

# Removing product links

%postun
RPM_PACKAGE_SUMMARY="WebSphere MQ Explorer"
RPM_PACKAGE_NAME="MQSeriesExplorer"
RPM_PACKAGE_VERSION="7.5.0"
RPM_PACKAGE_RELEASE="2"
PACKAGEPLATFORMS="x86_64"
MQ_DEFAULT_INSTALLATION_PATH=/opt/mqm
MQ_INSTALLATION_PATH=${RPM_INSTALL_PREFIX}
MQ_RUNSCRIPT=
echo ${SHELLOPTS} | grep xtrace > /dev/null
if [ $? -eq 0 ] ; then 
  env | sort
  uname -a
  id
  MQ_RUNSCRIPT="sh -x"
fi

# @(#) MQMBID sn=p750-002-130704 su=_UmspIOSZEeK1oKoKL_dPJA pn=install/unix/linux_2/mqexplorer_postuninstall.sh
#   <copyright 
#   notice="lm-source-program" 
#   pids="5724-H72," 
#   years="2011,2012" 
#   crc="931196921" > 
#  Licensed Materials - Property of IBM  
#   
#  5724-H72, 
#   
#  (C) Copyright IBM Corp. 2011, 2012 All Rights Reserved.  
#   
#  US Government Users Restricted Rights - Use, duplication or  
#  disclosure restricted by GSA ADP Schedule Contract with  
#  IBM Corp.  
#   </copyright> 

# Remove any system menus entries referring to this installation
DESKTOP_DIR=/usr/share/applications
if [ -d "${DESKTOP_DIR}" ]
then
  for file in `ls ${DESKTOP_DIR}/IBM_WebSphere_MQ_Explorer-*.desktop`
  do
    grep "Exec=${MQ_INSTALLATION_PATH}/bin/MQExplorer" "${file}" >/dev/null
    if [ $? -eq 0 ]
    then
      # a line matched so remove the file
      rm -f "${file}"
    fi
  done
fi

