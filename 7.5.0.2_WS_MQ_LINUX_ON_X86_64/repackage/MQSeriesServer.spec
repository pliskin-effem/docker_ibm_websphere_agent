Summary: WebSphere MQ Server FileSet
Name: MQSeriesServer
Version: 7.5.0
Release: 2
License: Commercial
ExclusiveArch: x86_64
Group: Applications/Networking
AutoReqProv: no
Vendor: IBM
Prefix: /opt/mqm
Requires: MQSeriesRuntime = 7.5.0-2
%global __strip /bin/true
%global _rpmdir /build/slot1/p750_P/inst.images/amd64_linux_2/images/
%global _tmppath /build/slot1/p750_P/obj/amd64_linux_2/install/unix/linux_2
BuildRoot: /build/slot1/p750_P/obj/amd64_linux_2/ship

%description
IBM WebSphere MQ for Developers for Linux for x86_64
5724-H72
%clean
rm -rf $RPM_BUILD_ROOT

%install
install -d $RPM_BUILD_ROOT/opt/mqm
install -d $RPM_BUILD_ROOT/opt/mqm/properties
install -d $RPM_BUILD_ROOT/opt/mqm/properties/version
install -d $RPM_BUILD_ROOT/opt/mqm/lib
install -d $RPM_BUILD_ROOT/opt/mqm/lib/compat
install -d $RPM_BUILD_ROOT/opt/mqm/lib64
install -d $RPM_BUILD_ROOT/opt/mqm/lib64/compat
install -d $RPM_BUILD_ROOT/opt/mqm/bin
install -d $RPM_BUILD_ROOT/opt/mqm/inc
install -d $RPM_BUILD_ROOT/opt/mqm/lib/4.1
install -d $RPM_BUILD_ROOT/opt/mqm/lib64/4.1
install -d $RPM_BUILD_ROOT/opt/mqm/java
install -d $RPM_BUILD_ROOT/opt/mqm/java/lib
install -d $RPM_BUILD_ROOT/opt/mqm/java/lib64
install -d $RPM_BUILD_ROOT/opt/mqm/java/lib/jdbc
install -d $RPM_BUILD_ROOT/opt/mqm/java/lib64/jdbc
install -d $RPM_BUILD_ROOT/opt/mqm/java/bin
install -d $RPM_BUILD_ROOT/opt/mqm/doc
install -d $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz
install -d $RPM_BUILD_ROOT/opt/mqm/doc/de_de
install -d $RPM_BUILD_ROOT/opt/mqm/doc/en_us
install -d $RPM_BUILD_ROOT/opt/mqm/doc/es_es
install -d $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr
install -d $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu
install -d $RPM_BUILD_ROOT/opt/mqm/doc/it_it
install -d $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp
install -d $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr
install -d $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl
install -d $RPM_BUILD_ROOT/opt/mqm/doc/pt_br
install -d $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru
install -d $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn
install -d $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/properties/version/IBM_WebSphere_MQ_Server.7.5.0.swtag $RPM_BUILD_ROOT/opt/mqm/properties/version/IBM_WebSphere_MQ_Server.7.5.0.swtag
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib64/libmqmf_r.so $RPM_BUILD_ROOT/opt/mqm/lib64/libmqmf_r.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/runmqbrk $RPM_BUILD_ROOT/opt/mqm/bin/runmqbrk
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/amqfcxba $RPM_BUILD_ROOT/opt/mqm/bin/amqfcxba
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/dltmqbrk $RPM_BUILD_ROOT/opt/mqm/bin/dltmqbrk
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/endmqbrk $RPM_BUILD_ROOT/opt/mqm/bin/endmqbrk
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/amqfqpub $RPM_BUILD_ROOT/opt/mqm/bin/amqfqpub
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/amqcrsta $RPM_BUILD_ROOT/opt/mqm/bin/amqcrsta
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/amqpcsea $RPM_BUILD_ROOT/opt/mqm/bin/amqpcsea
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib64/amqzif_r $RPM_BUILD_ROOT/opt/mqm/lib64/amqzif_r
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib64/amqzfu $RPM_BUILD_ROOT/opt/mqm/lib64/amqzfu
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib64/amqzfu1 $RPM_BUILD_ROOT/opt/mqm/lib64/amqzfu1
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib64/libmqmz1_r.so $RPM_BUILD_ROOT/opt/mqm/lib64/libmqmz1_r.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/amqzfuma $RPM_BUILD_ROOT/opt/mqm/bin/amqzfuma
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/amqoamd $RPM_BUILD_ROOT/opt/mqm/bin/amqoamd
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib64/libmqmz0_r.so $RPM_BUILD_ROOT/opt/mqm/lib64/libmqmz0_r.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/amqzlaa0 $RPM_BUILD_ROOT/opt/mqm/bin/amqzlaa0
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/amqzlsa0 $RPM_BUILD_ROOT/opt/mqm/bin/amqzlsa0
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/amqzslf0 $RPM_BUILD_ROOT/opt/mqm/bin/amqzslf0
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/amqzxma0 $RPM_BUILD_ROOT/opt/mqm/bin/amqzxma0
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/amqzmgr0 $RPM_BUILD_ROOT/opt/mqm/bin/amqzmgr0
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/amqzmuc0 $RPM_BUILD_ROOT/opt/mqm/bin/amqzmuc0
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/amqzmuf0 $RPM_BUILD_ROOT/opt/mqm/bin/amqzmuf0
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/amqzmur0 $RPM_BUILD_ROOT/opt/mqm/bin/amqzmur0
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/crtmqcvx $RPM_BUILD_ROOT/opt/mqm/bin/crtmqcvx
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/crtmqm $RPM_BUILD_ROOT/opt/mqm/bin/crtmqm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/dltmqm $RPM_BUILD_ROOT/opt/mqm/bin/dltmqm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/dmpmqaut $RPM_BUILD_ROOT/opt/mqm/bin/dmpmqaut
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/dspmqcsv $RPM_BUILD_ROOT/opt/mqm/bin/dspmqcsv
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/dspmqfls $RPM_BUILD_ROOT/opt/mqm/bin/dspmqfls
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/dspmqaut $RPM_BUILD_ROOT/opt/mqm/bin/dspmqaut
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/dspmqtrn $RPM_BUILD_ROOT/opt/mqm/bin/dspmqtrn
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/dspmq $RPM_BUILD_ROOT/opt/mqm/bin/dspmq
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/dspmqrte $RPM_BUILD_ROOT/opt/mqm/bin/dspmqrte
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/setmqaut $RPM_BUILD_ROOT/opt/mqm/bin/setmqaut
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/endmqcsv $RPM_BUILD_ROOT/opt/mqm/bin/endmqcsv
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/endmqm $RPM_BUILD_ROOT/opt/mqm/bin/endmqm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/rcdmqimg $RPM_BUILD_ROOT/opt/mqm/bin/rcdmqimg
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/rcrmqobj $RPM_BUILD_ROOT/opt/mqm/bin/rcrmqobj
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/runmqchi $RPM_BUILD_ROOT/opt/mqm/bin/runmqchi
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/runmqlsr $RPM_BUILD_ROOT/opt/mqm/bin/runmqlsr
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/runswchl $RPM_BUILD_ROOT/opt/mqm/bin/runswchl
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/amqrcmla $RPM_BUILD_ROOT/opt/mqm/bin/amqrcmla
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/amqrrmfa $RPM_BUILD_ROOT/opt/mqm/bin/amqrrmfa
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/amqrmppa $RPM_BUILD_ROOT/opt/mqm/bin/amqrmppa
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/amqzlwa0 $RPM_BUILD_ROOT/opt/mqm/bin/amqzlwa0
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/endmqlsr $RPM_BUILD_ROOT/opt/mqm/bin/endmqlsr
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/runmqchl $RPM_BUILD_ROOT/opt/mqm/bin/runmqchl
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/runmqsc $RPM_BUILD_ROOT/opt/mqm/bin/runmqsc
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/runmqtrm $RPM_BUILD_ROOT/opt/mqm/bin/runmqtrm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/rsvmqtrn $RPM_BUILD_ROOT/opt/mqm/bin/rsvmqtrn
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/addmqinf $RPM_BUILD_ROOT/opt/mqm/bin/addmqinf
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/dspmqinf $RPM_BUILD_ROOT/opt/mqm/bin/dspmqinf
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/rmvmqinf $RPM_BUILD_ROOT/opt/mqm/bin/rmvmqinf
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/amqsstop $RPM_BUILD_ROOT/opt/mqm/bin/amqsstop
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/setmqm $RPM_BUILD_ROOT/opt/mqm/bin/setmqm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/setmqprd $RPM_BUILD_ROOT/opt/mqm/bin/setmqprd
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/dspmqspl $RPM_BUILD_ROOT/opt/mqm/bin/dspmqspl
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/setmqspl $RPM_BUILD_ROOT/opt/mqm/bin/setmqspl
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/strmqm $RPM_BUILD_ROOT/opt/mqm/bin/strmqm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/strmqcsv $RPM_BUILD_ROOT/opt/mqm/bin/strmqcsv
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib/libmqml_r.so $RPM_BUILD_ROOT/opt/mqm/lib/libmqml_r.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib64/libmqml_r.so $RPM_BUILD_ROOT/opt/mqm/lib64/libmqml_r.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib64/libmqmalda_r.so $RPM_BUILD_ROOT/opt/mqm/lib64/libmqmalda_r.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib64/libmqmaldb_r.so $RPM_BUILD_ROOT/opt/mqm/lib64/libmqmaldb_r.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib/libmqmzf.so $RPM_BUILD_ROOT/opt/mqm/lib/libmqmzf.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib64/libmqmzf.so $RPM_BUILD_ROOT/opt/mqm/lib64/libmqmzf.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib/libmqmzf_r.so $RPM_BUILD_ROOT/opt/mqm/lib/libmqmzf_r.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib64/libmqmzf_r.so $RPM_BUILD_ROOT/opt/mqm/lib64/libmqmzf_r.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib/libmqutl.so $RPM_BUILD_ROOT/opt/mqm/lib/libmqutl.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib64/libmqutl.so $RPM_BUILD_ROOT/opt/mqm/lib64/libmqutl.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib/libmqutl_r.so $RPM_BUILD_ROOT/opt/mqm/lib/libmqutl_r.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib64/libmqutl_r.so $RPM_BUILD_ROOT/opt/mqm/lib64/libmqutl_r.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib/libmqzi.so $RPM_BUILD_ROOT/opt/mqm/lib/libmqzi.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib64/libmqzi.so $RPM_BUILD_ROOT/opt/mqm/lib64/libmqzi.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib/libmqzi_r.so $RPM_BUILD_ROOT/opt/mqm/lib/libmqzi_r.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib64/libmqzi_r.so $RPM_BUILD_ROOT/opt/mqm/lib64/libmqzi_r.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib/libmqds.so $RPM_BUILD_ROOT/opt/mqm/lib/libmqds.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib64/libmqds.so $RPM_BUILD_ROOT/opt/mqm/lib64/libmqds.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib/libmqds_r.so $RPM_BUILD_ROOT/opt/mqm/lib/libmqds_r.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib64/libmqds_r.so $RPM_BUILD_ROOT/opt/mqm/lib64/libmqds_r.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib/libmqmr.so $RPM_BUILD_ROOT/opt/mqm/lib/libmqmr.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib64/libmqmr.so $RPM_BUILD_ROOT/opt/mqm/lib64/libmqmr.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib/libmqmr_r.so $RPM_BUILD_ROOT/opt/mqm/lib/libmqmr_r.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib64/libmqmr_r.so $RPM_BUILD_ROOT/opt/mqm/lib64/libmqmr_r.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib/libmqmcb.so $RPM_BUILD_ROOT/opt/mqm/lib/libmqmcb.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib64/libmqmcb.so $RPM_BUILD_ROOT/opt/mqm/lib64/libmqmcb.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib/libmqmcb_r.so $RPM_BUILD_ROOT/opt/mqm/lib/libmqmcb_r.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib64/libmqmcb_r.so $RPM_BUILD_ROOT/opt/mqm/lib64/libmqmcb_r.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib/libmqmxa.so $RPM_BUILD_ROOT/opt/mqm/lib/libmqmxa.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib64/libmqmxa.so $RPM_BUILD_ROOT/opt/mqm/lib64/libmqmxa.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib64/libmqmxa64.so $RPM_BUILD_ROOT/opt/mqm/lib64/libmqmxa64.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib/libmqmxa_r.so $RPM_BUILD_ROOT/opt/mqm/lib/libmqmxa_r.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib64/libmqmxa_r.so $RPM_BUILD_ROOT/opt/mqm/lib64/libmqmxa_r.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib64/libmqmxa64_r.so $RPM_BUILD_ROOT/opt/mqm/lib64/libmqmxa64_r.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/amqzdmaa $RPM_BUILD_ROOT/opt/mqm/bin/amqzdmaa
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib/libmqmax.so $RPM_BUILD_ROOT/opt/mqm/lib/libmqmax.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib64/libmqmax.so $RPM_BUILD_ROOT/opt/mqm/lib64/libmqmax.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib/libmqmax_r.so $RPM_BUILD_ROOT/opt/mqm/lib/libmqmax_r.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib64/libmqmax_r.so $RPM_BUILD_ROOT/opt/mqm/lib64/libmqmax_r.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib64/libmqmax64.so $RPM_BUILD_ROOT/opt/mqm/lib64/libmqmax64.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib64/libmqmax64_r.so $RPM_BUILD_ROOT/opt/mqm/lib64/libmqmax64_r.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib/amqzaax $RPM_BUILD_ROOT/opt/mqm/lib/amqzaax
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib64/amqzaax $RPM_BUILD_ROOT/opt/mqm/lib64/amqzaax
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib/amqzaax_r $RPM_BUILD_ROOT/opt/mqm/lib/amqzaax_r
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib64/amqzaax_r $RPM_BUILD_ROOT/opt/mqm/lib64/amqzaax_r
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib64/amq64ax $RPM_BUILD_ROOT/opt/mqm/lib64/amq64ax
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib64/amq64ax_r $RPM_BUILD_ROOT/opt/mqm/lib64/amq64ax_r
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/runmqdlq $RPM_BUILD_ROOT/opt/mqm/bin/runmqdlq
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/dmpmqlog $RPM_BUILD_ROOT/opt/mqm/bin/dmpmqlog
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib/4.1/libimqs23gl.so $RPM_BUILD_ROOT/opt/mqm/lib/4.1/libimqs23gl.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib/4.1/libimqs23gl_r.so $RPM_BUILD_ROOT/opt/mqm/lib/4.1/libimqs23gl_r.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib64/4.1/libimqs23gl.so $RPM_BUILD_ROOT/opt/mqm/lib64/4.1/libimqs23gl.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib64/4.1/libimqs23gl_r.so $RPM_BUILD_ROOT/opt/mqm/lib64/4.1/libimqs23gl_r.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib/amqpcert.lic $RPM_BUILD_ROOT/opt/mqm/lib/amqpcert.lic
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/ffstsummary $RPM_BUILD_ROOT/opt/mqm/bin/ffstsummary
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/amqldmpa $RPM_BUILD_ROOT/opt/mqm/bin/amqldmpa
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/amqspdbg $RPM_BUILD_ROOT/opt/mqm/bin/amqspdbg
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/amqrfdm $RPM_BUILD_ROOT/opt/mqm/bin/amqrfdm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/amqrdbgm $RPM_BUILD_ROOT/opt/mqm/bin/amqrdbgm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/amqorxr $RPM_BUILD_ROOT/opt/mqm/bin/amqorxr
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib64/amqwasoa $RPM_BUILD_ROOT/opt/mqm/lib64/amqwasoa
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib64/amqwascx $RPM_BUILD_ROOT/opt/mqm/lib64/amqwascx
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib/libmqjxs_r.so $RPM_BUILD_ROOT/opt/mqm/lib/libmqjxs_r.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib64/libmqjxs_r.so $RPM_BUILD_ROOT/opt/mqm/lib64/libmqjxs_r.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib/libmqjxds_r.so $RPM_BUILD_ROOT/opt/mqm/lib/libmqjxds_r.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib64/libmqjxds_r.so $RPM_BUILD_ROOT/opt/mqm/lib64/libmqjxds_r.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib64/amqzxmr0 $RPM_BUILD_ROOT/opt/mqm/lib64/amqzxmr0
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/java/lib/libmqjbnd.so $RPM_BUILD_ROOT/opt/mqm/java/lib/libmqjbnd.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/java/lib64/libmqjbnd.so $RPM_BUILD_ROOT/opt/mqm/java/lib64/libmqjbnd.so
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/java/lib/jdbc/jdbcdb2.o $RPM_BUILD_ROOT/opt/mqm/java/lib/jdbc/jdbcdb2.o
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/java/lib/jdbc/jdbcora.o $RPM_BUILD_ROOT/opt/mqm/java/lib/jdbc/jdbcora.o
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/java/lib/jdbc/Makefile $RPM_BUILD_ROOT/opt/mqm/java/lib/jdbc/Makefile
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/java/lib64/jdbc/jdbcdb2.o $RPM_BUILD_ROOT/opt/mqm/java/lib64/jdbc/jdbcdb2.o
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/java/lib64/jdbc/jdbcora.o $RPM_BUILD_ROOT/opt/mqm/java/lib64/jdbc/jdbcora.o
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/java/lib64/jdbc/Makefile $RPM_BUILD_ROOT/opt/mqm/java/lib64/jdbc/Makefile
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/java/lib/com.ibm.mq.postcard.jar $RPM_BUILD_ROOT/opt/mqm/java/lib/com.ibm.mq.postcard.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/java/bin/postcard $RPM_BUILD_ROOT/opt/mqm/java/bin/postcard
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/amqpcard $RPM_BUILD_ROOT/opt/mqm/bin/amqpcard
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/runpcard $RPM_BUILD_ROOT/opt/mqm/bin/runpcard
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/java/lib/com.ibm.mq.defaultconfig.jar $RPM_BUILD_ROOT/opt/mqm/java/lib/com.ibm.mq.defaultconfig.jar
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/java/bin/DefaultConfiguration $RPM_BUILD_ROOT/opt/mqm/java/bin/DefaultConfiguration
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/rundefconf $RPM_BUILD_ROOT/opt/mqm/bin/rundefconf
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/amqmgse $RPM_BUILD_ROOT/opt/mqm/bin/amqmgse
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/amqmbbrk $RPM_BUILD_ROOT/opt/mqm/bin/amqmbbrk
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/migmbbrk $RPM_BUILD_ROOT/opt/mqm/bin/migmbbrk
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib64/libicudata.so.32.0 $RPM_BUILD_ROOT/opt/mqm/lib64/libicudata.so.32.0
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib64/libicuuc.so.32.0 $RPM_BUILD_ROOT/opt/mqm/lib64/libicuuc.so.32.0
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib64/libicui18n.so.32.0 $RPM_BUILD_ROOT/opt/mqm/lib64/libicui18n.so.32.0
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib64/libxml4c.so.56.3 $RPM_BUILD_ROOT/opt/mqm/lib64/libxml4c.so.56.3
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib64/libxml4c-depdom.so.56.3 $RPM_BUILD_ROOT/opt/mqm/lib64/libxml4c-depdom.so.56.3
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/lib64/libXML4CMessages.so.56.3 $RPM_BUILD_ROOT/opt/mqm/lib64/libXML4CMessages.so.56.3
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/bin/amqmfsck $RPM_BUILD_ROOT/opt/mqm/bin/amqmfsck
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/ax99995_.htm $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/ax99995_.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/banner.css $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/banner.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/bearings3.gif $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/bearings3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/bip4.css $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/bip4.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/blank.gif $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/blank.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/concept_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/concept_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/copyright.gif $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/copyright.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/corp_timestamp.gif $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/corp_timestamp.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/delta.gif $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/delta.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/deltaend.gif $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/deltaend.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/diagnostic3.gif $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/diagnostic3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/docscd.gif $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/docscd.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/external_link.gif $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/external_link.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/feedback_r.htm $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/feedback_r.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/g_postcard.htm $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/g_postcard.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/g_postcard_different.htm $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/g_postcard_different.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/g_postcard_oneqm.htm $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/g_postcard_oneqm.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/g_postcard_signon.htm $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/g_postcard_signon.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/g_postcard_twoqm.htm $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/g_postcard_twoqm.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/g_postcard_workings.htm $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/g_postcard_workings.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/general_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/general_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/helpbanner2.gif $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/helpbanner2.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/i_choices.htm $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/i_choices.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/i_comp_wiz.htm $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/i_comp_wiz.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/i_conf_win2000.htm $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/i_conf_win2000.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/i_config.htm $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/i_config.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/i_def_conf.htm $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/i_def_conf.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/i_def_conf_wiz.htm $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/i_def_conf_wiz.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/i_folders.htm $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/i_folders.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/i_join_def_clus.htm $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/i_join_def_clus.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/i_launch.htm $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/i_launch.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/i_listener.htm $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/i_listener.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/i_local_repos.htm $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/i_local_repos.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/i_net_prereq.htm $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/i_net_prereq.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/i_prepare_wiz.htm $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/i_prepare_wiz.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/i_qmgr_conf.htm $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/i_qmgr_conf.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/i_rem_repos.htm $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/i_rem_repos.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/i_remote_admin.htm $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/i_remote_admin.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/i_setup_type.htm $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/i_setup_type.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/i_soft_prereq.htm $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/i_soft_prereq.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/i_uninst.htm $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/i_uninst.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/i_wsmq.htm $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/i_wsmq.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/ibm-logo-white.gif $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/ibm-logo-white.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/ibmdita.css $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/ibmdita.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/ng2000.gif $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/ng2000.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/ngaix.gif $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/ngaix.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/nghpux.gif $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/nghpux.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/nglinux.gif $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/nglinux.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/ngnt.gif $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/ngnt.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/ngsolaris.gif $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/ngsolaris.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/ngunix.gif $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/ngunix.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/ngwin.gif $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/ngwin.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/ngxp.gif $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/ngxp.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/ngzos.gif $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/ngzos.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/problem3.gif $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/problem3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/query3.gif $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/query3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/reference_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/reference_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/sample_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/sample_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/start_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/start_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/swg_info_common.css $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/swg_info_common.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/task_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/task_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/timestamp.gif $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/timestamp.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/tip3.gif $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/tip3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/tutorial_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/tutorial_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/warning3.gif $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/warning3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/websphere-brandmark.gif $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/websphere-brandmark.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/wizard_help.xml $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/wizard_help.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/cs_cz/workbench_link.gif $RPM_BUILD_ROOT/opt/mqm/doc/cs_cz/workbench_link.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/ax99995_.htm $RPM_BUILD_ROOT/opt/mqm/doc/de_de/ax99995_.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/banner.css $RPM_BUILD_ROOT/opt/mqm/doc/de_de/banner.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/bearings3.gif $RPM_BUILD_ROOT/opt/mqm/doc/de_de/bearings3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/bip4.css $RPM_BUILD_ROOT/opt/mqm/doc/de_de/bip4.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/blank.gif $RPM_BUILD_ROOT/opt/mqm/doc/de_de/blank.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/concept_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/de_de/concept_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/copyright.gif $RPM_BUILD_ROOT/opt/mqm/doc/de_de/copyright.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/corp_timestamp.gif $RPM_BUILD_ROOT/opt/mqm/doc/de_de/corp_timestamp.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/delta.gif $RPM_BUILD_ROOT/opt/mqm/doc/de_de/delta.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/deltaend.gif $RPM_BUILD_ROOT/opt/mqm/doc/de_de/deltaend.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/diagnostic3.gif $RPM_BUILD_ROOT/opt/mqm/doc/de_de/diagnostic3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/docscd.gif $RPM_BUILD_ROOT/opt/mqm/doc/de_de/docscd.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/external_link.gif $RPM_BUILD_ROOT/opt/mqm/doc/de_de/external_link.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/feedback_r.htm $RPM_BUILD_ROOT/opt/mqm/doc/de_de/feedback_r.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/g_postcard.htm $RPM_BUILD_ROOT/opt/mqm/doc/de_de/g_postcard.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/g_postcard_different.htm $RPM_BUILD_ROOT/opt/mqm/doc/de_de/g_postcard_different.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/g_postcard_oneqm.htm $RPM_BUILD_ROOT/opt/mqm/doc/de_de/g_postcard_oneqm.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/g_postcard_signon.htm $RPM_BUILD_ROOT/opt/mqm/doc/de_de/g_postcard_signon.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/g_postcard_twoqm.htm $RPM_BUILD_ROOT/opt/mqm/doc/de_de/g_postcard_twoqm.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/g_postcard_workings.htm $RPM_BUILD_ROOT/opt/mqm/doc/de_de/g_postcard_workings.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/general_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/de_de/general_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/helpbanner2.gif $RPM_BUILD_ROOT/opt/mqm/doc/de_de/helpbanner2.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/i_choices.htm $RPM_BUILD_ROOT/opt/mqm/doc/de_de/i_choices.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/i_comp_wiz.htm $RPM_BUILD_ROOT/opt/mqm/doc/de_de/i_comp_wiz.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/i_conf_win2000.htm $RPM_BUILD_ROOT/opt/mqm/doc/de_de/i_conf_win2000.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/i_config.htm $RPM_BUILD_ROOT/opt/mqm/doc/de_de/i_config.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/i_def_conf.htm $RPM_BUILD_ROOT/opt/mqm/doc/de_de/i_def_conf.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/i_def_conf_wiz.htm $RPM_BUILD_ROOT/opt/mqm/doc/de_de/i_def_conf_wiz.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/i_folders.htm $RPM_BUILD_ROOT/opt/mqm/doc/de_de/i_folders.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/i_join_def_clus.htm $RPM_BUILD_ROOT/opt/mqm/doc/de_de/i_join_def_clus.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/i_launch.htm $RPM_BUILD_ROOT/opt/mqm/doc/de_de/i_launch.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/i_listener.htm $RPM_BUILD_ROOT/opt/mqm/doc/de_de/i_listener.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/i_local_repos.htm $RPM_BUILD_ROOT/opt/mqm/doc/de_de/i_local_repos.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/i_net_prereq.htm $RPM_BUILD_ROOT/opt/mqm/doc/de_de/i_net_prereq.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/i_prepare_wiz.htm $RPM_BUILD_ROOT/opt/mqm/doc/de_de/i_prepare_wiz.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/i_qmgr_conf.htm $RPM_BUILD_ROOT/opt/mqm/doc/de_de/i_qmgr_conf.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/i_rem_repos.htm $RPM_BUILD_ROOT/opt/mqm/doc/de_de/i_rem_repos.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/i_remote_admin.htm $RPM_BUILD_ROOT/opt/mqm/doc/de_de/i_remote_admin.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/i_setup_type.htm $RPM_BUILD_ROOT/opt/mqm/doc/de_de/i_setup_type.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/i_soft_prereq.htm $RPM_BUILD_ROOT/opt/mqm/doc/de_de/i_soft_prereq.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/i_uninst.htm $RPM_BUILD_ROOT/opt/mqm/doc/de_de/i_uninst.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/i_wsmq.htm $RPM_BUILD_ROOT/opt/mqm/doc/de_de/i_wsmq.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/ibm-logo-white.gif $RPM_BUILD_ROOT/opt/mqm/doc/de_de/ibm-logo-white.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/ibmdita.css $RPM_BUILD_ROOT/opt/mqm/doc/de_de/ibmdita.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/ng2000.gif $RPM_BUILD_ROOT/opt/mqm/doc/de_de/ng2000.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/ngaix.gif $RPM_BUILD_ROOT/opt/mqm/doc/de_de/ngaix.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/nghpux.gif $RPM_BUILD_ROOT/opt/mqm/doc/de_de/nghpux.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/nglinux.gif $RPM_BUILD_ROOT/opt/mqm/doc/de_de/nglinux.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/ngnt.gif $RPM_BUILD_ROOT/opt/mqm/doc/de_de/ngnt.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/ngsolaris.gif $RPM_BUILD_ROOT/opt/mqm/doc/de_de/ngsolaris.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/ngunix.gif $RPM_BUILD_ROOT/opt/mqm/doc/de_de/ngunix.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/ngwin.gif $RPM_BUILD_ROOT/opt/mqm/doc/de_de/ngwin.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/ngxp.gif $RPM_BUILD_ROOT/opt/mqm/doc/de_de/ngxp.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/ngzos.gif $RPM_BUILD_ROOT/opt/mqm/doc/de_de/ngzos.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/problem3.gif $RPM_BUILD_ROOT/opt/mqm/doc/de_de/problem3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/query3.gif $RPM_BUILD_ROOT/opt/mqm/doc/de_de/query3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/reference_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/de_de/reference_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/sample_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/de_de/sample_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/start_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/de_de/start_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/swg_info_common.css $RPM_BUILD_ROOT/opt/mqm/doc/de_de/swg_info_common.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/task_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/de_de/task_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/timestamp.gif $RPM_BUILD_ROOT/opt/mqm/doc/de_de/timestamp.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/tip3.gif $RPM_BUILD_ROOT/opt/mqm/doc/de_de/tip3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/tutorial_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/de_de/tutorial_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/warning3.gif $RPM_BUILD_ROOT/opt/mqm/doc/de_de/warning3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/websphere-brandmark.gif $RPM_BUILD_ROOT/opt/mqm/doc/de_de/websphere-brandmark.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/wizard_help.xml $RPM_BUILD_ROOT/opt/mqm/doc/de_de/wizard_help.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/de_de/workbench_link.gif $RPM_BUILD_ROOT/opt/mqm/doc/de_de/workbench_link.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/ax99995_.htm $RPM_BUILD_ROOT/opt/mqm/doc/en_us/ax99995_.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/banner.css $RPM_BUILD_ROOT/opt/mqm/doc/en_us/banner.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/bearings3.gif $RPM_BUILD_ROOT/opt/mqm/doc/en_us/bearings3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/bip4.css $RPM_BUILD_ROOT/opt/mqm/doc/en_us/bip4.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/blank.gif $RPM_BUILD_ROOT/opt/mqm/doc/en_us/blank.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/concept_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/en_us/concept_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/copyright.gif $RPM_BUILD_ROOT/opt/mqm/doc/en_us/copyright.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/corp_timestamp.gif $RPM_BUILD_ROOT/opt/mqm/doc/en_us/corp_timestamp.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/delta.gif $RPM_BUILD_ROOT/opt/mqm/doc/en_us/delta.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/deltaend.gif $RPM_BUILD_ROOT/opt/mqm/doc/en_us/deltaend.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/diagnostic3.gif $RPM_BUILD_ROOT/opt/mqm/doc/en_us/diagnostic3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/docscd.gif $RPM_BUILD_ROOT/opt/mqm/doc/en_us/docscd.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/external_link.gif $RPM_BUILD_ROOT/opt/mqm/doc/en_us/external_link.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/feedback_r.htm $RPM_BUILD_ROOT/opt/mqm/doc/en_us/feedback_r.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/g_postcard.htm $RPM_BUILD_ROOT/opt/mqm/doc/en_us/g_postcard.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/g_postcard_different.htm $RPM_BUILD_ROOT/opt/mqm/doc/en_us/g_postcard_different.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/g_postcard_oneqm.htm $RPM_BUILD_ROOT/opt/mqm/doc/en_us/g_postcard_oneqm.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/g_postcard_signon.htm $RPM_BUILD_ROOT/opt/mqm/doc/en_us/g_postcard_signon.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/g_postcard_twoqm.htm $RPM_BUILD_ROOT/opt/mqm/doc/en_us/g_postcard_twoqm.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/g_postcard_workings.htm $RPM_BUILD_ROOT/opt/mqm/doc/en_us/g_postcard_workings.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/general_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/en_us/general_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/helpbanner2.gif $RPM_BUILD_ROOT/opt/mqm/doc/en_us/helpbanner2.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/i_choices.htm $RPM_BUILD_ROOT/opt/mqm/doc/en_us/i_choices.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/i_comp_wiz.htm $RPM_BUILD_ROOT/opt/mqm/doc/en_us/i_comp_wiz.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/i_conf_win2000.htm $RPM_BUILD_ROOT/opt/mqm/doc/en_us/i_conf_win2000.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/i_config.htm $RPM_BUILD_ROOT/opt/mqm/doc/en_us/i_config.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/i_def_conf.htm $RPM_BUILD_ROOT/opt/mqm/doc/en_us/i_def_conf.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/i_def_conf_wiz.htm $RPM_BUILD_ROOT/opt/mqm/doc/en_us/i_def_conf_wiz.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/i_folders.htm $RPM_BUILD_ROOT/opt/mqm/doc/en_us/i_folders.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/i_join_def_clus.htm $RPM_BUILD_ROOT/opt/mqm/doc/en_us/i_join_def_clus.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/i_launch.htm $RPM_BUILD_ROOT/opt/mqm/doc/en_us/i_launch.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/i_listener.htm $RPM_BUILD_ROOT/opt/mqm/doc/en_us/i_listener.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/i_local_repos.htm $RPM_BUILD_ROOT/opt/mqm/doc/en_us/i_local_repos.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/i_net_prereq.htm $RPM_BUILD_ROOT/opt/mqm/doc/en_us/i_net_prereq.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/i_prepare_wiz.htm $RPM_BUILD_ROOT/opt/mqm/doc/en_us/i_prepare_wiz.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/i_qmgr_conf.htm $RPM_BUILD_ROOT/opt/mqm/doc/en_us/i_qmgr_conf.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/i_rem_repos.htm $RPM_BUILD_ROOT/opt/mqm/doc/en_us/i_rem_repos.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/i_remote_admin.htm $RPM_BUILD_ROOT/opt/mqm/doc/en_us/i_remote_admin.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/i_setup_type.htm $RPM_BUILD_ROOT/opt/mqm/doc/en_us/i_setup_type.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/i_soft_prereq.htm $RPM_BUILD_ROOT/opt/mqm/doc/en_us/i_soft_prereq.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/i_uninst.htm $RPM_BUILD_ROOT/opt/mqm/doc/en_us/i_uninst.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/i_wsmq.htm $RPM_BUILD_ROOT/opt/mqm/doc/en_us/i_wsmq.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/ibm-logo-white.gif $RPM_BUILD_ROOT/opt/mqm/doc/en_us/ibm-logo-white.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/ibmdita.css $RPM_BUILD_ROOT/opt/mqm/doc/en_us/ibmdita.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/ng2000.gif $RPM_BUILD_ROOT/opt/mqm/doc/en_us/ng2000.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/ngaix.gif $RPM_BUILD_ROOT/opt/mqm/doc/en_us/ngaix.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/nghpux.gif $RPM_BUILD_ROOT/opt/mqm/doc/en_us/nghpux.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/nglinux.gif $RPM_BUILD_ROOT/opt/mqm/doc/en_us/nglinux.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/ngnt.gif $RPM_BUILD_ROOT/opt/mqm/doc/en_us/ngnt.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/ngsolaris.gif $RPM_BUILD_ROOT/opt/mqm/doc/en_us/ngsolaris.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/ngunix.gif $RPM_BUILD_ROOT/opt/mqm/doc/en_us/ngunix.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/ngwin.gif $RPM_BUILD_ROOT/opt/mqm/doc/en_us/ngwin.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/ngxp.gif $RPM_BUILD_ROOT/opt/mqm/doc/en_us/ngxp.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/ngzos.gif $RPM_BUILD_ROOT/opt/mqm/doc/en_us/ngzos.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/plugin.properties $RPM_BUILD_ROOT/opt/mqm/doc/en_us/plugin.properties
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/plugin.xml $RPM_BUILD_ROOT/opt/mqm/doc/en_us/plugin.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/problem3.gif $RPM_BUILD_ROOT/opt/mqm/doc/en_us/problem3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/query3.gif $RPM_BUILD_ROOT/opt/mqm/doc/en_us/query3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/reference_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/en_us/reference_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/sample_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/en_us/sample_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/start_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/en_us/start_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/swg_info_common.css $RPM_BUILD_ROOT/opt/mqm/doc/en_us/swg_info_common.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/task_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/en_us/task_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/timestamp.gif $RPM_BUILD_ROOT/opt/mqm/doc/en_us/timestamp.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/tip3.gif $RPM_BUILD_ROOT/opt/mqm/doc/en_us/tip3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/tutorial_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/en_us/tutorial_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/warning3.gif $RPM_BUILD_ROOT/opt/mqm/doc/en_us/warning3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/websphere-brandmark.gif $RPM_BUILD_ROOT/opt/mqm/doc/en_us/websphere-brandmark.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/wizard_help.xml $RPM_BUILD_ROOT/opt/mqm/doc/en_us/wizard_help.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/en_us/workbench_link.gif $RPM_BUILD_ROOT/opt/mqm/doc/en_us/workbench_link.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/ax99995_.htm $RPM_BUILD_ROOT/opt/mqm/doc/es_es/ax99995_.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/banner.css $RPM_BUILD_ROOT/opt/mqm/doc/es_es/banner.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/bearings3.gif $RPM_BUILD_ROOT/opt/mqm/doc/es_es/bearings3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/bip4.css $RPM_BUILD_ROOT/opt/mqm/doc/es_es/bip4.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/blank.gif $RPM_BUILD_ROOT/opt/mqm/doc/es_es/blank.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/concept_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/es_es/concept_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/copyright.gif $RPM_BUILD_ROOT/opt/mqm/doc/es_es/copyright.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/corp_timestamp.gif $RPM_BUILD_ROOT/opt/mqm/doc/es_es/corp_timestamp.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/delta.gif $RPM_BUILD_ROOT/opt/mqm/doc/es_es/delta.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/deltaend.gif $RPM_BUILD_ROOT/opt/mqm/doc/es_es/deltaend.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/diagnostic3.gif $RPM_BUILD_ROOT/opt/mqm/doc/es_es/diagnostic3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/docscd.gif $RPM_BUILD_ROOT/opt/mqm/doc/es_es/docscd.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/external_link.gif $RPM_BUILD_ROOT/opt/mqm/doc/es_es/external_link.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/feedback_r.htm $RPM_BUILD_ROOT/opt/mqm/doc/es_es/feedback_r.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/g_postcard.htm $RPM_BUILD_ROOT/opt/mqm/doc/es_es/g_postcard.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/g_postcard_different.htm $RPM_BUILD_ROOT/opt/mqm/doc/es_es/g_postcard_different.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/g_postcard_oneqm.htm $RPM_BUILD_ROOT/opt/mqm/doc/es_es/g_postcard_oneqm.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/g_postcard_signon.htm $RPM_BUILD_ROOT/opt/mqm/doc/es_es/g_postcard_signon.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/g_postcard_twoqm.htm $RPM_BUILD_ROOT/opt/mqm/doc/es_es/g_postcard_twoqm.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/g_postcard_workings.htm $RPM_BUILD_ROOT/opt/mqm/doc/es_es/g_postcard_workings.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/general_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/es_es/general_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/helpbanner2.gif $RPM_BUILD_ROOT/opt/mqm/doc/es_es/helpbanner2.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/i_choices.htm $RPM_BUILD_ROOT/opt/mqm/doc/es_es/i_choices.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/i_comp_wiz.htm $RPM_BUILD_ROOT/opt/mqm/doc/es_es/i_comp_wiz.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/i_conf_win2000.htm $RPM_BUILD_ROOT/opt/mqm/doc/es_es/i_conf_win2000.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/i_config.htm $RPM_BUILD_ROOT/opt/mqm/doc/es_es/i_config.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/i_def_conf.htm $RPM_BUILD_ROOT/opt/mqm/doc/es_es/i_def_conf.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/i_def_conf_wiz.htm $RPM_BUILD_ROOT/opt/mqm/doc/es_es/i_def_conf_wiz.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/i_folders.htm $RPM_BUILD_ROOT/opt/mqm/doc/es_es/i_folders.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/i_join_def_clus.htm $RPM_BUILD_ROOT/opt/mqm/doc/es_es/i_join_def_clus.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/i_launch.htm $RPM_BUILD_ROOT/opt/mqm/doc/es_es/i_launch.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/i_listener.htm $RPM_BUILD_ROOT/opt/mqm/doc/es_es/i_listener.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/i_local_repos.htm $RPM_BUILD_ROOT/opt/mqm/doc/es_es/i_local_repos.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/i_net_prereq.htm $RPM_BUILD_ROOT/opt/mqm/doc/es_es/i_net_prereq.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/i_prepare_wiz.htm $RPM_BUILD_ROOT/opt/mqm/doc/es_es/i_prepare_wiz.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/i_qmgr_conf.htm $RPM_BUILD_ROOT/opt/mqm/doc/es_es/i_qmgr_conf.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/i_rem_repos.htm $RPM_BUILD_ROOT/opt/mqm/doc/es_es/i_rem_repos.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/i_remote_admin.htm $RPM_BUILD_ROOT/opt/mqm/doc/es_es/i_remote_admin.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/i_setup_type.htm $RPM_BUILD_ROOT/opt/mqm/doc/es_es/i_setup_type.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/i_soft_prereq.htm $RPM_BUILD_ROOT/opt/mqm/doc/es_es/i_soft_prereq.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/i_uninst.htm $RPM_BUILD_ROOT/opt/mqm/doc/es_es/i_uninst.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/i_wsmq.htm $RPM_BUILD_ROOT/opt/mqm/doc/es_es/i_wsmq.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/ibm-logo-white.gif $RPM_BUILD_ROOT/opt/mqm/doc/es_es/ibm-logo-white.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/ibmdita.css $RPM_BUILD_ROOT/opt/mqm/doc/es_es/ibmdita.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/ng2000.gif $RPM_BUILD_ROOT/opt/mqm/doc/es_es/ng2000.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/ngaix.gif $RPM_BUILD_ROOT/opt/mqm/doc/es_es/ngaix.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/nghpux.gif $RPM_BUILD_ROOT/opt/mqm/doc/es_es/nghpux.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/nglinux.gif $RPM_BUILD_ROOT/opt/mqm/doc/es_es/nglinux.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/ngnt.gif $RPM_BUILD_ROOT/opt/mqm/doc/es_es/ngnt.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/ngsolaris.gif $RPM_BUILD_ROOT/opt/mqm/doc/es_es/ngsolaris.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/ngunix.gif $RPM_BUILD_ROOT/opt/mqm/doc/es_es/ngunix.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/ngwin.gif $RPM_BUILD_ROOT/opt/mqm/doc/es_es/ngwin.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/ngxp.gif $RPM_BUILD_ROOT/opt/mqm/doc/es_es/ngxp.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/ngzos.gif $RPM_BUILD_ROOT/opt/mqm/doc/es_es/ngzos.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/problem3.gif $RPM_BUILD_ROOT/opt/mqm/doc/es_es/problem3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/query3.gif $RPM_BUILD_ROOT/opt/mqm/doc/es_es/query3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/reference_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/es_es/reference_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/sample_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/es_es/sample_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/start_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/es_es/start_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/swg_info_common.css $RPM_BUILD_ROOT/opt/mqm/doc/es_es/swg_info_common.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/task_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/es_es/task_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/timestamp.gif $RPM_BUILD_ROOT/opt/mqm/doc/es_es/timestamp.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/tip3.gif $RPM_BUILD_ROOT/opt/mqm/doc/es_es/tip3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/tutorial_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/es_es/tutorial_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/warning3.gif $RPM_BUILD_ROOT/opt/mqm/doc/es_es/warning3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/websphere-brandmark.gif $RPM_BUILD_ROOT/opt/mqm/doc/es_es/websphere-brandmark.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/wizard_help.xml $RPM_BUILD_ROOT/opt/mqm/doc/es_es/wizard_help.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/es_es/workbench_link.gif $RPM_BUILD_ROOT/opt/mqm/doc/es_es/workbench_link.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/ax99995_.htm $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/ax99995_.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/banner.css $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/banner.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/bearings3.gif $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/bearings3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/bip4.css $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/bip4.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/blank.gif $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/blank.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/concept_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/concept_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/copyright.gif $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/copyright.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/corp_timestamp.gif $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/corp_timestamp.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/delta.gif $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/delta.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/deltaend.gif $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/deltaend.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/diagnostic3.gif $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/diagnostic3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/docscd.gif $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/docscd.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/external_link.gif $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/external_link.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/feedback_r.htm $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/feedback_r.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/g_postcard.htm $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/g_postcard.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/g_postcard_different.htm $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/g_postcard_different.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/g_postcard_oneqm.htm $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/g_postcard_oneqm.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/g_postcard_signon.htm $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/g_postcard_signon.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/g_postcard_twoqm.htm $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/g_postcard_twoqm.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/g_postcard_workings.htm $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/g_postcard_workings.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/general_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/general_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/helpbanner2.gif $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/helpbanner2.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/i_choices.htm $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/i_choices.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/i_comp_wiz.htm $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/i_comp_wiz.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/i_conf_win2000.htm $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/i_conf_win2000.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/i_config.htm $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/i_config.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/i_def_conf.htm $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/i_def_conf.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/i_def_conf_wiz.htm $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/i_def_conf_wiz.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/i_folders.htm $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/i_folders.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/i_join_def_clus.htm $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/i_join_def_clus.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/i_launch.htm $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/i_launch.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/i_listener.htm $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/i_listener.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/i_local_repos.htm $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/i_local_repos.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/i_net_prereq.htm $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/i_net_prereq.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/i_prepare_wiz.htm $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/i_prepare_wiz.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/i_qmgr_conf.htm $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/i_qmgr_conf.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/i_rem_repos.htm $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/i_rem_repos.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/i_remote_admin.htm $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/i_remote_admin.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/i_setup_type.htm $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/i_setup_type.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/i_soft_prereq.htm $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/i_soft_prereq.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/i_uninst.htm $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/i_uninst.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/i_wsmq.htm $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/i_wsmq.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/ibm-logo-white.gif $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/ibm-logo-white.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/ibmdita.css $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/ibmdita.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/ng2000.gif $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/ng2000.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/ngaix.gif $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/ngaix.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/nghpux.gif $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/nghpux.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/nglinux.gif $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/nglinux.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/ngnt.gif $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/ngnt.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/ngsolaris.gif $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/ngsolaris.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/ngunix.gif $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/ngunix.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/ngwin.gif $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/ngwin.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/ngxp.gif $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/ngxp.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/ngzos.gif $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/ngzos.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/problem3.gif $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/problem3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/query3.gif $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/query3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/reference_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/reference_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/sample_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/sample_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/start_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/start_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/swg_info_common.css $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/swg_info_common.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/task_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/task_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/timestamp.gif $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/timestamp.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/tip3.gif $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/tip3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/tutorial_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/tutorial_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/warning3.gif $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/warning3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/websphere-brandmark.gif $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/websphere-brandmark.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/wizard_help.xml $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/wizard_help.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/fr_fr/workbench_link.gif $RPM_BUILD_ROOT/opt/mqm/doc/fr_fr/workbench_link.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/ax99995_.htm $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/ax99995_.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/banner.css $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/banner.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/bearings3.gif $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/bearings3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/bip4.css $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/bip4.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/blank.gif $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/blank.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/concept_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/concept_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/copyright.gif $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/copyright.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/corp_timestamp.gif $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/corp_timestamp.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/delta.gif $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/delta.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/deltaend.gif $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/deltaend.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/diagnostic3.gif $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/diagnostic3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/docscd.gif $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/docscd.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/external_link.gif $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/external_link.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/feedback_r.htm $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/feedback_r.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/g_postcard.htm $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/g_postcard.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/g_postcard_different.htm $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/g_postcard_different.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/g_postcard_oneqm.htm $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/g_postcard_oneqm.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/g_postcard_signon.htm $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/g_postcard_signon.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/g_postcard_twoqm.htm $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/g_postcard_twoqm.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/g_postcard_workings.htm $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/g_postcard_workings.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/general_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/general_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/helpbanner2.gif $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/helpbanner2.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/i_choices.htm $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/i_choices.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/i_comp_wiz.htm $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/i_comp_wiz.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/i_conf_win2000.htm $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/i_conf_win2000.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/i_config.htm $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/i_config.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/i_def_conf.htm $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/i_def_conf.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/i_def_conf_wiz.htm $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/i_def_conf_wiz.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/i_folders.htm $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/i_folders.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/i_join_def_clus.htm $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/i_join_def_clus.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/i_launch.htm $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/i_launch.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/i_listener.htm $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/i_listener.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/i_local_repos.htm $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/i_local_repos.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/i_net_prereq.htm $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/i_net_prereq.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/i_prepare_wiz.htm $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/i_prepare_wiz.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/i_qmgr_conf.htm $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/i_qmgr_conf.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/i_rem_repos.htm $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/i_rem_repos.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/i_remote_admin.htm $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/i_remote_admin.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/i_setup_type.htm $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/i_setup_type.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/i_soft_prereq.htm $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/i_soft_prereq.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/i_uninst.htm $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/i_uninst.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/i_wsmq.htm $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/i_wsmq.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/ibm-logo-white.gif $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/ibm-logo-white.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/ibmdita.css $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/ibmdita.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/ng2000.gif $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/ng2000.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/ngaix.gif $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/ngaix.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/nghpux.gif $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/nghpux.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/nglinux.gif $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/nglinux.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/ngnt.gif $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/ngnt.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/ngsolaris.gif $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/ngsolaris.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/ngunix.gif $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/ngunix.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/ngwin.gif $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/ngwin.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/ngxp.gif $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/ngxp.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/ngzos.gif $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/ngzos.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/problem3.gif $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/problem3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/query3.gif $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/query3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/reference_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/reference_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/sample_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/sample_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/start_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/start_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/swg_info_common.css $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/swg_info_common.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/task_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/task_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/timestamp.gif $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/timestamp.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/tip3.gif $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/tip3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/tutorial_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/tutorial_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/warning3.gif $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/warning3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/websphere-brandmark.gif $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/websphere-brandmark.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/wizard_help.xml $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/wizard_help.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/hu_hu/workbench_link.gif $RPM_BUILD_ROOT/opt/mqm/doc/hu_hu/workbench_link.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/ax99995_.htm $RPM_BUILD_ROOT/opt/mqm/doc/it_it/ax99995_.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/banner.css $RPM_BUILD_ROOT/opt/mqm/doc/it_it/banner.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/bearings3.gif $RPM_BUILD_ROOT/opt/mqm/doc/it_it/bearings3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/bip4.css $RPM_BUILD_ROOT/opt/mqm/doc/it_it/bip4.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/blank.gif $RPM_BUILD_ROOT/opt/mqm/doc/it_it/blank.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/concept_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/it_it/concept_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/copyright.gif $RPM_BUILD_ROOT/opt/mqm/doc/it_it/copyright.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/corp_timestamp.gif $RPM_BUILD_ROOT/opt/mqm/doc/it_it/corp_timestamp.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/delta.gif $RPM_BUILD_ROOT/opt/mqm/doc/it_it/delta.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/deltaend.gif $RPM_BUILD_ROOT/opt/mqm/doc/it_it/deltaend.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/diagnostic3.gif $RPM_BUILD_ROOT/opt/mqm/doc/it_it/diagnostic3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/docscd.gif $RPM_BUILD_ROOT/opt/mqm/doc/it_it/docscd.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/external_link.gif $RPM_BUILD_ROOT/opt/mqm/doc/it_it/external_link.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/feedback_r.htm $RPM_BUILD_ROOT/opt/mqm/doc/it_it/feedback_r.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/g_postcard.htm $RPM_BUILD_ROOT/opt/mqm/doc/it_it/g_postcard.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/g_postcard_different.htm $RPM_BUILD_ROOT/opt/mqm/doc/it_it/g_postcard_different.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/g_postcard_oneqm.htm $RPM_BUILD_ROOT/opt/mqm/doc/it_it/g_postcard_oneqm.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/g_postcard_signon.htm $RPM_BUILD_ROOT/opt/mqm/doc/it_it/g_postcard_signon.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/g_postcard_twoqm.htm $RPM_BUILD_ROOT/opt/mqm/doc/it_it/g_postcard_twoqm.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/g_postcard_workings.htm $RPM_BUILD_ROOT/opt/mqm/doc/it_it/g_postcard_workings.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/general_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/it_it/general_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/helpbanner2.gif $RPM_BUILD_ROOT/opt/mqm/doc/it_it/helpbanner2.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/i_choices.htm $RPM_BUILD_ROOT/opt/mqm/doc/it_it/i_choices.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/i_comp_wiz.htm $RPM_BUILD_ROOT/opt/mqm/doc/it_it/i_comp_wiz.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/i_conf_win2000.htm $RPM_BUILD_ROOT/opt/mqm/doc/it_it/i_conf_win2000.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/i_config.htm $RPM_BUILD_ROOT/opt/mqm/doc/it_it/i_config.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/i_def_conf.htm $RPM_BUILD_ROOT/opt/mqm/doc/it_it/i_def_conf.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/i_def_conf_wiz.htm $RPM_BUILD_ROOT/opt/mqm/doc/it_it/i_def_conf_wiz.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/i_folders.htm $RPM_BUILD_ROOT/opt/mqm/doc/it_it/i_folders.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/i_join_def_clus.htm $RPM_BUILD_ROOT/opt/mqm/doc/it_it/i_join_def_clus.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/i_launch.htm $RPM_BUILD_ROOT/opt/mqm/doc/it_it/i_launch.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/i_listener.htm $RPM_BUILD_ROOT/opt/mqm/doc/it_it/i_listener.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/i_local_repos.htm $RPM_BUILD_ROOT/opt/mqm/doc/it_it/i_local_repos.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/i_net_prereq.htm $RPM_BUILD_ROOT/opt/mqm/doc/it_it/i_net_prereq.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/i_prepare_wiz.htm $RPM_BUILD_ROOT/opt/mqm/doc/it_it/i_prepare_wiz.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/i_qmgr_conf.htm $RPM_BUILD_ROOT/opt/mqm/doc/it_it/i_qmgr_conf.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/i_rem_repos.htm $RPM_BUILD_ROOT/opt/mqm/doc/it_it/i_rem_repos.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/i_remote_admin.htm $RPM_BUILD_ROOT/opt/mqm/doc/it_it/i_remote_admin.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/i_setup_type.htm $RPM_BUILD_ROOT/opt/mqm/doc/it_it/i_setup_type.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/i_soft_prereq.htm $RPM_BUILD_ROOT/opt/mqm/doc/it_it/i_soft_prereq.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/i_uninst.htm $RPM_BUILD_ROOT/opt/mqm/doc/it_it/i_uninst.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/i_wsmq.htm $RPM_BUILD_ROOT/opt/mqm/doc/it_it/i_wsmq.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/ibm-logo-white.gif $RPM_BUILD_ROOT/opt/mqm/doc/it_it/ibm-logo-white.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/ibmdita.css $RPM_BUILD_ROOT/opt/mqm/doc/it_it/ibmdita.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/ng2000.gif $RPM_BUILD_ROOT/opt/mqm/doc/it_it/ng2000.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/ngaix.gif $RPM_BUILD_ROOT/opt/mqm/doc/it_it/ngaix.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/nghpux.gif $RPM_BUILD_ROOT/opt/mqm/doc/it_it/nghpux.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/nglinux.gif $RPM_BUILD_ROOT/opt/mqm/doc/it_it/nglinux.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/ngnt.gif $RPM_BUILD_ROOT/opt/mqm/doc/it_it/ngnt.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/ngsolaris.gif $RPM_BUILD_ROOT/opt/mqm/doc/it_it/ngsolaris.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/ngunix.gif $RPM_BUILD_ROOT/opt/mqm/doc/it_it/ngunix.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/ngwin.gif $RPM_BUILD_ROOT/opt/mqm/doc/it_it/ngwin.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/ngxp.gif $RPM_BUILD_ROOT/opt/mqm/doc/it_it/ngxp.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/ngzos.gif $RPM_BUILD_ROOT/opt/mqm/doc/it_it/ngzos.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/problem3.gif $RPM_BUILD_ROOT/opt/mqm/doc/it_it/problem3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/query3.gif $RPM_BUILD_ROOT/opt/mqm/doc/it_it/query3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/reference_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/it_it/reference_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/sample_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/it_it/sample_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/start_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/it_it/start_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/swg_info_common.css $RPM_BUILD_ROOT/opt/mqm/doc/it_it/swg_info_common.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/task_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/it_it/task_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/timestamp.gif $RPM_BUILD_ROOT/opt/mqm/doc/it_it/timestamp.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/tip3.gif $RPM_BUILD_ROOT/opt/mqm/doc/it_it/tip3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/tutorial_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/it_it/tutorial_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/warning3.gif $RPM_BUILD_ROOT/opt/mqm/doc/it_it/warning3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/websphere-brandmark.gif $RPM_BUILD_ROOT/opt/mqm/doc/it_it/websphere-brandmark.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/wizard_help.xml $RPM_BUILD_ROOT/opt/mqm/doc/it_it/wizard_help.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/it_it/workbench_link.gif $RPM_BUILD_ROOT/opt/mqm/doc/it_it/workbench_link.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/ax99995_.htm $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/ax99995_.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/banner.css $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/banner.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/bearings3.gif $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/bearings3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/bip4.css $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/bip4.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/blank.gif $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/blank.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/concept_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/concept_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/copyright.gif $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/copyright.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/corp_timestamp.gif $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/corp_timestamp.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/delta.gif $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/delta.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/deltaend.gif $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/deltaend.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/diagnostic3.gif $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/diagnostic3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/docscd.gif $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/docscd.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/external_link.gif $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/external_link.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/feedback_r.htm $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/feedback_r.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/g_postcard.htm $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/g_postcard.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/g_postcard_different.htm $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/g_postcard_different.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/g_postcard_oneqm.htm $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/g_postcard_oneqm.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/g_postcard_signon.htm $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/g_postcard_signon.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/g_postcard_twoqm.htm $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/g_postcard_twoqm.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/g_postcard_workings.htm $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/g_postcard_workings.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/general_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/general_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/helpbanner2.gif $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/helpbanner2.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/i_choices.htm $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/i_choices.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/i_comp_wiz.htm $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/i_comp_wiz.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/i_conf_win2000.htm $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/i_conf_win2000.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/i_config.htm $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/i_config.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/i_def_conf.htm $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/i_def_conf.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/i_def_conf_wiz.htm $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/i_def_conf_wiz.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/i_folders.htm $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/i_folders.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/i_join_def_clus.htm $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/i_join_def_clus.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/i_launch.htm $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/i_launch.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/i_listener.htm $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/i_listener.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/i_local_repos.htm $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/i_local_repos.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/i_net_prereq.htm $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/i_net_prereq.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/i_prepare_wiz.htm $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/i_prepare_wiz.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/i_qmgr_conf.htm $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/i_qmgr_conf.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/i_rem_repos.htm $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/i_rem_repos.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/i_remote_admin.htm $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/i_remote_admin.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/i_setup_type.htm $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/i_setup_type.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/i_soft_prereq.htm $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/i_soft_prereq.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/i_uninst.htm $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/i_uninst.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/i_wsmq.htm $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/i_wsmq.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/ibm-logo-white.gif $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/ibm-logo-white.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/ibmdita.css $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/ibmdita.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/ng2000.gif $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/ng2000.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/ngaix.gif $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/ngaix.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/nghpux.gif $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/nghpux.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/nglinux.gif $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/nglinux.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/ngnt.gif $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/ngnt.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/ngsolaris.gif $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/ngsolaris.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/ngunix.gif $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/ngunix.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/ngwin.gif $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/ngwin.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/ngxp.gif $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/ngxp.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/ngzos.gif $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/ngzos.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/problem3.gif $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/problem3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/query3.gif $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/query3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/reference_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/reference_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/sample_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/sample_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/start_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/start_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/swg_info_common.css $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/swg_info_common.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/task_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/task_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/timestamp.gif $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/timestamp.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/tip3.gif $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/tip3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/tutorial_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/tutorial_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/warning3.gif $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/warning3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/websphere-brandmark.gif $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/websphere-brandmark.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/wizard_help.xml $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/wizard_help.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ja_jp/workbench_link.gif $RPM_BUILD_ROOT/opt/mqm/doc/ja_jp/workbench_link.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/ax99995_.htm $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/ax99995_.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/banner.css $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/banner.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/bearings3.gif $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/bearings3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/bip4.css $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/bip4.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/blank.gif $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/blank.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/concept_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/concept_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/copyright.gif $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/copyright.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/corp_timestamp.gif $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/corp_timestamp.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/delta.gif $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/delta.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/deltaend.gif $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/deltaend.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/diagnostic3.gif $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/diagnostic3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/docscd.gif $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/docscd.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/external_link.gif $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/external_link.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/feedback_r.htm $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/feedback_r.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/g_postcard.htm $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/g_postcard.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/g_postcard_different.htm $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/g_postcard_different.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/g_postcard_oneqm.htm $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/g_postcard_oneqm.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/g_postcard_signon.htm $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/g_postcard_signon.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/g_postcard_twoqm.htm $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/g_postcard_twoqm.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/g_postcard_workings.htm $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/g_postcard_workings.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/general_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/general_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/helpbanner2.gif $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/helpbanner2.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/i_choices.htm $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/i_choices.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/i_comp_wiz.htm $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/i_comp_wiz.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/i_conf_win2000.htm $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/i_conf_win2000.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/i_config.htm $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/i_config.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/i_def_conf.htm $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/i_def_conf.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/i_def_conf_wiz.htm $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/i_def_conf_wiz.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/i_folders.htm $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/i_folders.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/i_join_def_clus.htm $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/i_join_def_clus.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/i_launch.htm $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/i_launch.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/i_listener.htm $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/i_listener.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/i_local_repos.htm $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/i_local_repos.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/i_net_prereq.htm $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/i_net_prereq.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/i_prepare_wiz.htm $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/i_prepare_wiz.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/i_qmgr_conf.htm $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/i_qmgr_conf.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/i_rem_repos.htm $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/i_rem_repos.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/i_remote_admin.htm $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/i_remote_admin.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/i_setup_type.htm $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/i_setup_type.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/i_soft_prereq.htm $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/i_soft_prereq.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/i_uninst.htm $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/i_uninst.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/i_wsmq.htm $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/i_wsmq.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/ibm-logo-white.gif $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/ibm-logo-white.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/ibmdita.css $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/ibmdita.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/ng2000.gif $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/ng2000.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/ngaix.gif $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/ngaix.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/nghpux.gif $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/nghpux.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/nglinux.gif $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/nglinux.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/ngnt.gif $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/ngnt.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/ngsolaris.gif $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/ngsolaris.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/ngunix.gif $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/ngunix.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/ngwin.gif $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/ngwin.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/ngxp.gif $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/ngxp.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/ngzos.gif $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/ngzos.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/problem3.gif $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/problem3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/query3.gif $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/query3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/reference_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/reference_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/sample_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/sample_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/start_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/start_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/swg_info_common.css $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/swg_info_common.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/task_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/task_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/timestamp.gif $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/timestamp.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/tip3.gif $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/tip3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/tutorial_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/tutorial_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/warning3.gif $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/warning3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/websphere-brandmark.gif $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/websphere-brandmark.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/wizard_help.xml $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/wizard_help.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ko_kr/workbench_link.gif $RPM_BUILD_ROOT/opt/mqm/doc/ko_kr/workbench_link.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/ax99995_.htm $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/ax99995_.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/banner.css $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/banner.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/bearings3.gif $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/bearings3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/bip4.css $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/bip4.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/blank.gif $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/blank.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/concept_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/concept_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/copyright.gif $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/copyright.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/corp_timestamp.gif $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/corp_timestamp.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/delta.gif $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/delta.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/deltaend.gif $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/deltaend.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/diagnostic3.gif $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/diagnostic3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/docscd.gif $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/docscd.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/external_link.gif $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/external_link.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/feedback_r.htm $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/feedback_r.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/g_postcard.htm $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/g_postcard.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/g_postcard_different.htm $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/g_postcard_different.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/g_postcard_oneqm.htm $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/g_postcard_oneqm.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/g_postcard_signon.htm $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/g_postcard_signon.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/g_postcard_twoqm.htm $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/g_postcard_twoqm.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/g_postcard_workings.htm $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/g_postcard_workings.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/general_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/general_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/helpbanner2.gif $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/helpbanner2.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/i_choices.htm $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/i_choices.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/i_comp_wiz.htm $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/i_comp_wiz.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/i_conf_win2000.htm $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/i_conf_win2000.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/i_config.htm $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/i_config.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/i_def_conf.htm $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/i_def_conf.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/i_def_conf_wiz.htm $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/i_def_conf_wiz.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/i_folders.htm $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/i_folders.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/i_join_def_clus.htm $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/i_join_def_clus.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/i_launch.htm $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/i_launch.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/i_listener.htm $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/i_listener.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/i_local_repos.htm $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/i_local_repos.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/i_net_prereq.htm $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/i_net_prereq.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/i_prepare_wiz.htm $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/i_prepare_wiz.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/i_qmgr_conf.htm $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/i_qmgr_conf.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/i_rem_repos.htm $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/i_rem_repos.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/i_remote_admin.htm $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/i_remote_admin.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/i_setup_type.htm $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/i_setup_type.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/i_soft_prereq.htm $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/i_soft_prereq.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/i_uninst.htm $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/i_uninst.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/i_wsmq.htm $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/i_wsmq.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/ibm-logo-white.gif $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/ibm-logo-white.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/ibmdita.css $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/ibmdita.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/ng2000.gif $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/ng2000.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/ngaix.gif $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/ngaix.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/nghpux.gif $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/nghpux.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/nglinux.gif $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/nglinux.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/ngnt.gif $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/ngnt.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/ngsolaris.gif $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/ngsolaris.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/ngunix.gif $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/ngunix.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/ngwin.gif $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/ngwin.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/ngxp.gif $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/ngxp.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/ngzos.gif $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/ngzos.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/problem3.gif $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/problem3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/query3.gif $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/query3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/reference_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/reference_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/sample_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/sample_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/start_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/start_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/swg_info_common.css $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/swg_info_common.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/task_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/task_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/timestamp.gif $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/timestamp.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/tip3.gif $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/tip3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/tutorial_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/tutorial_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/warning3.gif $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/warning3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/websphere-brandmark.gif $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/websphere-brandmark.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/wizard_help.xml $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/wizard_help.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pl_pl/workbench_link.gif $RPM_BUILD_ROOT/opt/mqm/doc/pl_pl/workbench_link.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/ax99995_.htm $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/ax99995_.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/banner.css $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/banner.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/bearings3.gif $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/bearings3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/bip4.css $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/bip4.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/blank.gif $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/blank.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/concept_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/concept_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/copyright.gif $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/copyright.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/corp_timestamp.gif $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/corp_timestamp.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/delta.gif $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/delta.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/deltaend.gif $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/deltaend.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/diagnostic3.gif $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/diagnostic3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/docscd.gif $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/docscd.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/external_link.gif $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/external_link.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/feedback_r.htm $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/feedback_r.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/g_postcard.htm $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/g_postcard.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/g_postcard_different.htm $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/g_postcard_different.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/g_postcard_oneqm.htm $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/g_postcard_oneqm.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/g_postcard_signon.htm $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/g_postcard_signon.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/g_postcard_twoqm.htm $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/g_postcard_twoqm.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/g_postcard_workings.htm $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/g_postcard_workings.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/general_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/general_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/helpbanner2.gif $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/helpbanner2.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/i_choices.htm $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/i_choices.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/i_comp_wiz.htm $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/i_comp_wiz.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/i_conf_win2000.htm $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/i_conf_win2000.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/i_config.htm $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/i_config.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/i_def_conf.htm $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/i_def_conf.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/i_def_conf_wiz.htm $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/i_def_conf_wiz.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/i_folders.htm $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/i_folders.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/i_join_def_clus.htm $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/i_join_def_clus.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/i_launch.htm $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/i_launch.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/i_listener.htm $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/i_listener.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/i_local_repos.htm $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/i_local_repos.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/i_net_prereq.htm $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/i_net_prereq.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/i_prepare_wiz.htm $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/i_prepare_wiz.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/i_qmgr_conf.htm $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/i_qmgr_conf.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/i_rem_repos.htm $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/i_rem_repos.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/i_remote_admin.htm $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/i_remote_admin.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/i_setup_type.htm $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/i_setup_type.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/i_soft_prereq.htm $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/i_soft_prereq.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/i_uninst.htm $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/i_uninst.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/i_wsmq.htm $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/i_wsmq.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/ibm-logo-white.gif $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/ibm-logo-white.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/ibmdita.css $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/ibmdita.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/ng2000.gif $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/ng2000.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/ngaix.gif $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/ngaix.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/nghpux.gif $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/nghpux.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/nglinux.gif $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/nglinux.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/ngnt.gif $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/ngnt.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/ngsolaris.gif $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/ngsolaris.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/ngunix.gif $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/ngunix.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/ngwin.gif $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/ngwin.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/ngxp.gif $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/ngxp.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/ngzos.gif $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/ngzos.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/problem3.gif $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/problem3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/query3.gif $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/query3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/reference_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/reference_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/sample_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/sample_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/start_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/start_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/swg_info_common.css $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/swg_info_common.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/task_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/task_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/timestamp.gif $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/timestamp.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/tip3.gif $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/tip3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/tutorial_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/tutorial_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/warning3.gif $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/warning3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/websphere-brandmark.gif $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/websphere-brandmark.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/wizard_help.xml $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/wizard_help.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/pt_br/workbench_link.gif $RPM_BUILD_ROOT/opt/mqm/doc/pt_br/workbench_link.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/ax99995_.htm $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/ax99995_.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/banner.css $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/banner.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/bearings3.gif $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/bearings3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/bip4.css $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/bip4.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/blank.gif $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/blank.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/concept_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/concept_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/copyright.gif $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/copyright.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/corp_timestamp.gif $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/corp_timestamp.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/delta.gif $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/delta.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/deltaend.gif $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/deltaend.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/diagnostic3.gif $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/diagnostic3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/docscd.gif $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/docscd.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/external_link.gif $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/external_link.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/feedback_r.htm $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/feedback_r.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/g_postcard.htm $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/g_postcard.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/g_postcard_different.htm $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/g_postcard_different.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/g_postcard_oneqm.htm $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/g_postcard_oneqm.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/g_postcard_signon.htm $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/g_postcard_signon.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/g_postcard_twoqm.htm $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/g_postcard_twoqm.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/g_postcard_workings.htm $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/g_postcard_workings.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/general_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/general_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/helpbanner2.gif $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/helpbanner2.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/i_choices.htm $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/i_choices.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/i_comp_wiz.htm $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/i_comp_wiz.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/i_conf_win2000.htm $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/i_conf_win2000.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/i_config.htm $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/i_config.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/i_def_conf.htm $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/i_def_conf.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/i_def_conf_wiz.htm $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/i_def_conf_wiz.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/i_folders.htm $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/i_folders.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/i_join_def_clus.htm $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/i_join_def_clus.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/i_launch.htm $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/i_launch.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/i_listener.htm $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/i_listener.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/i_local_repos.htm $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/i_local_repos.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/i_net_prereq.htm $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/i_net_prereq.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/i_prepare_wiz.htm $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/i_prepare_wiz.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/i_qmgr_conf.htm $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/i_qmgr_conf.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/i_rem_repos.htm $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/i_rem_repos.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/i_remote_admin.htm $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/i_remote_admin.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/i_setup_type.htm $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/i_setup_type.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/i_soft_prereq.htm $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/i_soft_prereq.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/i_uninst.htm $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/i_uninst.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/i_wsmq.htm $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/i_wsmq.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/ibm-logo-white.gif $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/ibm-logo-white.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/ibmdita.css $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/ibmdita.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/ng2000.gif $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/ng2000.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/ngaix.gif $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/ngaix.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/nghpux.gif $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/nghpux.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/nglinux.gif $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/nglinux.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/ngnt.gif $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/ngnt.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/ngsolaris.gif $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/ngsolaris.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/ngunix.gif $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/ngunix.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/ngwin.gif $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/ngwin.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/ngxp.gif $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/ngxp.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/ngzos.gif $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/ngzos.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/problem3.gif $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/problem3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/query3.gif $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/query3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/reference_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/reference_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/sample_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/sample_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/start_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/start_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/swg_info_common.css $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/swg_info_common.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/task_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/task_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/timestamp.gif $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/timestamp.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/tip3.gif $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/tip3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/tutorial_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/tutorial_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/warning3.gif $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/warning3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/websphere-brandmark.gif $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/websphere-brandmark.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/wizard_help.xml $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/wizard_help.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/ru_ru/workbench_link.gif $RPM_BUILD_ROOT/opt/mqm/doc/ru_ru/workbench_link.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/ax99995_.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/ax99995_.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/banner.css $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/banner.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/bearings3.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/bearings3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/bip4.css $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/bip4.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/blank.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/blank.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/concept_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/concept_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/copyright.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/copyright.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/corp_timestamp.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/corp_timestamp.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/delta.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/delta.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/deltaend.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/deltaend.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/diagnostic3.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/diagnostic3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/docscd.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/docscd.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/external_link.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/external_link.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/feedback_r.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/feedback_r.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/g_postcard.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/g_postcard.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/g_postcard_different.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/g_postcard_different.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/g_postcard_oneqm.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/g_postcard_oneqm.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/g_postcard_signon.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/g_postcard_signon.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/g_postcard_twoqm.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/g_postcard_twoqm.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/g_postcard_workings.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/g_postcard_workings.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/general_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/general_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/helpbanner2.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/helpbanner2.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/i_choices.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/i_choices.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/i_comp_wiz.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/i_comp_wiz.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/i_conf_win2000.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/i_conf_win2000.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/i_config.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/i_config.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/i_def_conf.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/i_def_conf.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/i_def_conf_wiz.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/i_def_conf_wiz.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/i_folders.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/i_folders.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/i_join_def_clus.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/i_join_def_clus.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/i_launch.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/i_launch.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/i_listener.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/i_listener.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/i_local_repos.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/i_local_repos.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/i_net_prereq.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/i_net_prereq.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/i_prepare_wiz.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/i_prepare_wiz.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/i_qmgr_conf.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/i_qmgr_conf.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/i_rem_repos.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/i_rem_repos.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/i_remote_admin.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/i_remote_admin.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/i_setup_type.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/i_setup_type.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/i_soft_prereq.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/i_soft_prereq.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/i_uninst.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/i_uninst.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/i_wsmq.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/i_wsmq.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/ibm-logo-white.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/ibm-logo-white.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/ibmdita.css $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/ibmdita.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/ng2000.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/ng2000.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/ngaix.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/ngaix.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/nghpux.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/nghpux.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/nglinux.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/nglinux.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/ngnt.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/ngnt.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/ngsolaris.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/ngsolaris.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/ngunix.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/ngunix.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/ngwin.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/ngwin.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/ngxp.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/ngxp.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/ngzos.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/ngzos.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/problem3.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/problem3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/query3.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/query3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/reference_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/reference_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/sample_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/sample_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/start_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/start_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/swg_info_common.css $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/swg_info_common.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/task_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/task_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/timestamp.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/timestamp.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/tip3.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/tip3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/tutorial_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/tutorial_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/warning3.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/warning3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/websphere-brandmark.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/websphere-brandmark.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/wizard_help.xml $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/wizard_help.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_cn/workbench_link.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_cn/workbench_link.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/ax99995_.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/ax99995_.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/banner.css $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/banner.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/bearings3.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/bearings3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/bip4.css $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/bip4.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/blank.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/blank.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/concept_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/concept_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/copyright.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/copyright.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/corp_timestamp.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/corp_timestamp.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/delta.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/delta.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/deltaend.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/deltaend.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/diagnostic3.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/diagnostic3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/docscd.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/docscd.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/external_link.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/external_link.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/feedback_r.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/feedback_r.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/g_postcard.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/g_postcard.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/g_postcard_different.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/g_postcard_different.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/g_postcard_oneqm.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/g_postcard_oneqm.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/g_postcard_signon.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/g_postcard_signon.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/g_postcard_twoqm.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/g_postcard_twoqm.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/g_postcard_workings.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/g_postcard_workings.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/general_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/general_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/helpbanner2.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/helpbanner2.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/i_choices.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/i_choices.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/i_comp_wiz.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/i_comp_wiz.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/i_conf_win2000.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/i_conf_win2000.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/i_config.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/i_config.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/i_def_conf.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/i_def_conf.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/i_def_conf_wiz.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/i_def_conf_wiz.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/i_folders.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/i_folders.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/i_join_def_clus.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/i_join_def_clus.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/i_launch.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/i_launch.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/i_listener.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/i_listener.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/i_local_repos.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/i_local_repos.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/i_net_prereq.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/i_net_prereq.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/i_prepare_wiz.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/i_prepare_wiz.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/i_qmgr_conf.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/i_qmgr_conf.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/i_rem_repos.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/i_rem_repos.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/i_remote_admin.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/i_remote_admin.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/i_setup_type.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/i_setup_type.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/i_soft_prereq.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/i_soft_prereq.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/i_uninst.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/i_uninst.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/i_wsmq.htm $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/i_wsmq.htm
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/ibm-logo-white.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/ibm-logo-white.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/ibmdita.css $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/ibmdita.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/ng2000.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/ng2000.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/ngaix.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/ngaix.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/nghpux.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/nghpux.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/nglinux.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/nglinux.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/ngnt.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/ngnt.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/ngsolaris.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/ngsolaris.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/ngunix.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/ngunix.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/ngwin.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/ngwin.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/ngxp.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/ngxp.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/ngzos.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/ngzos.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/problem3.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/problem3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/query3.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/query3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/reference_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/reference_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/sample_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/sample_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/start_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/start_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/swg_info_common.css $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/swg_info_common.css
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/task_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/task_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/timestamp.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/timestamp.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/tip3.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/tip3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/tutorial_obj.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/tutorial_obj.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/warning3.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/warning3.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/websphere-brandmark.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/websphere-brandmark.gif
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/wizard_help.xml $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/wizard_help.xml
install /build/slot1/p750_P/obj/amd64_linux_2/ship/opt/mqm/doc/zh_tw/workbench_link.gif $RPM_BUILD_ROOT/opt/mqm/doc/zh_tw/workbench_link.gif

%files
%dir %attr(555,mqm,mqm) "/opt/mqm"
%dir %attr(555,mqm,mqm) "/opt/mqm/properties"
%dir %attr(555,mqm,mqm) "/opt/mqm/properties/version"
%dir %attr(555,mqm,mqm) "/opt/mqm/lib"
%dir %attr(555,mqm,mqm) "/opt/mqm/lib/compat"
%dir %attr(555,mqm,mqm) "/opt/mqm/lib64"
%dir %attr(555,mqm,mqm) "/opt/mqm/lib64/compat"
%dir %attr(555,mqm,mqm) "/opt/mqm/bin"
%dir %attr(555,mqm,mqm) "/opt/mqm/inc"
%dir %attr(555,mqm,mqm) "/opt/mqm/lib/4.1"
%dir %attr(555,mqm,mqm) "/opt/mqm/lib64/4.1"
%dir %attr(555,mqm,mqm) "/opt/mqm/java"
%dir %attr(555,mqm,mqm) "/opt/mqm/java/lib"
%dir %attr(555,mqm,mqm) "/opt/mqm/java/lib64"
%dir %attr(555,mqm,mqm) "/opt/mqm/java/lib/jdbc"
%dir %attr(555,mqm,mqm) "/opt/mqm/java/lib64/jdbc"
%dir %attr(555,mqm,mqm) "/opt/mqm/java/bin"
%dir %attr(555,mqm,mqm) "/opt/mqm/doc"
%dir %attr(555,mqm,mqm) "/opt/mqm/doc/cs_cz"
%dir %attr(555,mqm,mqm) "/opt/mqm/doc/de_de"
%dir %attr(555,mqm,mqm) "/opt/mqm/doc/en_us"
%dir %attr(555,mqm,mqm) "/opt/mqm/doc/es_es"
%dir %attr(555,mqm,mqm) "/opt/mqm/doc/fr_fr"
%dir %attr(555,mqm,mqm) "/opt/mqm/doc/hu_hu"
%dir %attr(555,mqm,mqm) "/opt/mqm/doc/it_it"
%dir %attr(555,mqm,mqm) "/opt/mqm/doc/ja_jp"
%dir %attr(555,mqm,mqm) "/opt/mqm/doc/ko_kr"
%dir %attr(555,mqm,mqm) "/opt/mqm/doc/pl_pl"
%dir %attr(555,mqm,mqm) "/opt/mqm/doc/pt_br"
%dir %attr(555,mqm,mqm) "/opt/mqm/doc/ru_ru"
%dir %attr(555,mqm,mqm) "/opt/mqm/doc/zh_cn"
%dir %attr(555,mqm,mqm) "/opt/mqm/doc/zh_tw"
%attr(444,mqm,mqm) "/opt/mqm/properties/version/IBM_WebSphere_MQ_Server.7.5.0.swtag"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib64/libmqmf_r.so"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/runmqbrk"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/amqfcxba"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/dltmqbrk"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/endmqbrk"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/amqfqpub"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/amqcrsta"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/amqpcsea"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib64/amqzif_r"
%attr(554,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib64/amqzfu"
%attr(554,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib64/amqzfu1"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib64/libmqmz1_r.so"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/amqzfuma"
%attr(550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/amqoamd"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib64/libmqmz0_r.so"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/amqzlaa0"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/amqzlsa0"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/amqzslf0"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/amqzxma0"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/amqzmgr0"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/amqzmuc0"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/amqzmuf0"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/amqzmur0"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/crtmqcvx"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/crtmqm"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/dltmqm"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/dmpmqaut"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/dspmqcsv"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/dspmqfls"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/dspmqaut"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/dspmqtrn"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/dspmq"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/dspmqrte"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/setmqaut"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/endmqcsv"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/endmqm"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/rcdmqimg"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/rcrmqobj"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/runmqchi"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/runmqlsr"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/runswchl"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/amqrcmla"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/amqrrmfa"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/amqrmppa"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/amqzlwa0"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/endmqlsr"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/runmqchl"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/runmqsc"
%attr(550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/runmqtrm"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/rsvmqtrn"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/addmqinf"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/dspmqinf"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/rmvmqinf"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/amqsstop"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/setmqm"
%attr(550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/setmqprd"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/dspmqspl"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/setmqspl"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/strmqm"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/strmqcsv"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib/libmqml_r.so"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib64/libmqml_r.so"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib64/libmqmalda_r.so"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib64/libmqmaldb_r.so"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib/libmqmzf.so"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib64/libmqmzf.so"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib/libmqmzf_r.so"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib64/libmqmzf_r.so"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib/libmqutl.so"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib64/libmqutl.so"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib/libmqutl_r.so"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib64/libmqutl_r.so"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib/libmqzi.so"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib64/libmqzi.so"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib/libmqzi_r.so"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib64/libmqzi_r.so"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib/libmqds.so"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib64/libmqds.so"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib/libmqds_r.so"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib64/libmqds_r.so"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib/libmqmr.so"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib64/libmqmr.so"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib/libmqmr_r.so"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib64/libmqmr_r.so"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib/libmqmcb.so"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib64/libmqmcb.so"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib/libmqmcb_r.so"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib64/libmqmcb_r.so"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib/libmqmxa.so"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib64/libmqmxa.so"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib64/libmqmxa64.so"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib/libmqmxa_r.so"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib64/libmqmxa_r.so"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib64/libmqmxa64_r.so"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/amqzdmaa"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib/libmqmax.so"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib64/libmqmax.so"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib/libmqmax_r.so"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib64/libmqmax_r.so"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib64/libmqmax64.so"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib64/libmqmax64_r.so"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib/amqzaax"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib64/amqzaax"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib/amqzaax_r"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib64/amqzaax_r"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib64/amq64ax"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib64/amq64ax_r"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/runmqdlq"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/dmpmqlog"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib/4.1/libimqs23gl.so"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib/4.1/libimqs23gl_r.so"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib64/4.1/libimqs23gl.so"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib64/4.1/libimqs23gl_r.so"
%attr(444,mqm,mqm) "/opt/mqm/lib/amqpcert.lic"
%attr(550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/ffstsummary"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/amqldmpa"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/amqspdbg"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/amqrfdm"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/amqrdbgm"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/amqorxr"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib64/amqwasoa"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib64/amqwascx"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib/libmqjxs_r.so"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib64/libmqjxs_r.so"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib/libmqjxds_r.so"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib64/libmqjxds_r.so"
%attr(554,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib64/amqzxmr0"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/java/lib/libmqjbnd.so"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/java/lib64/libmqjbnd.so"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/java/lib/jdbc/jdbcdb2.o"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/java/lib/jdbc/jdbcora.o"
%attr(555,mqm,mqm) "/opt/mqm/java/lib/jdbc/Makefile"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/java/lib64/jdbc/jdbcdb2.o"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/java/lib64/jdbc/jdbcora.o"
%attr(555,mqm,mqm) "/opt/mqm/java/lib64/jdbc/Makefile"
%attr(555,mqm,mqm) "/opt/mqm/java/lib/com.ibm.mq.postcard.jar"
%attr(555,mqm,mqm) "/opt/mqm/java/bin/postcard"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/amqpcard"
%attr(555,mqm,mqm) "/opt/mqm/bin/runpcard"
%attr(555,mqm,mqm) "/opt/mqm/java/lib/com.ibm.mq.defaultconfig.jar"
%attr(555,mqm,mqm) "/opt/mqm/java/bin/DefaultConfiguration"
%attr(555,mqm,mqm) "/opt/mqm/bin/rundefconf"
%attr(555,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/amqmgse"
%attr(550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/amqmbbrk"
%attr(550,mqm,mqm) "/opt/mqm/bin/migmbbrk"
%attr(550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib64/libicudata.so.32.0"
%attr(550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib64/libicuuc.so.32.0"
%attr(550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib64/libicui18n.so.32.0"
%attr(550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib64/libxml4c.so.56.3"
%attr(550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib64/libxml4c-depdom.so.56.3"
%attr(550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/lib64/libXML4CMessages.so.56.3"
%attr(6550,mqm,mqm) %verify(not md5 mtime) "/opt/mqm/bin/amqmfsck"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/ax99995_.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/banner.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/bearings3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/bip4.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/blank.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/concept_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/copyright.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/corp_timestamp.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/delta.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/deltaend.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/diagnostic3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/docscd.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/external_link.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/feedback_r.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/g_postcard.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/g_postcard_different.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/g_postcard_oneqm.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/g_postcard_signon.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/g_postcard_twoqm.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/g_postcard_workings.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/general_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/helpbanner2.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/i_choices.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/i_comp_wiz.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/i_conf_win2000.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/i_config.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/i_def_conf.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/i_def_conf_wiz.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/i_folders.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/i_join_def_clus.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/i_launch.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/i_listener.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/i_local_repos.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/i_net_prereq.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/i_prepare_wiz.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/i_qmgr_conf.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/i_rem_repos.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/i_remote_admin.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/i_setup_type.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/i_soft_prereq.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/i_uninst.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/i_wsmq.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/ibm-logo-white.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/ibmdita.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/ng2000.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/ngaix.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/nghpux.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/nglinux.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/ngnt.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/ngsolaris.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/ngunix.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/ngwin.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/ngxp.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/ngzos.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/problem3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/query3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/reference_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/sample_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/start_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/swg_info_common.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/task_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/timestamp.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/tip3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/tutorial_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/warning3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/websphere-brandmark.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/wizard_help.xml"
%attr(444,mqm,mqm) "/opt/mqm/doc/cs_cz/workbench_link.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/ax99995_.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/banner.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/bearings3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/bip4.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/blank.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/concept_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/copyright.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/corp_timestamp.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/delta.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/deltaend.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/diagnostic3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/docscd.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/external_link.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/feedback_r.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/g_postcard.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/g_postcard_different.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/g_postcard_oneqm.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/g_postcard_signon.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/g_postcard_twoqm.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/g_postcard_workings.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/general_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/helpbanner2.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/i_choices.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/i_comp_wiz.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/i_conf_win2000.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/i_config.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/i_def_conf.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/i_def_conf_wiz.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/i_folders.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/i_join_def_clus.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/i_launch.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/i_listener.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/i_local_repos.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/i_net_prereq.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/i_prepare_wiz.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/i_qmgr_conf.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/i_rem_repos.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/i_remote_admin.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/i_setup_type.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/i_soft_prereq.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/i_uninst.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/i_wsmq.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/ibm-logo-white.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/ibmdita.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/ng2000.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/ngaix.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/nghpux.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/nglinux.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/ngnt.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/ngsolaris.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/ngunix.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/ngwin.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/ngxp.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/ngzos.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/problem3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/query3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/reference_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/sample_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/start_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/swg_info_common.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/task_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/timestamp.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/tip3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/tutorial_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/warning3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/websphere-brandmark.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/wizard_help.xml"
%attr(444,mqm,mqm) "/opt/mqm/doc/de_de/workbench_link.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/ax99995_.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/banner.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/bearings3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/bip4.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/blank.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/concept_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/copyright.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/corp_timestamp.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/delta.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/deltaend.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/diagnostic3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/docscd.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/external_link.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/feedback_r.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/g_postcard.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/g_postcard_different.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/g_postcard_oneqm.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/g_postcard_signon.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/g_postcard_twoqm.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/g_postcard_workings.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/general_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/helpbanner2.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/i_choices.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/i_comp_wiz.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/i_conf_win2000.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/i_config.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/i_def_conf.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/i_def_conf_wiz.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/i_folders.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/i_join_def_clus.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/i_launch.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/i_listener.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/i_local_repos.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/i_net_prereq.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/i_prepare_wiz.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/i_qmgr_conf.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/i_rem_repos.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/i_remote_admin.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/i_setup_type.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/i_soft_prereq.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/i_uninst.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/i_wsmq.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/ibm-logo-white.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/ibmdita.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/ng2000.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/ngaix.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/nghpux.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/nglinux.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/ngnt.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/ngsolaris.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/ngunix.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/ngwin.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/ngxp.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/ngzos.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/plugin.properties"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/plugin.xml"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/problem3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/query3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/reference_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/sample_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/start_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/swg_info_common.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/task_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/timestamp.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/tip3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/tutorial_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/warning3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/websphere-brandmark.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/wizard_help.xml"
%attr(444,mqm,mqm) "/opt/mqm/doc/en_us/workbench_link.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/ax99995_.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/banner.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/bearings3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/bip4.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/blank.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/concept_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/copyright.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/corp_timestamp.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/delta.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/deltaend.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/diagnostic3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/docscd.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/external_link.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/feedback_r.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/g_postcard.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/g_postcard_different.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/g_postcard_oneqm.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/g_postcard_signon.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/g_postcard_twoqm.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/g_postcard_workings.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/general_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/helpbanner2.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/i_choices.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/i_comp_wiz.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/i_conf_win2000.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/i_config.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/i_def_conf.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/i_def_conf_wiz.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/i_folders.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/i_join_def_clus.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/i_launch.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/i_listener.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/i_local_repos.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/i_net_prereq.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/i_prepare_wiz.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/i_qmgr_conf.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/i_rem_repos.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/i_remote_admin.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/i_setup_type.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/i_soft_prereq.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/i_uninst.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/i_wsmq.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/ibm-logo-white.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/ibmdita.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/ng2000.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/ngaix.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/nghpux.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/nglinux.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/ngnt.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/ngsolaris.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/ngunix.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/ngwin.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/ngxp.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/ngzos.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/problem3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/query3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/reference_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/sample_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/start_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/swg_info_common.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/task_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/timestamp.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/tip3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/tutorial_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/warning3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/websphere-brandmark.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/wizard_help.xml"
%attr(444,mqm,mqm) "/opt/mqm/doc/es_es/workbench_link.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/ax99995_.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/banner.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/bearings3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/bip4.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/blank.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/concept_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/copyright.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/corp_timestamp.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/delta.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/deltaend.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/diagnostic3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/docscd.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/external_link.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/feedback_r.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/g_postcard.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/g_postcard_different.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/g_postcard_oneqm.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/g_postcard_signon.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/g_postcard_twoqm.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/g_postcard_workings.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/general_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/helpbanner2.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/i_choices.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/i_comp_wiz.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/i_conf_win2000.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/i_config.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/i_def_conf.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/i_def_conf_wiz.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/i_folders.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/i_join_def_clus.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/i_launch.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/i_listener.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/i_local_repos.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/i_net_prereq.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/i_prepare_wiz.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/i_qmgr_conf.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/i_rem_repos.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/i_remote_admin.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/i_setup_type.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/i_soft_prereq.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/i_uninst.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/i_wsmq.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/ibm-logo-white.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/ibmdita.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/ng2000.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/ngaix.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/nghpux.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/nglinux.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/ngnt.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/ngsolaris.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/ngunix.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/ngwin.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/ngxp.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/ngzos.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/problem3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/query3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/reference_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/sample_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/start_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/swg_info_common.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/task_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/timestamp.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/tip3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/tutorial_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/warning3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/websphere-brandmark.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/wizard_help.xml"
%attr(444,mqm,mqm) "/opt/mqm/doc/fr_fr/workbench_link.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/ax99995_.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/banner.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/bearings3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/bip4.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/blank.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/concept_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/copyright.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/corp_timestamp.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/delta.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/deltaend.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/diagnostic3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/docscd.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/external_link.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/feedback_r.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/g_postcard.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/g_postcard_different.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/g_postcard_oneqm.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/g_postcard_signon.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/g_postcard_twoqm.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/g_postcard_workings.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/general_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/helpbanner2.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/i_choices.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/i_comp_wiz.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/i_conf_win2000.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/i_config.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/i_def_conf.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/i_def_conf_wiz.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/i_folders.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/i_join_def_clus.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/i_launch.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/i_listener.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/i_local_repos.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/i_net_prereq.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/i_prepare_wiz.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/i_qmgr_conf.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/i_rem_repos.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/i_remote_admin.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/i_setup_type.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/i_soft_prereq.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/i_uninst.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/i_wsmq.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/ibm-logo-white.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/ibmdita.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/ng2000.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/ngaix.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/nghpux.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/nglinux.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/ngnt.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/ngsolaris.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/ngunix.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/ngwin.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/ngxp.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/ngzos.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/problem3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/query3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/reference_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/sample_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/start_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/swg_info_common.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/task_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/timestamp.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/tip3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/tutorial_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/warning3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/websphere-brandmark.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/wizard_help.xml"
%attr(444,mqm,mqm) "/opt/mqm/doc/hu_hu/workbench_link.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/ax99995_.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/banner.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/bearings3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/bip4.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/blank.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/concept_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/copyright.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/corp_timestamp.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/delta.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/deltaend.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/diagnostic3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/docscd.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/external_link.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/feedback_r.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/g_postcard.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/g_postcard_different.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/g_postcard_oneqm.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/g_postcard_signon.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/g_postcard_twoqm.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/g_postcard_workings.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/general_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/helpbanner2.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/i_choices.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/i_comp_wiz.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/i_conf_win2000.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/i_config.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/i_def_conf.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/i_def_conf_wiz.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/i_folders.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/i_join_def_clus.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/i_launch.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/i_listener.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/i_local_repos.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/i_net_prereq.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/i_prepare_wiz.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/i_qmgr_conf.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/i_rem_repos.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/i_remote_admin.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/i_setup_type.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/i_soft_prereq.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/i_uninst.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/i_wsmq.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/ibm-logo-white.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/ibmdita.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/ng2000.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/ngaix.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/nghpux.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/nglinux.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/ngnt.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/ngsolaris.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/ngunix.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/ngwin.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/ngxp.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/ngzos.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/problem3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/query3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/reference_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/sample_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/start_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/swg_info_common.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/task_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/timestamp.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/tip3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/tutorial_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/warning3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/websphere-brandmark.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/wizard_help.xml"
%attr(444,mqm,mqm) "/opt/mqm/doc/it_it/workbench_link.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/ax99995_.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/banner.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/bearings3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/bip4.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/blank.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/concept_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/copyright.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/corp_timestamp.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/delta.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/deltaend.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/diagnostic3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/docscd.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/external_link.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/feedback_r.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/g_postcard.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/g_postcard_different.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/g_postcard_oneqm.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/g_postcard_signon.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/g_postcard_twoqm.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/g_postcard_workings.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/general_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/helpbanner2.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/i_choices.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/i_comp_wiz.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/i_conf_win2000.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/i_config.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/i_def_conf.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/i_def_conf_wiz.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/i_folders.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/i_join_def_clus.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/i_launch.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/i_listener.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/i_local_repos.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/i_net_prereq.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/i_prepare_wiz.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/i_qmgr_conf.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/i_rem_repos.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/i_remote_admin.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/i_setup_type.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/i_soft_prereq.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/i_uninst.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/i_wsmq.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/ibm-logo-white.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/ibmdita.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/ng2000.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/ngaix.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/nghpux.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/nglinux.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/ngnt.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/ngsolaris.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/ngunix.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/ngwin.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/ngxp.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/ngzos.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/problem3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/query3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/reference_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/sample_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/start_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/swg_info_common.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/task_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/timestamp.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/tip3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/tutorial_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/warning3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/websphere-brandmark.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/wizard_help.xml"
%attr(444,mqm,mqm) "/opt/mqm/doc/ja_jp/workbench_link.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/ax99995_.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/banner.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/bearings3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/bip4.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/blank.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/concept_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/copyright.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/corp_timestamp.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/delta.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/deltaend.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/diagnostic3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/docscd.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/external_link.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/feedback_r.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/g_postcard.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/g_postcard_different.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/g_postcard_oneqm.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/g_postcard_signon.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/g_postcard_twoqm.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/g_postcard_workings.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/general_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/helpbanner2.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/i_choices.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/i_comp_wiz.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/i_conf_win2000.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/i_config.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/i_def_conf.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/i_def_conf_wiz.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/i_folders.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/i_join_def_clus.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/i_launch.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/i_listener.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/i_local_repos.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/i_net_prereq.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/i_prepare_wiz.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/i_qmgr_conf.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/i_rem_repos.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/i_remote_admin.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/i_setup_type.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/i_soft_prereq.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/i_uninst.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/i_wsmq.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/ibm-logo-white.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/ibmdita.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/ng2000.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/ngaix.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/nghpux.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/nglinux.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/ngnt.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/ngsolaris.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/ngunix.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/ngwin.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/ngxp.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/ngzos.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/problem3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/query3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/reference_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/sample_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/start_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/swg_info_common.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/task_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/timestamp.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/tip3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/tutorial_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/warning3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/websphere-brandmark.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/wizard_help.xml"
%attr(444,mqm,mqm) "/opt/mqm/doc/ko_kr/workbench_link.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/ax99995_.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/banner.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/bearings3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/bip4.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/blank.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/concept_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/copyright.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/corp_timestamp.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/delta.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/deltaend.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/diagnostic3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/docscd.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/external_link.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/feedback_r.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/g_postcard.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/g_postcard_different.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/g_postcard_oneqm.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/g_postcard_signon.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/g_postcard_twoqm.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/g_postcard_workings.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/general_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/helpbanner2.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/i_choices.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/i_comp_wiz.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/i_conf_win2000.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/i_config.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/i_def_conf.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/i_def_conf_wiz.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/i_folders.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/i_join_def_clus.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/i_launch.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/i_listener.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/i_local_repos.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/i_net_prereq.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/i_prepare_wiz.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/i_qmgr_conf.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/i_rem_repos.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/i_remote_admin.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/i_setup_type.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/i_soft_prereq.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/i_uninst.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/i_wsmq.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/ibm-logo-white.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/ibmdita.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/ng2000.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/ngaix.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/nghpux.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/nglinux.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/ngnt.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/ngsolaris.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/ngunix.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/ngwin.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/ngxp.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/ngzos.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/problem3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/query3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/reference_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/sample_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/start_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/swg_info_common.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/task_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/timestamp.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/tip3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/tutorial_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/warning3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/websphere-brandmark.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/wizard_help.xml"
%attr(444,mqm,mqm) "/opt/mqm/doc/pl_pl/workbench_link.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/ax99995_.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/banner.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/bearings3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/bip4.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/blank.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/concept_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/copyright.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/corp_timestamp.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/delta.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/deltaend.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/diagnostic3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/docscd.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/external_link.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/feedback_r.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/g_postcard.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/g_postcard_different.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/g_postcard_oneqm.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/g_postcard_signon.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/g_postcard_twoqm.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/g_postcard_workings.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/general_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/helpbanner2.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/i_choices.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/i_comp_wiz.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/i_conf_win2000.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/i_config.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/i_def_conf.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/i_def_conf_wiz.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/i_folders.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/i_join_def_clus.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/i_launch.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/i_listener.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/i_local_repos.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/i_net_prereq.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/i_prepare_wiz.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/i_qmgr_conf.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/i_rem_repos.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/i_remote_admin.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/i_setup_type.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/i_soft_prereq.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/i_uninst.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/i_wsmq.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/ibm-logo-white.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/ibmdita.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/ng2000.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/ngaix.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/nghpux.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/nglinux.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/ngnt.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/ngsolaris.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/ngunix.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/ngwin.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/ngxp.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/ngzos.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/problem3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/query3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/reference_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/sample_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/start_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/swg_info_common.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/task_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/timestamp.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/tip3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/tutorial_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/warning3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/websphere-brandmark.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/wizard_help.xml"
%attr(444,mqm,mqm) "/opt/mqm/doc/pt_br/workbench_link.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/ax99995_.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/banner.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/bearings3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/bip4.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/blank.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/concept_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/copyright.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/corp_timestamp.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/delta.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/deltaend.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/diagnostic3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/docscd.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/external_link.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/feedback_r.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/g_postcard.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/g_postcard_different.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/g_postcard_oneqm.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/g_postcard_signon.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/g_postcard_twoqm.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/g_postcard_workings.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/general_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/helpbanner2.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/i_choices.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/i_comp_wiz.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/i_conf_win2000.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/i_config.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/i_def_conf.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/i_def_conf_wiz.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/i_folders.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/i_join_def_clus.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/i_launch.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/i_listener.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/i_local_repos.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/i_net_prereq.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/i_prepare_wiz.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/i_qmgr_conf.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/i_rem_repos.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/i_remote_admin.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/i_setup_type.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/i_soft_prereq.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/i_uninst.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/i_wsmq.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/ibm-logo-white.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/ibmdita.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/ng2000.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/ngaix.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/nghpux.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/nglinux.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/ngnt.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/ngsolaris.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/ngunix.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/ngwin.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/ngxp.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/ngzos.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/problem3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/query3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/reference_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/sample_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/start_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/swg_info_common.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/task_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/timestamp.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/tip3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/tutorial_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/warning3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/websphere-brandmark.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/wizard_help.xml"
%attr(444,mqm,mqm) "/opt/mqm/doc/ru_ru/workbench_link.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/ax99995_.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/banner.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/bearings3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/bip4.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/blank.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/concept_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/copyright.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/corp_timestamp.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/delta.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/deltaend.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/diagnostic3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/docscd.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/external_link.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/feedback_r.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/g_postcard.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/g_postcard_different.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/g_postcard_oneqm.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/g_postcard_signon.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/g_postcard_twoqm.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/g_postcard_workings.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/general_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/helpbanner2.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/i_choices.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/i_comp_wiz.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/i_conf_win2000.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/i_config.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/i_def_conf.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/i_def_conf_wiz.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/i_folders.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/i_join_def_clus.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/i_launch.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/i_listener.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/i_local_repos.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/i_net_prereq.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/i_prepare_wiz.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/i_qmgr_conf.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/i_rem_repos.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/i_remote_admin.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/i_setup_type.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/i_soft_prereq.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/i_uninst.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/i_wsmq.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/ibm-logo-white.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/ibmdita.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/ng2000.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/ngaix.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/nghpux.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/nglinux.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/ngnt.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/ngsolaris.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/ngunix.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/ngwin.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/ngxp.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/ngzos.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/problem3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/query3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/reference_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/sample_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/start_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/swg_info_common.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/task_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/timestamp.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/tip3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/tutorial_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/warning3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/websphere-brandmark.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/wizard_help.xml"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_cn/workbench_link.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/ax99995_.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/banner.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/bearings3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/bip4.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/blank.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/concept_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/copyright.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/corp_timestamp.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/delta.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/deltaend.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/diagnostic3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/docscd.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/external_link.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/feedback_r.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/g_postcard.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/g_postcard_different.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/g_postcard_oneqm.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/g_postcard_signon.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/g_postcard_twoqm.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/g_postcard_workings.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/general_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/helpbanner2.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/i_choices.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/i_comp_wiz.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/i_conf_win2000.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/i_config.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/i_def_conf.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/i_def_conf_wiz.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/i_folders.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/i_join_def_clus.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/i_launch.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/i_listener.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/i_local_repos.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/i_net_prereq.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/i_prepare_wiz.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/i_qmgr_conf.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/i_rem_repos.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/i_remote_admin.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/i_setup_type.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/i_soft_prereq.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/i_uninst.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/i_wsmq.htm"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/ibm-logo-white.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/ibmdita.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/ng2000.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/ngaix.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/nghpux.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/nglinux.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/ngnt.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/ngsolaris.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/ngunix.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/ngwin.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/ngxp.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/ngzos.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/problem3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/query3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/reference_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/sample_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/start_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/swg_info_common.css"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/task_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/timestamp.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/tip3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/tutorial_obj.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/warning3.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/websphere-brandmark.gif"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/wizard_help.xml"
%attr(444,mqm,mqm) "/opt/mqm/doc/zh_tw/workbench_link.gif"

%pre
RPM_PACKAGE_SUMMARY="WebSphere MQ Server FileSet"
RPM_PACKAGE_NAME="MQSeriesServer"
RPM_PACKAGE_VERSION="7.5.0"
RPM_PACKAGE_RELEASE="2"
PACKAGEPLATFORMS="x86_64"
MQ_DEFAULT_INSTALLATION_PATH=/opt/mqm
MQ_INSTALLATION_PATH=${RPM_INSTALL_PREFIX}
MQ_RUNSCRIPT=
echo ${SHELLOPTS} | grep xtrace > /dev/null
if [ $? -eq 0 ] ; then 
  env | sort
  uname -a
  id
  MQ_RUNSCRIPT="sh -x"
fi

# @(#) MQMBID sn=p750-002-130704 su=_UmspIOSZEeK1oKoKL_dPJA pn=install/unix/linux_2/preinstall.sh
#   <copyright 
#   notice="lm-source-program" 
#   pids="5724-H72," 
#   years="2005,2012" 
#   crc="2186337890" > 
#  Licensed Materials - Property of IBM  
#   
#  5724-H72, 
#   
#  (C) Copyright IBM Corp. 2005, 2012 All Rights Reserved.  
#   
#  US Government Users Restricted Rights - Use, duplication or  
#  disclosure restricted by GSA ADP Schedule Contract with  
#  IBM Corp.  
#   </copyright> 
# Preinstallation script
# Checks to see if the mqm userid and groups exists, and if now will create
# 
# Check that this package is not being installed to a location where 
# a different VR exists
# 

# Set up environment variables
TMPDIR="/tmp"
RC=0
DUMMY=""

create_mqm_group()
{
  echo "Creating group mqm"
  ErrorText=`groupadd mqm 2>&1`
  RC=$?
  if [ $RC -ne 0 ]; then
    echo "ERROR:  Failed to add 'mqm' group:" >&2
    echo $ErrorText >&2
    exit $RC
  fi
}
# Determine if the group exists on the system.  The solution used here is to
# create a file in the temporary location, attempt to change its group id to
# mqm.  This method should work independent of the multitude of ways groups can
# be defined across distributions and network environments
TMPFILE="${TMPDIR}/amq_grouptest"
touch $TMPFILE 2>/dev/null
if [ -f "$TMPFILE" ]; then
  chown :mqm $TMPFILE 2>/dev/null
  if [ $? -ne 0 ]; then
    create_mqm_group
  fi
  rm -f $TMPFILE
else  # Unable to create temporary file.  This is also needed by other install
      # processes - so it is fair to abort the install at this point
  echo "ERROR: Unable to write to ${TMPDIR} - aborting install"
  exit 1
fi

# Determine if the user id "mqm" exists, and is in the group mqm
ErrorText=`id -u mqm 2>&1`
RC=$?
if [ $RC -ne 0 ]; then
  echo "Creating user mqm"
  ErrorText=`useradd -r -d /var/mqm -g mqm mqm 2>&1`
  RC=$?
  if [ $RC -ne 0 ]; then
    echo "ERROR:  Failed to add 'mqm' user:" >&2
    echo $ErrorText >&2
    exit $RC
  fi
else    # Check the user mqm is in the group mqm
  ErrorText=`id -Gn mqm | grep -w mqm 2>&1`
  if [ $? -ne 0 ]; then
    ErrorText=`usermod -G mqm mqm 2>&1`
    if [ $RC -ne 0 ]; then
      echo "ERROR:  Failed to add user mqm to group mqm:" >&2
      echo $ErrorText >&1
      echo $RC
    fi
  fi
fi

#######################################################################################################
# Check the install path does not exceed the Websphere MQ maximum length of 256
#######################################################################################################
if [ ${#MQ_INSTALLATION_PATH} -gt 256 ]; then
  echo ""
  echo "ERROR:   Specified path exceeds WebSphere MQ maximum length of 256"
  echo ""
  exit 1
fi

#######################################################################################################
# Check the install path does not contain unsupported charaters
#######################################################################################################
echo ${MQ_INSTALLATION_PATH} | grep "[:%#]" > /dev/null
if [ $? -eq 0 ] ; then
  echo ""
  echo "ERROR:   Specified path contains an unsupported character"
  echo ""
  exit 1
fi

#######################################################################################################
# Runtime checks
#######################################################################################################
echo ${RPM_PACKAGE_NAME} | grep  "MQSeriesRuntime" > /dev/null
if [ $? -eq 0 ] ; then
  #####################################################################################################
  # Check that the install path is empty
  #####################################################################################################
  if [ -d ${MQ_INSTALLATION_PATH} ] ; then
    LS_ALL=`ls -A ${MQ_INSTALLATION_PATH}`
    if [ "${LS_ALL}" ] && [ "${LS_ALL}" != "lost+found" ] ; then
      echo ""
      echo "ERROR:   Specified path is not empty"
      echo ""
      exit 1
    fi
  fi
#######################################################################################################
# Non Runtime checks
#######################################################################################################
else
  #####################################################################################################
  # Check the version/release of the product already at MQ_INSTALLATION_PATH is the same as this one
  #####################################################################################################
  if [ -x ${MQ_INSTALLATION_PATH}/bin/dspmqver ] ; then
    INSTALLED_VR=$(${MQ_INSTALLATION_PATH}/bin/dspmqver -f2 -b | awk -F. '{print $1 "." $2}')
    PACKAGE_VR=`echo ${RPM_PACKAGE_VERSION} | awk -F. '{print $1 "." $2}'`
    if [ ${INSTALLED_VR} != ${PACKAGE_VR} ] ; then
      echo ""
      echo "ERROR:   This package is not applicable to the WebSphere MQ installation at ${MQ_INSTALLATION_PATH}"
      echo ""
      exit 1
    fi
  else
    echo ""
    echo "ERROR:   There is no MQSeriesRuntime installed at ${MQ_INSTALLATION_PATH}"
    echo ""
    exit 1
  fi
fi

#   <copyright 
#   notice="lm-source-program" 
#   pids="5724-H72," 
#   years="2005,2012" 
#   crc="1114153681" > 
#  Licensed Materials - Property of IBM  
#   
#  5724-H72, 
#   
#  (C) Copyright IBM Corp. 2005, 2012 All Rights Reserved.  
#   
#  US Government Users Restricted Rights - Use, duplication or  
#  disclosure restricted by GSA ADP Schedule Contract with  
#  IBM Corp.  
#   </copyright> 
# Preinstallation script
# Check's to see if the license agreement has been accepted

if [ ! -r /tmp/mq_license_${RPM_PACKAGE_VERSION}/license/status.dat ] && [ ! -r ${MQ_INSTALLATION_PATH}/licenses/status.dat ] ; then

    cat << +++EOM+++
ERROR:  Product cannot be installed until the license
        agreement has been accepted.
        Run the 'mqlicense' script, which is in the root
        directory of the install media, or see the
        installation instructions in the product 
        Infocenter for more information
+++EOM+++

   exit 1
fi

# @(#) MQMBID sn=p750-002-130704 su=_UmspIOSZEeK1oKoKL_dPJA pn=install/unix/linux_2/server_preinstall.sh

INSTALLROOT=${MQ_INSTALLATION_PATH}

#  Server preinstall script
#  ------------------------------
#   <copyright 
#   notice="lm-source-program" 
#   pids="5724-H72," 
#   years="2005,2012" 
#   crc="767503690" > 
#  Licensed Materials - Property of IBM  
#   
#  5724-H72, 
#   
#  (C) Copyright IBM Corp. 2005, 2012 All Rights Reserved.  
#   
#  US Government Users Restricted Rights - Use, duplication or  
#  disclosure restricted by GSA ADP Schedule Contract with  
#  IBM Corp.  
#   </copyright> 

# If the file "${MQ_INSTALLATION_PATH}/lib/amqpcert.lic" exists and is a link, it should be
# deleted as is it not desirable to have two license files.  Note that the
# license file installed by the server component might be of the same name
# (in which case the symlink would just be overwritten), however it might have
# an alternate name which would be the trial or beta license.
if [ -h "${MQ_INSTALLATION_PATH}/lib/amqpcert.lic" ]; then
  rm -f "${MQ_INSTALLATION_PATH}/lib/amqpcert.lic"
fi
echo > /dev/null 2>/dev/null

%post
RPM_PACKAGE_SUMMARY="WebSphere MQ Server FileSet"
RPM_PACKAGE_NAME="MQSeriesServer"
RPM_PACKAGE_VERSION="7.5.0"
RPM_PACKAGE_RELEASE="2"
PACKAGEPLATFORMS="x86_64"
MQ_DEFAULT_INSTALLATION_PATH=/opt/mqm
MQ_INSTALLATION_PATH=${RPM_INSTALL_PREFIX}
MQ_RUNSCRIPT=
echo ${SHELLOPTS} | grep xtrace > /dev/null
if [ $? -eq 0 ] ; then 
  env | sort
  uname -a
  id
  MQ_RUNSCRIPT="sh -x"
fi
if [ ${MQ_INSTALLATION_PATH} !=  ${MQ_DEFAULT_INSTALLATION_PATH} ] ; then 
  if [ -x ${MQ_INSTALLATION_PATH}/bin/amqicrel ] ; then 
     ${MQ_RUNSCRIPT} ${MQ_INSTALLATION_PATH}/bin/amqicrel ${MQ_INSTALLATION_PATH} ${RPM_PACKAGE_NAME}-${RPM_PACKAGE_VERSION}-${RPM_PACKAGE_RELEASE}
  fi
fi
# Linking ${MQ_INSTALLATION_PATH}/bin/runmqbrk to ${MQ_INSTALLATION_PATH}/bin/strmqbrk
if [ ! -d ${MQ_INSTALLATION_PATH}/bin ]
then
    mkdir -p ${MQ_INSTALLATION_PATH}/bin
else
    rm -f ${MQ_INSTALLATION_PATH}/bin/strmqbrk
fi
ln -s ${MQ_INSTALLATION_PATH}/bin/runmqbrk ${MQ_INSTALLATION_PATH}/bin/strmqbrk
chown -fh mqm ${MQ_INSTALLATION_PATH}/bin/strmqbrk
chgrp -fh mqm ${MQ_INSTALLATION_PATH}/bin/strmqbrk
# Linking ${MQ_INSTALLATION_PATH}/lib64/amqzfu to ${MQ_INSTALLATION_PATH}/lib/amqzfu
if [ ! -d ${MQ_INSTALLATION_PATH}/lib ]
then
    mkdir -p ${MQ_INSTALLATION_PATH}/lib
else
    rm -f ${MQ_INSTALLATION_PATH}/lib/amqzfu
fi
ln -s ${MQ_INSTALLATION_PATH}/lib64/amqzfu ${MQ_INSTALLATION_PATH}/lib/amqzfu
chown -fh mqm ${MQ_INSTALLATION_PATH}/lib/amqzfu
chgrp -fh mqm ${MQ_INSTALLATION_PATH}/lib/amqzfu
# Linking ${MQ_INSTALLATION_PATH}/lib/libmqmzf.so to ${MQ_INSTALLATION_PATH}/lib/compat/libmqmzf.so
if [ ! -d ${MQ_INSTALLATION_PATH}/lib/compat ]
then
    mkdir -p ${MQ_INSTALLATION_PATH}/lib/compat
else
    rm -f ${MQ_INSTALLATION_PATH}/lib/compat/libmqmzf.so
fi
ln -s ${MQ_INSTALLATION_PATH}/lib/libmqmzf.so ${MQ_INSTALLATION_PATH}/lib/compat/libmqmzf.so
chown -fh mqm ${MQ_INSTALLATION_PATH}/lib/compat/libmqmzf.so
chgrp -fh mqm ${MQ_INSTALLATION_PATH}/lib/compat/libmqmzf.so
# Linking ${MQ_INSTALLATION_PATH}/lib64/libmqmzf.so to ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmzf.so
if [ ! -d ${MQ_INSTALLATION_PATH}/lib64/compat ]
then
    mkdir -p ${MQ_INSTALLATION_PATH}/lib64/compat
else
    rm -f ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmzf.so
fi
ln -s ${MQ_INSTALLATION_PATH}/lib64/libmqmzf.so ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmzf.so
chown -fh mqm ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmzf.so
chgrp -fh mqm ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmzf.so
# Linking ${MQ_INSTALLATION_PATH}/lib/libmqmzf_r.so to ${MQ_INSTALLATION_PATH}/lib/compat/libmqmzf_r.so
if [ ! -d ${MQ_INSTALLATION_PATH}/lib/compat ]
then
    mkdir -p ${MQ_INSTALLATION_PATH}/lib/compat
else
    rm -f ${MQ_INSTALLATION_PATH}/lib/compat/libmqmzf_r.so
fi
ln -s ${MQ_INSTALLATION_PATH}/lib/libmqmzf_r.so ${MQ_INSTALLATION_PATH}/lib/compat/libmqmzf_r.so
chown -fh mqm ${MQ_INSTALLATION_PATH}/lib/compat/libmqmzf_r.so
chgrp -fh mqm ${MQ_INSTALLATION_PATH}/lib/compat/libmqmzf_r.so
# Linking ${MQ_INSTALLATION_PATH}/lib64/libmqmzf_r.so to ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmzf_r.so
if [ ! -d ${MQ_INSTALLATION_PATH}/lib64/compat ]
then
    mkdir -p ${MQ_INSTALLATION_PATH}/lib64/compat
else
    rm -f ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmzf_r.so
fi
ln -s ${MQ_INSTALLATION_PATH}/lib64/libmqmzf_r.so ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmzf_r.so
chown -fh mqm ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmzf_r.so
chgrp -fh mqm ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmzf_r.so
# Linking ${MQ_INSTALLATION_PATH}/lib/libmqutl.so to ${MQ_INSTALLATION_PATH}/lib/compat/libmqutl.so
if [ ! -d ${MQ_INSTALLATION_PATH}/lib/compat ]
then
    mkdir -p ${MQ_INSTALLATION_PATH}/lib/compat
else
    rm -f ${MQ_INSTALLATION_PATH}/lib/compat/libmqutl.so
fi
ln -s ${MQ_INSTALLATION_PATH}/lib/libmqutl.so ${MQ_INSTALLATION_PATH}/lib/compat/libmqutl.so
chown -fh mqm ${MQ_INSTALLATION_PATH}/lib/compat/libmqutl.so
chgrp -fh mqm ${MQ_INSTALLATION_PATH}/lib/compat/libmqutl.so
# Linking ${MQ_INSTALLATION_PATH}/lib64/libmqutl.so to ${MQ_INSTALLATION_PATH}/lib64/compat/libmqutl.so
if [ ! -d ${MQ_INSTALLATION_PATH}/lib64/compat ]
then
    mkdir -p ${MQ_INSTALLATION_PATH}/lib64/compat
else
    rm -f ${MQ_INSTALLATION_PATH}/lib64/compat/libmqutl.so
fi
ln -s ${MQ_INSTALLATION_PATH}/lib64/libmqutl.so ${MQ_INSTALLATION_PATH}/lib64/compat/libmqutl.so
chown -fh mqm ${MQ_INSTALLATION_PATH}/lib64/compat/libmqutl.so
chgrp -fh mqm ${MQ_INSTALLATION_PATH}/lib64/compat/libmqutl.so
# Linking ${MQ_INSTALLATION_PATH}/lib/libmqutl_r.so to ${MQ_INSTALLATION_PATH}/lib/compat/libmqutl_r.so
if [ ! -d ${MQ_INSTALLATION_PATH}/lib/compat ]
then
    mkdir -p ${MQ_INSTALLATION_PATH}/lib/compat
else
    rm -f ${MQ_INSTALLATION_PATH}/lib/compat/libmqutl_r.so
fi
ln -s ${MQ_INSTALLATION_PATH}/lib/libmqutl_r.so ${MQ_INSTALLATION_PATH}/lib/compat/libmqutl_r.so
chown -fh mqm ${MQ_INSTALLATION_PATH}/lib/compat/libmqutl_r.so
chgrp -fh mqm ${MQ_INSTALLATION_PATH}/lib/compat/libmqutl_r.so
# Linking ${MQ_INSTALLATION_PATH}/lib64/libmqutl_r.so to ${MQ_INSTALLATION_PATH}/lib64/compat/libmqutl_r.so
if [ ! -d ${MQ_INSTALLATION_PATH}/lib64/compat ]
then
    mkdir -p ${MQ_INSTALLATION_PATH}/lib64/compat
else
    rm -f ${MQ_INSTALLATION_PATH}/lib64/compat/libmqutl_r.so
fi
ln -s ${MQ_INSTALLATION_PATH}/lib64/libmqutl_r.so ${MQ_INSTALLATION_PATH}/lib64/compat/libmqutl_r.so
chown -fh mqm ${MQ_INSTALLATION_PATH}/lib64/compat/libmqutl_r.so
chgrp -fh mqm ${MQ_INSTALLATION_PATH}/lib64/compat/libmqutl_r.so
# Linking ${MQ_INSTALLATION_PATH}/lib/libmqmcb.so to ${MQ_INSTALLATION_PATH}/lib/compat/libmqmcb.so
if [ ! -d ${MQ_INSTALLATION_PATH}/lib/compat ]
then
    mkdir -p ${MQ_INSTALLATION_PATH}/lib/compat
else
    rm -f ${MQ_INSTALLATION_PATH}/lib/compat/libmqmcb.so
fi
ln -s ${MQ_INSTALLATION_PATH}/lib/libmqmcb.so ${MQ_INSTALLATION_PATH}/lib/compat/libmqmcb.so
chown -fh mqm ${MQ_INSTALLATION_PATH}/lib/compat/libmqmcb.so
chgrp -fh mqm ${MQ_INSTALLATION_PATH}/lib/compat/libmqmcb.so
# Linking ${MQ_INSTALLATION_PATH}/lib64/libmqmcb.so to ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmcb.so
if [ ! -d ${MQ_INSTALLATION_PATH}/lib64/compat ]
then
    mkdir -p ${MQ_INSTALLATION_PATH}/lib64/compat
else
    rm -f ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmcb.so
fi
ln -s ${MQ_INSTALLATION_PATH}/lib64/libmqmcb.so ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmcb.so
chown -fh mqm ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmcb.so
chgrp -fh mqm ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmcb.so
# Linking ${MQ_INSTALLATION_PATH}/lib/libmqmcb_r.so to ${MQ_INSTALLATION_PATH}/lib/compat/libmqmcb_r.so
if [ ! -d ${MQ_INSTALLATION_PATH}/lib/compat ]
then
    mkdir -p ${MQ_INSTALLATION_PATH}/lib/compat
else
    rm -f ${MQ_INSTALLATION_PATH}/lib/compat/libmqmcb_r.so
fi
ln -s ${MQ_INSTALLATION_PATH}/lib/libmqmcb_r.so ${MQ_INSTALLATION_PATH}/lib/compat/libmqmcb_r.so
chown -fh mqm ${MQ_INSTALLATION_PATH}/lib/compat/libmqmcb_r.so
chgrp -fh mqm ${MQ_INSTALLATION_PATH}/lib/compat/libmqmcb_r.so
# Linking ${MQ_INSTALLATION_PATH}/lib64/libmqmcb_r.so to ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmcb_r.so
if [ ! -d ${MQ_INSTALLATION_PATH}/lib64/compat ]
then
    mkdir -p ${MQ_INSTALLATION_PATH}/lib64/compat
else
    rm -f ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmcb_r.so
fi
ln -s ${MQ_INSTALLATION_PATH}/lib64/libmqmcb_r.so ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmcb_r.so
chown -fh mqm ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmcb_r.so
chgrp -fh mqm ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmcb_r.so
# Linking ${MQ_INSTALLATION_PATH}/lib/libmqmxa.so to ${MQ_INSTALLATION_PATH}/lib/compat/libmqmxa.so
if [ ! -d ${MQ_INSTALLATION_PATH}/lib/compat ]
then
    mkdir -p ${MQ_INSTALLATION_PATH}/lib/compat
else
    rm -f ${MQ_INSTALLATION_PATH}/lib/compat/libmqmxa.so
fi
ln -s ${MQ_INSTALLATION_PATH}/lib/libmqmxa.so ${MQ_INSTALLATION_PATH}/lib/compat/libmqmxa.so
chown -fh mqm ${MQ_INSTALLATION_PATH}/lib/compat/libmqmxa.so
chgrp -fh mqm ${MQ_INSTALLATION_PATH}/lib/compat/libmqmxa.so
# Linking ${MQ_INSTALLATION_PATH}/lib64/libmqmxa.so to ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmxa.so
if [ ! -d ${MQ_INSTALLATION_PATH}/lib64/compat ]
then
    mkdir -p ${MQ_INSTALLATION_PATH}/lib64/compat
else
    rm -f ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmxa.so
fi
ln -s ${MQ_INSTALLATION_PATH}/lib64/libmqmxa.so ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmxa.so
chown -fh mqm ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmxa.so
chgrp -fh mqm ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmxa.so
# Linking ${MQ_INSTALLATION_PATH}/lib64/libmqmxa64.so to ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmxa64.so
if [ ! -d ${MQ_INSTALLATION_PATH}/lib64/compat ]
then
    mkdir -p ${MQ_INSTALLATION_PATH}/lib64/compat
else
    rm -f ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmxa64.so
fi
ln -s ${MQ_INSTALLATION_PATH}/lib64/libmqmxa64.so ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmxa64.so
chown -fh mqm ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmxa64.so
chgrp -fh mqm ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmxa64.so
# Linking ${MQ_INSTALLATION_PATH}/lib/libmqmxa_r.so to ${MQ_INSTALLATION_PATH}/lib/compat/libmqmxa_r.so
if [ ! -d ${MQ_INSTALLATION_PATH}/lib/compat ]
then
    mkdir -p ${MQ_INSTALLATION_PATH}/lib/compat
else
    rm -f ${MQ_INSTALLATION_PATH}/lib/compat/libmqmxa_r.so
fi
ln -s ${MQ_INSTALLATION_PATH}/lib/libmqmxa_r.so ${MQ_INSTALLATION_PATH}/lib/compat/libmqmxa_r.so
chown -fh mqm ${MQ_INSTALLATION_PATH}/lib/compat/libmqmxa_r.so
chgrp -fh mqm ${MQ_INSTALLATION_PATH}/lib/compat/libmqmxa_r.so
# Linking ${MQ_INSTALLATION_PATH}/lib64/libmqmxa_r.so to ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmxa_r.so
if [ ! -d ${MQ_INSTALLATION_PATH}/lib64/compat ]
then
    mkdir -p ${MQ_INSTALLATION_PATH}/lib64/compat
else
    rm -f ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmxa_r.so
fi
ln -s ${MQ_INSTALLATION_PATH}/lib64/libmqmxa_r.so ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmxa_r.so
chown -fh mqm ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmxa_r.so
chgrp -fh mqm ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmxa_r.so
# Linking ${MQ_INSTALLATION_PATH}/lib64/libmqmxa64_r.so to ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmxa64_r.so
if [ ! -d ${MQ_INSTALLATION_PATH}/lib64/compat ]
then
    mkdir -p ${MQ_INSTALLATION_PATH}/lib64/compat
else
    rm -f ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmxa64_r.so
fi
ln -s ${MQ_INSTALLATION_PATH}/lib64/libmqmxa64_r.so ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmxa64_r.so
chown -fh mqm ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmxa64_r.so
chgrp -fh mqm ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmxa64_r.so
# Linking ${MQ_INSTALLATION_PATH}/lib/libmqmax.so to ${MQ_INSTALLATION_PATH}/lib/compat/libmqmax.so
if [ ! -d ${MQ_INSTALLATION_PATH}/lib/compat ]
then
    mkdir -p ${MQ_INSTALLATION_PATH}/lib/compat
else
    rm -f ${MQ_INSTALLATION_PATH}/lib/compat/libmqmax.so
fi
ln -s ${MQ_INSTALLATION_PATH}/lib/libmqmax.so ${MQ_INSTALLATION_PATH}/lib/compat/libmqmax.so
chown -fh mqm ${MQ_INSTALLATION_PATH}/lib/compat/libmqmax.so
chgrp -fh mqm ${MQ_INSTALLATION_PATH}/lib/compat/libmqmax.so
# Linking ${MQ_INSTALLATION_PATH}/lib64/libmqmax.so to ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmax.so
if [ ! -d ${MQ_INSTALLATION_PATH}/lib64/compat ]
then
    mkdir -p ${MQ_INSTALLATION_PATH}/lib64/compat
else
    rm -f ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmax.so
fi
ln -s ${MQ_INSTALLATION_PATH}/lib64/libmqmax.so ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmax.so
chown -fh mqm ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmax.so
chgrp -fh mqm ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmax.so
# Linking ${MQ_INSTALLATION_PATH}/lib/libmqmax_r.so to ${MQ_INSTALLATION_PATH}/lib/compat/libmqmax_r.so
if [ ! -d ${MQ_INSTALLATION_PATH}/lib/compat ]
then
    mkdir -p ${MQ_INSTALLATION_PATH}/lib/compat
else
    rm -f ${MQ_INSTALLATION_PATH}/lib/compat/libmqmax_r.so
fi
ln -s ${MQ_INSTALLATION_PATH}/lib/libmqmax_r.so ${MQ_INSTALLATION_PATH}/lib/compat/libmqmax_r.so
chown -fh mqm ${MQ_INSTALLATION_PATH}/lib/compat/libmqmax_r.so
chgrp -fh mqm ${MQ_INSTALLATION_PATH}/lib/compat/libmqmax_r.so
# Linking ${MQ_INSTALLATION_PATH}/lib64/libmqmax_r.so to ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmax_r.so
if [ ! -d ${MQ_INSTALLATION_PATH}/lib64/compat ]
then
    mkdir -p ${MQ_INSTALLATION_PATH}/lib64/compat
else
    rm -f ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmax_r.so
fi
ln -s ${MQ_INSTALLATION_PATH}/lib64/libmqmax_r.so ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmax_r.so
chown -fh mqm ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmax_r.so
chgrp -fh mqm ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmax_r.so
# Linking ${MQ_INSTALLATION_PATH}/lib64/libmqmax64.so to ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmax64.so
if [ ! -d ${MQ_INSTALLATION_PATH}/lib64/compat ]
then
    mkdir -p ${MQ_INSTALLATION_PATH}/lib64/compat
else
    rm -f ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmax64.so
fi
ln -s ${MQ_INSTALLATION_PATH}/lib64/libmqmax64.so ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmax64.so
chown -fh mqm ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmax64.so
chgrp -fh mqm ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmax64.so
# Linking ${MQ_INSTALLATION_PATH}/lib64/libmqmax64_r.so to ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmax64_r.so
if [ ! -d ${MQ_INSTALLATION_PATH}/lib64/compat ]
then
    mkdir -p ${MQ_INSTALLATION_PATH}/lib64/compat
else
    rm -f ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmax64_r.so
fi
ln -s ${MQ_INSTALLATION_PATH}/lib64/libmqmax64_r.so ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmax64_r.so
chown -fh mqm ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmax64_r.so
chgrp -fh mqm ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmax64_r.so
# Linking ${MQ_INSTALLATION_PATH}/lib/4.1/libimqs23gl.so to ${MQ_INSTALLATION_PATH}/lib/libimqs23gl.so
if [ ! -d ${MQ_INSTALLATION_PATH}/lib ]
then
    mkdir -p ${MQ_INSTALLATION_PATH}/lib
else
    rm -f ${MQ_INSTALLATION_PATH}/lib/libimqs23gl.so
fi
ln -s ${MQ_INSTALLATION_PATH}/lib/4.1/libimqs23gl.so ${MQ_INSTALLATION_PATH}/lib/libimqs23gl.so
chown -fh mqm ${MQ_INSTALLATION_PATH}/lib/libimqs23gl.so
chgrp -fh mqm ${MQ_INSTALLATION_PATH}/lib/libimqs23gl.so
# Linking ${MQ_INSTALLATION_PATH}/lib/4.1/libimqs23gl_r.so to ${MQ_INSTALLATION_PATH}/lib/libimqs23gl_r.so
if [ ! -d ${MQ_INSTALLATION_PATH}/lib ]
then
    mkdir -p ${MQ_INSTALLATION_PATH}/lib
else
    rm -f ${MQ_INSTALLATION_PATH}/lib/libimqs23gl_r.so
fi
ln -s ${MQ_INSTALLATION_PATH}/lib/4.1/libimqs23gl_r.so ${MQ_INSTALLATION_PATH}/lib/libimqs23gl_r.so
chown -fh mqm ${MQ_INSTALLATION_PATH}/lib/libimqs23gl_r.so
chgrp -fh mqm ${MQ_INSTALLATION_PATH}/lib/libimqs23gl_r.so
# Linking ${MQ_INSTALLATION_PATH}/lib64/4.1/libimqs23gl.so to ${MQ_INSTALLATION_PATH}/lib64/libimqs23gl.so
if [ ! -d ${MQ_INSTALLATION_PATH}/lib64 ]
then
    mkdir -p ${MQ_INSTALLATION_PATH}/lib64
else
    rm -f ${MQ_INSTALLATION_PATH}/lib64/libimqs23gl.so
fi
ln -s ${MQ_INSTALLATION_PATH}/lib64/4.1/libimqs23gl.so ${MQ_INSTALLATION_PATH}/lib64/libimqs23gl.so
chown -fh mqm ${MQ_INSTALLATION_PATH}/lib64/libimqs23gl.so
chgrp -fh mqm ${MQ_INSTALLATION_PATH}/lib64/libimqs23gl.so
# Linking ${MQ_INSTALLATION_PATH}/lib64/4.1/libimqs23gl_r.so to ${MQ_INSTALLATION_PATH}/lib64/libimqs23gl_r.so
if [ ! -d ${MQ_INSTALLATION_PATH}/lib64 ]
then
    mkdir -p ${MQ_INSTALLATION_PATH}/lib64
else
    rm -f ${MQ_INSTALLATION_PATH}/lib64/libimqs23gl_r.so
fi
ln -s ${MQ_INSTALLATION_PATH}/lib64/4.1/libimqs23gl_r.so ${MQ_INSTALLATION_PATH}/lib64/libimqs23gl_r.so
chown -fh mqm ${MQ_INSTALLATION_PATH}/lib64/libimqs23gl_r.so
chgrp -fh mqm ${MQ_INSTALLATION_PATH}/lib64/libimqs23gl_r.so
# Linking ${MQ_INSTALLATION_PATH}/lib/libimqs23gl.so to ${MQ_INSTALLATION_PATH}/lib/compat/libimqs23gl.so
if [ ! -d ${MQ_INSTALLATION_PATH}/lib/compat ]
then
    mkdir -p ${MQ_INSTALLATION_PATH}/lib/compat
else
    rm -f ${MQ_INSTALLATION_PATH}/lib/compat/libimqs23gl.so
fi
ln -s ${MQ_INSTALLATION_PATH}/lib/libimqs23gl.so ${MQ_INSTALLATION_PATH}/lib/compat/libimqs23gl.so
chown -fh mqm ${MQ_INSTALLATION_PATH}/lib/compat/libimqs23gl.so
chgrp -fh mqm ${MQ_INSTALLATION_PATH}/lib/compat/libimqs23gl.so
# Linking ${MQ_INSTALLATION_PATH}/lib/libimqs23gl_r.so to ${MQ_INSTALLATION_PATH}/lib/compat/libimqs23gl_r.so
if [ ! -d ${MQ_INSTALLATION_PATH}/lib/compat ]
then
    mkdir -p ${MQ_INSTALLATION_PATH}/lib/compat
else
    rm -f ${MQ_INSTALLATION_PATH}/lib/compat/libimqs23gl_r.so
fi
ln -s ${MQ_INSTALLATION_PATH}/lib/libimqs23gl_r.so ${MQ_INSTALLATION_PATH}/lib/compat/libimqs23gl_r.so
chown -fh mqm ${MQ_INSTALLATION_PATH}/lib/compat/libimqs23gl_r.so
chgrp -fh mqm ${MQ_INSTALLATION_PATH}/lib/compat/libimqs23gl_r.so
# Linking ${MQ_INSTALLATION_PATH}/lib64/libimqs23gl.so to ${MQ_INSTALLATION_PATH}/lib64/compat/libimqs23gl.so
if [ ! -d ${MQ_INSTALLATION_PATH}/lib64/compat ]
then
    mkdir -p ${MQ_INSTALLATION_PATH}/lib64/compat
else
    rm -f ${MQ_INSTALLATION_PATH}/lib64/compat/libimqs23gl.so
fi
ln -s ${MQ_INSTALLATION_PATH}/lib64/libimqs23gl.so ${MQ_INSTALLATION_PATH}/lib64/compat/libimqs23gl.so
chown -fh mqm ${MQ_INSTALLATION_PATH}/lib64/compat/libimqs23gl.so
chgrp -fh mqm ${MQ_INSTALLATION_PATH}/lib64/compat/libimqs23gl.so
# Linking ${MQ_INSTALLATION_PATH}/lib64/libimqs23gl_r.so to ${MQ_INSTALLATION_PATH}/lib64/compat/libimqs23gl_r.so
if [ ! -d ${MQ_INSTALLATION_PATH}/lib64/compat ]
then
    mkdir -p ${MQ_INSTALLATION_PATH}/lib64/compat
else
    rm -f ${MQ_INSTALLATION_PATH}/lib64/compat/libimqs23gl_r.so
fi
ln -s ${MQ_INSTALLATION_PATH}/lib64/libimqs23gl_r.so ${MQ_INSTALLATION_PATH}/lib64/compat/libimqs23gl_r.so
chown -fh mqm ${MQ_INSTALLATION_PATH}/lib64/compat/libimqs23gl_r.so
chgrp -fh mqm ${MQ_INSTALLATION_PATH}/lib64/compat/libimqs23gl_r.so
# Linking ${MQ_INSTALLATION_PATH}/lib64/libicudata.so.32.0 to ${MQ_INSTALLATION_PATH}/lib64/libicudata.so.32
if [ ! -d ${MQ_INSTALLATION_PATH}/lib64 ]
then
    mkdir -p ${MQ_INSTALLATION_PATH}/lib64
else
    rm -f ${MQ_INSTALLATION_PATH}/lib64/libicudata.so.32
fi
ln -s ${MQ_INSTALLATION_PATH}/lib64/libicudata.so.32.0 ${MQ_INSTALLATION_PATH}/lib64/libicudata.so.32
chown -fh mqm ${MQ_INSTALLATION_PATH}/lib64/libicudata.so.32
chgrp -fh mqm ${MQ_INSTALLATION_PATH}/lib64/libicudata.so.32
# Linking ${MQ_INSTALLATION_PATH}/lib64/libicudata.so.32.0 to ${MQ_INSTALLATION_PATH}/lib64/libicudata.so
if [ ! -d ${MQ_INSTALLATION_PATH}/lib64 ]
then
    mkdir -p ${MQ_INSTALLATION_PATH}/lib64
else
    rm -f ${MQ_INSTALLATION_PATH}/lib64/libicudata.so
fi
ln -s ${MQ_INSTALLATION_PATH}/lib64/libicudata.so.32.0 ${MQ_INSTALLATION_PATH}/lib64/libicudata.so
chown -fh mqm ${MQ_INSTALLATION_PATH}/lib64/libicudata.so
chgrp -fh mqm ${MQ_INSTALLATION_PATH}/lib64/libicudata.so
# Linking ${MQ_INSTALLATION_PATH}/lib64/libicuuc.so.32.0 to ${MQ_INSTALLATION_PATH}/lib64/libicuuc.so.32
if [ ! -d ${MQ_INSTALLATION_PATH}/lib64 ]
then
    mkdir -p ${MQ_INSTALLATION_PATH}/lib64
else
    rm -f ${MQ_INSTALLATION_PATH}/lib64/libicuuc.so.32
fi
ln -s ${MQ_INSTALLATION_PATH}/lib64/libicuuc.so.32.0 ${MQ_INSTALLATION_PATH}/lib64/libicuuc.so.32
chown -fh mqm ${MQ_INSTALLATION_PATH}/lib64/libicuuc.so.32
chgrp -fh mqm ${MQ_INSTALLATION_PATH}/lib64/libicuuc.so.32
# Linking ${MQ_INSTALLATION_PATH}/lib64/libicuuc.so.32.0 to ${MQ_INSTALLATION_PATH}/lib64/libicuuc.so
if [ ! -d ${MQ_INSTALLATION_PATH}/lib64 ]
then
    mkdir -p ${MQ_INSTALLATION_PATH}/lib64
else
    rm -f ${MQ_INSTALLATION_PATH}/lib64/libicuuc.so
fi
ln -s ${MQ_INSTALLATION_PATH}/lib64/libicuuc.so.32.0 ${MQ_INSTALLATION_PATH}/lib64/libicuuc.so
chown -fh mqm ${MQ_INSTALLATION_PATH}/lib64/libicuuc.so
chgrp -fh mqm ${MQ_INSTALLATION_PATH}/lib64/libicuuc.so
# Linking ${MQ_INSTALLATION_PATH}/lib64/libicui18n.so.32.0 to ${MQ_INSTALLATION_PATH}/lib64/libicui18n.so.32
if [ ! -d ${MQ_INSTALLATION_PATH}/lib64 ]
then
    mkdir -p ${MQ_INSTALLATION_PATH}/lib64
else
    rm -f ${MQ_INSTALLATION_PATH}/lib64/libicui18n.so.32
fi
ln -s ${MQ_INSTALLATION_PATH}/lib64/libicui18n.so.32.0 ${MQ_INSTALLATION_PATH}/lib64/libicui18n.so.32
chown -fh mqm ${MQ_INSTALLATION_PATH}/lib64/libicui18n.so.32
chgrp -fh mqm ${MQ_INSTALLATION_PATH}/lib64/libicui18n.so.32
# Linking ${MQ_INSTALLATION_PATH}/lib64/libicui18n.so.32.0 to ${MQ_INSTALLATION_PATH}/lib64/libicui18n.so
if [ ! -d ${MQ_INSTALLATION_PATH}/lib64 ]
then
    mkdir -p ${MQ_INSTALLATION_PATH}/lib64
else
    rm -f ${MQ_INSTALLATION_PATH}/lib64/libicui18n.so
fi
ln -s ${MQ_INSTALLATION_PATH}/lib64/libicui18n.so.32.0 ${MQ_INSTALLATION_PATH}/lib64/libicui18n.so
chown -fh mqm ${MQ_INSTALLATION_PATH}/lib64/libicui18n.so
chgrp -fh mqm ${MQ_INSTALLATION_PATH}/lib64/libicui18n.so
# Linking ${MQ_INSTALLATION_PATH}/lib64/libxml4c.so.56.3 to ${MQ_INSTALLATION_PATH}/lib64/libxml4c.so.56
if [ ! -d ${MQ_INSTALLATION_PATH}/lib64 ]
then
    mkdir -p ${MQ_INSTALLATION_PATH}/lib64
else
    rm -f ${MQ_INSTALLATION_PATH}/lib64/libxml4c.so.56
fi
ln -s ${MQ_INSTALLATION_PATH}/lib64/libxml4c.so.56.3 ${MQ_INSTALLATION_PATH}/lib64/libxml4c.so.56
chown -fh mqm ${MQ_INSTALLATION_PATH}/lib64/libxml4c.so.56
chgrp -fh mqm ${MQ_INSTALLATION_PATH}/lib64/libxml4c.so.56
# Linking ${MQ_INSTALLATION_PATH}/lib64/libxml4c.so.56.3 to ${MQ_INSTALLATION_PATH}/lib64/libxml4c.so
if [ ! -d ${MQ_INSTALLATION_PATH}/lib64 ]
then
    mkdir -p ${MQ_INSTALLATION_PATH}/lib64
else
    rm -f ${MQ_INSTALLATION_PATH}/lib64/libxml4c.so
fi
ln -s ${MQ_INSTALLATION_PATH}/lib64/libxml4c.so.56.3 ${MQ_INSTALLATION_PATH}/lib64/libxml4c.so
chown -fh mqm ${MQ_INSTALLATION_PATH}/lib64/libxml4c.so
chgrp -fh mqm ${MQ_INSTALLATION_PATH}/lib64/libxml4c.so
# Linking ${MQ_INSTALLATION_PATH}/lib64/libxml4c-depdom.so.56.3 to ${MQ_INSTALLATION_PATH}/lib64/libxml4c-depdom.so.56
if [ ! -d ${MQ_INSTALLATION_PATH}/lib64 ]
then
    mkdir -p ${MQ_INSTALLATION_PATH}/lib64
else
    rm -f ${MQ_INSTALLATION_PATH}/lib64/libxml4c-depdom.so.56
fi
ln -s ${MQ_INSTALLATION_PATH}/lib64/libxml4c-depdom.so.56.3 ${MQ_INSTALLATION_PATH}/lib64/libxml4c-depdom.so.56
chown -fh mqm ${MQ_INSTALLATION_PATH}/lib64/libxml4c-depdom.so.56
chgrp -fh mqm ${MQ_INSTALLATION_PATH}/lib64/libxml4c-depdom.so.56
# Linking ${MQ_INSTALLATION_PATH}/lib64/libxml4c-depdom.so.56.3 to ${MQ_INSTALLATION_PATH}/lib64/libxml4c-depdom.so
if [ ! -d ${MQ_INSTALLATION_PATH}/lib64 ]
then
    mkdir -p ${MQ_INSTALLATION_PATH}/lib64
else
    rm -f ${MQ_INSTALLATION_PATH}/lib64/libxml4c-depdom.so
fi
ln -s ${MQ_INSTALLATION_PATH}/lib64/libxml4c-depdom.so.56.3 ${MQ_INSTALLATION_PATH}/lib64/libxml4c-depdom.so
chown -fh mqm ${MQ_INSTALLATION_PATH}/lib64/libxml4c-depdom.so
chgrp -fh mqm ${MQ_INSTALLATION_PATH}/lib64/libxml4c-depdom.so
# Linking ${MQ_INSTALLATION_PATH}/lib64/libXML4CMessages.so.56.3 to ${MQ_INSTALLATION_PATH}/lib64/libXML4CMessages.so.56
if [ ! -d ${MQ_INSTALLATION_PATH}/lib64 ]
then
    mkdir -p ${MQ_INSTALLATION_PATH}/lib64
else
    rm -f ${MQ_INSTALLATION_PATH}/lib64/libXML4CMessages.so.56
fi
ln -s ${MQ_INSTALLATION_PATH}/lib64/libXML4CMessages.so.56.3 ${MQ_INSTALLATION_PATH}/lib64/libXML4CMessages.so.56
chown -fh mqm ${MQ_INSTALLATION_PATH}/lib64/libXML4CMessages.so.56
chgrp -fh mqm ${MQ_INSTALLATION_PATH}/lib64/libXML4CMessages.so.56
# Linking ${MQ_INSTALLATION_PATH}/lib64/libXML4CMessages.so.56.3 to ${MQ_INSTALLATION_PATH}/lib64/libXML4CMessages.so
if [ ! -d ${MQ_INSTALLATION_PATH}/lib64 ]
then
    mkdir -p ${MQ_INSTALLATION_PATH}/lib64
else
    rm -f ${MQ_INSTALLATION_PATH}/lib64/libXML4CMessages.so
fi
ln -s ${MQ_INSTALLATION_PATH}/lib64/libXML4CMessages.so.56.3 ${MQ_INSTALLATION_PATH}/lib64/libXML4CMessages.so
chown -fh mqm ${MQ_INSTALLATION_PATH}/lib64/libXML4CMessages.so
chgrp -fh mqm ${MQ_INSTALLATION_PATH}/lib64/libXML4CMessages.so

# @(#) MQMBID sn=p750-002-130704 su=_UmspIOSZEeK1oKoKL_dPJA pn=install/unix/linux_2/server_postinstall.sh
#   <copyright 
#   notice="lm-source-program" 
#   pids="5724-H72," 
#   years="2012" 
#   crc="3622503993" > 
#  Licensed Materials - Property of IBM  
#   
#  5724-H72, 
#   
#  (C) Copyright IBM Corp. 2012 All Rights Reserved.  
#   
#  US Government Users Restricted Rights - Use, duplication or  
#  disclosure restricted by GSA ADP Schedule Contract with  
#  IBM Corp.  
#   </copyright> 


INSTALLROOT=${MQ_INSTALLATION_PATH}


# Invoke setmqinst to refresh symlinks if this installation is the primary
setmqinst_out=`LD_LIBRARY_PATH="" ${MQ_INSTALLATION_PATH}/bin/setmqinst -r -p ${MQ_INSTALLATION_PATH} 2>&1`
rc=$?
if [ $rc -ne 0 ] ; then
  echo "ERROR: Return code \"$rc\" from setmqinst for \"-r -p ${MQ_INSTALLATION_PATH}\", output is:" >&2
  echo "       ${setmqinst_out}" >&2
fi

# Request the user to run mqconfig after installation has completed
cat << EOF

After the installation has completed, run the '${MQ_INSTALLATION_PATH}/bin/mqconfig'
command, using the 'mqm' user ID.

For example, execute the following statement when running as the 'root' user:

    su mqm -c "${MQ_INSTALLATION_PATH}/bin/mqconfig"

The 'mqconfig' command validates that the system configuration satisfies the
requirements for WebSphere MQ, and ensures that the settings for the 'mqm'
user ID are suitably configured.  Other WebSphere MQ administrators in the
'mqm' group can run this command to ensure their user limits are also
properly configured to use WebSphere MQ.

If 'mqconfig' indicates that any of the requirements have not been met,
consult the installation section within the WebSphere MQ Information Center
for details about how to configure the system and user limits.

EOF

echo > /dev/null


%preun
RPM_PACKAGE_SUMMARY="WebSphere MQ Server FileSet"
RPM_PACKAGE_NAME="MQSeriesServer"
RPM_PACKAGE_VERSION="7.5.0"
RPM_PACKAGE_RELEASE="2"
PACKAGEPLATFORMS="x86_64"
MQ_DEFAULT_INSTALLATION_PATH=/opt/mqm
MQ_INSTALLATION_PATH=${RPM_INSTALL_PREFIX}
MQ_RUNSCRIPT=
echo ${SHELLOPTS} | grep xtrace > /dev/null
if [ $? -eq 0 ] ; then 
  env | sort
  uname -a
  id
  MQ_RUNSCRIPT="sh -x"
fi

#   <copyright 
#   notice="lm-source-program" 
#   pids="5724-H72," 
#   years="2005,2012" 
#   crc="122768040" > 
#  Licensed Materials - Property of IBM  
#   
#  5724-H72, 
#   
#  (C) Copyright IBM Corp. 2005, 2012 All Rights Reserved.  
#   
#  US Government Users Restricted Rights - Use, duplication or  
#  disclosure restricted by GSA ADP Schedule Contract with  
#  IBM Corp.  
#   </copyright> 
#
# Pre-uninstallation script
# Checks for already running Q Managers, and if it finds one, stops the
# uninstall.

# If amqiclen exists (should do during uninstall) then run it to clean up
# IPCC resources. If amqiclen returns an error then a queue manager is still
# running so stop the uninstall.
if [ -x ${MQ_INSTALLATION_PATH}/bin/amqiclen ] && [ -f /var/mqm/mqs.ini ]
then
    ${MQ_INSTALLATION_PATH}/bin/amqiclen -x -v > /tmp/amqiclen.$$.out 2>&1
    amqiclen_rc=$?
    if [ $amqiclen_rc -ne 0 ]
    then
      echo "ERROR:    WebSphere MQ shared resources for this installation are still in use." >&2
      echo "          It is likely that one or more queue managers are still running." >&2
      echo "          Return code from from amqiclen was \"$amqiclen_rc\", output was:" >&2
      cat /tmp/amqiclen.$$.out >&2
      rm /tmp/amqiclen.$$.out
      exit 1
    fi
    rm /tmp/amqiclen.$$.out
fi

#   <copyright 
#   notice="lm-source-program" 
#   pids="5724-H72," 
#   years="2005,2012" 
#   crc="1595222582" > 
#  Licensed Materials - Property of IBM  
#   
#  5724-H72, 
#   
#  (C) Copyright IBM Corp. 2005, 2012 All Rights Reserved.  
#   
#  US Government Users Restricted Rights - Use, duplication or  
#  disclosure restricted by GSA ADP Schedule Contract with  
#  IBM Corp.  
#   </copyright> 
#
# Pre-uninstallation check script for all components
# A check is performed to see if there are any fixpack filesets applied to
# the base component which is currently being uninstalled.  If the fixpack
# has been applied, the uninstallation of this component is aborted to prevent
# the situation where the base fileset has been uninstalled leaving an
# uninstallable fixpack.

FIXPACK_BACKUPDIR="${MQ_INSTALLATION_PATH}/maintenance"

find $FIXPACK_BACKUPDIR -type d -maxdepth 2 -print 2>/dev/null | \
      while read directory ; do
  component=`basename $directory`
  if [ "$RPM_PACKAGE_NAME" = "$component" ]; then
    # safety check - are there actually files under this directory?
    num_files=`find "$directory" -type f -print 2>/dev/null | wc -l`
    if [ $num_files -gt 0 ]; then
      cat << EOF

  ERROR:  There appears to be a fixpack installed on this machine for this
          component.

  Please ensure you have removed all fixpacks for the \"$RPM_PACKAGE_NAME\"
  component before trying to remove this package.

EOF
      exit 1
    fi
  fi
done

# Removing product links
rm -f ${MQ_INSTALLATION_PATH}/bin/strmqbrk
rm -f ${MQ_INSTALLATION_PATH}/lib/amqzfu
rm -f ${MQ_INSTALLATION_PATH}/lib/compat/libmqmzf.so
rm -f ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmzf.so
rm -f ${MQ_INSTALLATION_PATH}/lib/compat/libmqmzf_r.so
rm -f ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmzf_r.so
rm -f ${MQ_INSTALLATION_PATH}/lib/compat/libmqutl.so
rm -f ${MQ_INSTALLATION_PATH}/lib64/compat/libmqutl.so
rm -f ${MQ_INSTALLATION_PATH}/lib/compat/libmqutl_r.so
rm -f ${MQ_INSTALLATION_PATH}/lib64/compat/libmqutl_r.so
rm -f ${MQ_INSTALLATION_PATH}/lib/compat/libmqmcb.so
rm -f ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmcb.so
rm -f ${MQ_INSTALLATION_PATH}/lib/compat/libmqmcb_r.so
rm -f ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmcb_r.so
rm -f ${MQ_INSTALLATION_PATH}/lib/compat/libmqmxa.so
rm -f ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmxa.so
rm -f ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmxa64.so
rm -f ${MQ_INSTALLATION_PATH}/lib/compat/libmqmxa_r.so
rm -f ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmxa_r.so
rm -f ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmxa64_r.so
rm -f ${MQ_INSTALLATION_PATH}/lib/compat/libmqmax.so
rm -f ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmax.so
rm -f ${MQ_INSTALLATION_PATH}/lib/compat/libmqmax_r.so
rm -f ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmax_r.so
rm -f ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmax64.so
rm -f ${MQ_INSTALLATION_PATH}/lib64/compat/libmqmax64_r.so
rm -f ${MQ_INSTALLATION_PATH}/lib/libimqs23gl.so
rm -f ${MQ_INSTALLATION_PATH}/lib/libimqs23gl_r.so
rm -f ${MQ_INSTALLATION_PATH}/lib64/libimqs23gl.so
rm -f ${MQ_INSTALLATION_PATH}/lib64/libimqs23gl_r.so
rm -f ${MQ_INSTALLATION_PATH}/lib/compat/libimqs23gl.so
rm -f ${MQ_INSTALLATION_PATH}/lib/compat/libimqs23gl_r.so
rm -f ${MQ_INSTALLATION_PATH}/lib64/compat/libimqs23gl.so
rm -f ${MQ_INSTALLATION_PATH}/lib64/compat/libimqs23gl_r.so
rm -f ${MQ_INSTALLATION_PATH}/lib64/libicudata.so.32
rm -f ${MQ_INSTALLATION_PATH}/lib64/libicudata.so
rm -f ${MQ_INSTALLATION_PATH}/lib64/libicuuc.so.32
rm -f ${MQ_INSTALLATION_PATH}/lib64/libicuuc.so
rm -f ${MQ_INSTALLATION_PATH}/lib64/libicui18n.so.32
rm -f ${MQ_INSTALLATION_PATH}/lib64/libicui18n.so
rm -f ${MQ_INSTALLATION_PATH}/lib64/libxml4c.so.56
rm -f ${MQ_INSTALLATION_PATH}/lib64/libxml4c.so
rm -f ${MQ_INSTALLATION_PATH}/lib64/libxml4c-depdom.so.56
rm -f ${MQ_INSTALLATION_PATH}/lib64/libxml4c-depdom.so
rm -f ${MQ_INSTALLATION_PATH}/lib64/libXML4CMessages.so.56
rm -f ${MQ_INSTALLATION_PATH}/lib64/libXML4CMessages.so

%postun
RPM_PACKAGE_SUMMARY="WebSphere MQ Server FileSet"
RPM_PACKAGE_NAME="MQSeriesServer"
RPM_PACKAGE_VERSION="7.5.0"
RPM_PACKAGE_RELEASE="2"
PACKAGEPLATFORMS="x86_64"
MQ_DEFAULT_INSTALLATION_PATH=/opt/mqm
MQ_INSTALLATION_PATH=${RPM_INSTALL_PREFIX}
MQ_RUNSCRIPT=
echo ${SHELLOPTS} | grep xtrace > /dev/null
if [ $? -eq 0 ] ; then 
  env | sort
  uname -a
  id
  MQ_RUNSCRIPT="sh -x"
fi

#   <copyright 
#   notice="lm-source-program" 
#   pids="5724-H72," 
#   years="2005,2012" 
#   crc="2675138520" > 
#  Licensed Materials - Property of IBM  
#   
#  5724-H72, 
#   
#  (C) Copyright IBM Corp. 2005, 2012 All Rights Reserved.  
#   
#  US Government Users Restricted Rights - Use, duplication or  
#  disclosure restricted by GSA ADP Schedule Contract with  
#  IBM Corp.  
#   </copyright> 
# Pre-uninstallation script
if [ -f ${MQ_INSTALLATION_PATH}/lib/amqpcert.lic ]
then
    rm -f ${MQ_INSTALLATION_PATH}/lib/amqpcert.lic
fi
if [ -f /var/mqm/qmgrs/@SYSTEM/nodelock ]
then
    rm -f /var/mqm/qmgrs/@SYSTEM/nodelock
fi
if [ -f /var/mqm/qmgrs/@SYSTEM/amqtrial.inf ]
then
    rm -f /var/mqm/qmgrs/@SYSTEM/amqtrial.inf
fi


