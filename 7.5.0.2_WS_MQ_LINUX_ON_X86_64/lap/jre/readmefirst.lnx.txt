IBM SDK for Linux, Java Technology Edition, Version 6
=====================================================

This READMEFIRST file applies to Version 6, and to all subsequent
releases, modifications, and service refreshes, until otherwise indicated
in a new READMEFIRST file.

Product documentation:
----------------------

User guides to support the IBM SDK for Java 6 are available for online viewing
in an IBM Information Center: http://publib.boulder.ibm.com/infocenter/javasdk/v6r0/index.jsp.

Any late breaking news can be found in the following IBM support technote:
http://www.ibm.com/support/docview.wss?uid=swg21587401
