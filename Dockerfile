FROM centos:7

# Add IBM MFT dependencies
COPY 7.5.0.2_WS_MQ_LINUX_ON_X86_64 /tmp

# Install updates and python
RUN yum -y install epel-release \
  && yum -y update \
  && yum -y install python python-pip python-devel gcc yum-utils \
  && /tmp/mqlicense.sh -accept \
  && rpm --prefix /opt/mqm -ivh --nodeps /tmp/MQSeriesRuntime-7.5.0-2.x86_64.rpm \
  && rpm --prefix /opt/mqm -ivh --nodeps /tmp/MQSeriesClient-7.5.0-2.x86_64.rpm \
  && rpm --prefix /opt/mqm -ivh --nodeps /tmp/MQSeriesSDK-7.5.0-2.x86_64.rpm \
  && pip install --upgrade pip \
  && pip install pymqi \
  && rm -rf /tmp/* \
  && yum -y remove $(package-cleanup --leaves) \
  && yum -y remove *devel *headers gcc* yum-utils \
  && yum --enablerepo=* clean all \
  && yum -y install git

ENV LD_LIBRARY_PATH=/opt/mqm/lib64

# Volume setup
VOLUME /opt/MQAgent_logs
VOLUME /opt/MQAgent/logs

# Script installation
WORKDIR /opt/MQAgent
RUN git clone https://gitlab.com/pliskin-effem/MQAgent /opt/MQAgent

# Runtime
ENTRYPOINT ["python", "monitor.py"]